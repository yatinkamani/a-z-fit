package com.azfit.Model.CouponCode;

import java.util.List;

public class CouponModel {

    public boolean status;
    public String message;
    public List<CouponData> result;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CouponData> getCouponData() {
        return result;
    }

    public void setCouponData(List<CouponData> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CouponCodeModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }
}
