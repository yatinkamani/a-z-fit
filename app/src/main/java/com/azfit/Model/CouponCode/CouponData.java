package com.azfit.Model.CouponCode;

public class CouponData {

    public String ID;
    public String coupon_code;
    public String coupon_description;
    public String discount_type;
    public String coupon_amount;
    public String free_shipping;
    public String expiry_date;
    public String minimum_amount;
    public String maximum_amount;
    public String individual_use;
    public String exclude_sale_items;
    public String product_ids;
    public String exclude_product_ids;
    public String product_categories;
    public String exclude_product_categories;
    public String customer_email;
    public String usage_limit;
    public String usage_limit_per_user;
    public String total_usaged;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getCoupon_description() {
        return coupon_description;
    }

    public void setCoupon_description(String coupon_description) {
        this.coupon_description = coupon_description;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getCoupon_amount() {
        return coupon_amount;
    }

    public void setCoupon_amount(String coupon_amount) {
        this.coupon_amount = coupon_amount;
    }

    public String getFree_shipping() {
        return free_shipping;
    }

    public void setFree_shipping(String free_shipping) {
        this.free_shipping = free_shipping;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getMinimum_amount() {
        return minimum_amount;
    }

    public void setMinimum_amount(String minimum_amount) {
        this.minimum_amount = minimum_amount;
    }

    public String getMaximum_amount() {
        return maximum_amount;
    }

    public void setMaximum_amount(String maximum_amount) {
        this.maximum_amount = maximum_amount;
    }

    public String getIndividual_use() {
        return individual_use;
    }

    public void setIndividual_use(String individual_use) {
        this.individual_use = individual_use;
    }

    public String getExclude_sale_items() {
        return exclude_sale_items;
    }

    public void setExclude_sale_items(String exclude_sale_items) {
        this.exclude_sale_items = exclude_sale_items;
    }

    public String getProduct_ids() {
        return product_ids;
    }

    public void setProduct_ids(String product_ids) {
        this.product_ids = product_ids;
    }

    public String getExclude_product_ids() {
        return exclude_product_ids;
    }

    public void setExclude_product_ids(String exclude_product_ids) {
        this.exclude_product_ids = exclude_product_ids;
    }

    public String getProduct_categories() {
        return product_categories;
    }

    public void setProduct_categories(String product_categories) {
        this.product_categories = product_categories;
    }

    public String getExclude_product_categories() {
        return exclude_product_categories;
    }

    public void setExclude_product_categories(String exclude_product_categories) {
        this.exclude_product_categories = exclude_product_categories;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getUsage_limit() {
        return usage_limit;
    }

    public void setUsage_limit(String usage_limit) {
        this.usage_limit = usage_limit;
    }

    public String getUsage_limit_per_user() {
        return usage_limit_per_user;
    }

    public void setUsage_limit_per_user(String usage_limit_per_user) {
        this.usage_limit_per_user = usage_limit_per_user;
    }

    public String getTotal_usaged() {
        return total_usaged;
    }

    public void setTotal_usaged(String total_usaged) {
        this.total_usaged = total_usaged;
    }

    @Override
    public String toString() {
        return "CouponData{" +
                "iD='" + ID + '\'' +
                ", coupon_code='" + coupon_code + '\'' +
                ", coupon_description='" + coupon_description + '\'' +
                ", discount_type='" + discount_type + '\'' +
                ", coupon_amount='" + coupon_amount + '\'' +
                ", free_shipping='" + free_shipping + '\'' +
                ", expiry_date=" + expiry_date +
                ", minimum_amount=" + minimum_amount +
                ", maximum_amount=" + maximum_amount +
                ", individual_use='" + individual_use + '\'' +
                ", exclude_sale_items='" + exclude_sale_items + '\'' +
                ", product_ids=" + product_ids +
                ", exclude_product_ids=" + exclude_product_ids +
                ", product_categories=" + product_categories +
                ", exclude_product_categories=" + exclude_product_categories +
                ", customer_email=" + customer_email +
                ", usage_limit='" + usage_limit + '\'' +
                ", usage_limit_per_user='" + usage_limit_per_user + '\'' +
                ", total_usaged='" + total_usaged + '\'' +
                '}';
    }
}
