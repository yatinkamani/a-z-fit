package com.azfit.Model.Product;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchProductModel {

    @SerializedName("product_search")
    private List<ProductData> all_products;

    public List<ProductData> getAll_products ()
    {
        return all_products;
    }

    public void setAll_products (List<ProductData> all_products) {
        this.all_products = all_products;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [all_products = "+all_products+"]";
    }
}
