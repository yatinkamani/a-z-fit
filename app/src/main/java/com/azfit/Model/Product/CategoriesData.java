package com.azfit.Model.Product;

public class CategoriesData {

    private String name;

    private String id;

    private String slug;

    public CategoriesData(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    /*@Override
    public String toString()
    {
        return "{name = "+name+", id = "+id+", slug = "+slug+"}";
    }*/

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", slug='" + slug + '\'' +
                '}';
    }
}
