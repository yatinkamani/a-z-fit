package com.azfit.Model.Product;

public class Unit_priceData {

    private String product;

    private String price_regular;

    private String price_auto;

    private String price_sale;

    private String price_html;

    private String price;

    private String base;

    public String getProduct ()
    {
        return product;
    }

    public void setProduct (String product)
    {
        this.product = product;
    }

    public String getPrice_regular ()
    {
        return price_regular;
    }

    public void setPrice_regular (String price_regular)
    {
        this.price_regular = price_regular;
    }

    public String getPrice_auto ()
    {
        return price_auto;
    }

    public void setPrice_auto (String price_auto)
    {
        this.price_auto = price_auto;
    }

    public String getPrice_sale ()
    {
        return price_sale;
    }

    public void setPrice_sale (String price_sale)
    {
        this.price_sale = price_sale;
    }

    public String getPrice_html ()
    {
        return price_html;
    }

    public void setPrice_html (String price_html)
    {
        this.price_html = price_html;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getBase ()
    {
        return base;
    }

    public void setBase (String base)
    {
        this.base = base;
    }

    /*@Override
    public String toString()
    {
        return "{product = "+product+", price_regular = "+price_regular+", price_auto = "+price_auto+", price_sale = "+price_sale+", price_html = "+price_html+", price = "+price+", base = "+base+"}";
    }*/

    @Override
    public String toString() {
        return "{" +
                "product='" + product + '\'' +
                ", price_regular='" + price_regular + '\'' +
                ", price_auto='" + price_auto + '\'' +
                ", price_sale='" + price_sale + '\'' +
                ", price_html='" + price_html + '\'' +
                ", price='" + price + '\'' +
                ", base='" + base + '\'' +
                '}';
    }
}
