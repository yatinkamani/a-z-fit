package com.azfit.Model.Product;

public class ImagesData {

    private String date_modified_gmt;

    private String thumbnail;

    private String date_modified;

    private String src;

    private String date_created;

    private String name;

    private String alt;

    private String date_created_gmt;

    private String id;

    public ImagesData(String id, String src, String name, String thumbnail) {
        this.id = id;
        this.src = src;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getDate_modified_gmt () {
        return date_modified_gmt;
    }

    public void setDate_modified_gmt (String date_modified_gmt) {
        this.date_modified_gmt = date_modified_gmt;
    }

    public String getThumbnail () {
        return thumbnail;
    }

    public void setThumbnail (String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDate_modified () {
        return date_modified;
    }

    public void setDate_modified (String date_modified) {
        this.date_modified = date_modified;
    }

    public String getSrc () {
        return src;
    }

    public void setSrc (String src) {
        this.src = src;
    }

    public String getDate_created () {
        return date_created;
    }

    public void setDate_created (String date_created) {
        this.date_created = date_created;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getAlt () {
        return alt;
    }

    public void setAlt (String alt) {
        this.alt = alt;
    }

    public String getDate_created_gmt () {
        return date_created_gmt;
    }

    public void setDate_created_gmt (String date_created_gmt) {
        this.date_created_gmt = date_created_gmt;
    }

    public String getId () {
        return id;
    }

    public void setId (String id) {
        this.id = id;
    }

    /*@Override
    public String toString() {
        return "{date_modified_gmt = "+date_modified_gmt+", thumbnail = "+thumbnail+", date_modified = "+date_modified+", src = "+src+", date_created = "+date_created+", name = "+name+", alt = "+alt+", date_created_gmt = "+date_created_gmt+", id = "+id+"}";
    }*/

    @Override
    public String toString() {
        return "{" +
                "date_modified_gmt='" + date_modified_gmt + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", date_modified='" + date_modified + '\'' +
                ", src='" + src + '\'' +
                ", date_created='" + date_created + '\'' +
                ", name='" + name + '\'' +
                ", alt='" + alt + '\'' +
                ", date_created_gmt='" + date_created_gmt + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
