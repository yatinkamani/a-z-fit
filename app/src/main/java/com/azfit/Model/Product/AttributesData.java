package com.azfit.Model.Product;

import java.util.List;

public class AttributesData {

    private String id;
    private String name;
    private String position;
    private String visible;
    private String variation;
    private List<String> option;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public List<String> getOption() {
        return option;
    }

    public void setOption(List<String> option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return "AttributesData{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", visible='" + visible + '\'' +
                ", variation='" + variation + '\'' +
                ", option=" + option +
                '}';
    }
}
