package com.azfit.Model.Product;

import java.util.List;

public class ProductData {

    private List<Object> upsell_ids;

    private String featured;

    private String purchasable;

    private List<Object> grouped_products;

    private String free_shipping;

    /*private String _links;*/

    private String tax_status;

    private String catalog_visibility;

    private List<Object> delivery_time;

    private String type;

    private String external_url;

    private String price;

    private List<Meta_dataData> meta_data;

    private String id;

    private String sku;

    private String slug;

    private String date_on_sale_from;

    private String shipping_required;

    private List<Object> sale_price_regular_label;

    private String date_modified_gmt;

    private List<ImagesData> images;

    private String stock_status;

    private String price_html;

    private String download_expiry;

    private String backordered;

    private String weight;

    private String rating_count;

    private List<TagData> tags;

    private String date_on_sale_to;

    private String sold_individually;

    private List<Object> unit;

    private String backorders;

    private String shipping_taxable;

    private String parent_id;

    private String download_limit;

    private String name;

    private String shipping_class;

    private String differential_taxation;

    private String button_text;

    private String permalink;

    private String status;

    private List<Object> cross_sell_ids;

    private String short_description;

    private String virtual;

    private String downloadable;

    private String menu_order;

    private String description;

    private String date_on_sale_to_gmt;

    private String date_on_sale_from_gmt;

    private String regular_price;

    private String backorders_allowed;

    private List<Object> sale_price_label;

    private List<Object> downloads;

    private String reviews_allowed;

    private List<String> variations;

    private List<CategoriesData> categories;

    private String total_sales;

    private String on_sale;

    private String manage_stock;

    private List<Default_attributesData> default_attributes;

    private String purchase_note;

    private String date_created;

    private String tax_class;

    private String date_created_gmt;

    private String average_rating;

    private String stock_quantity;

    private Unit_priceData unit_price;

    private String sale_price;

    private String shipping_class_id;

    private String date_modified;

    private String service;

    private List<String> related_ids;

    private List<Object> attributes;

    private String mini_desc;

    private String min_age;

    private Dimensions dimensions;

    public List<Object> getUpsell_ids ()
    {
        return upsell_ids;
    }

    public void setUpsell_ids (List<Object> upsell_ids)
    {
        this.upsell_ids = upsell_ids;
    }

    public String getFeatured ()
    {
        return featured;
    }

    public void setFeatured (String featured)
    {
        this.featured = featured;
    }

    public String getPurchasable ()
    {
        return purchasable;
    }

    public void setPurchasable (String purchasable)
    {
        this.purchasable = purchasable;
    }

    public List<Object> getGrouped_products ()
    {
        return grouped_products;
    }

    public void setGrouped_products (List<Object> grouped_products)
    {
        this.grouped_products = grouped_products;
    }

    public String getFree_shipping ()
    {
        return free_shipping;
    }

    public void setFree_shipping (String free_shipping)
    {
        this.free_shipping = free_shipping;
    }

   /* public _links get_links ()
    {
        return _links;
    }

    public void set_links (_links _links)
    {
        this._links = _links;
    }*/

    public String getTax_status ()
    {
        return tax_status;
    }

    public void setTax_status (String tax_status)
    {
        this.tax_status = tax_status;
    }

    public String getCatalog_visibility ()
    {
        return catalog_visibility;
    }

    public void setCatalog_visibility (String catalog_visibility)
    {
        this.catalog_visibility = catalog_visibility;
    }

    public List<Object> getDelivery_time ()
    {
        return delivery_time;
    }

    public void setDelivery_time (List<Object> delivery_time)
    {
        this.delivery_time = delivery_time;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getExternal_url ()
    {
        return external_url;
    }

    public void setExternal_url (String external_url)
    {
        this.external_url = external_url;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public List<Meta_dataData> getMeta_data ()
    {
        return meta_data;
    }

    public void setMeta_data (List<Meta_dataData> meta_data)
    {
        this.meta_data = meta_data;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSku ()
    {
        return sku;
    }

    public void setSku (String sku)
    {
        this.sku = sku;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    public String getDate_on_sale_from ()
    {
        return date_on_sale_from;
    }

    public void setDate_on_sale_from (String date_on_sale_from)
    {
        this.date_on_sale_from = date_on_sale_from;
    }

    public String getShipping_required ()
    {
        return shipping_required;
    }

    public void setShipping_required (String shipping_required)
    {
        this.shipping_required = shipping_required;
    }

    public List<Object> getSale_price_regular_label ()
    {
        return sale_price_regular_label;
    }

    public void setSale_price_regular_label (List<Object> sale_price_regular_label)
    {
        this.sale_price_regular_label = sale_price_regular_label;
    }

    public String getDate_modified_gmt ()
    {
        return date_modified_gmt;
    }

    public void setDate_modified_gmt (String date_modified_gmt)
    {
        this.date_modified_gmt = date_modified_gmt;
    }

    public List<ImagesData> getImages ()
    {
        return images;
    }

    public void setImages (List<ImagesData> images)
    {
        this.images = images;
    }

    public String getStock_status ()
    {
        return stock_status;
    }

    public void setStock_status (String stock_status)
    {
        this.stock_status = stock_status;
    }

    public String getPrice_html ()
    {
        return price_html;
    }

    public void setPrice_html (String price_html)
    {
        this.price_html = price_html;
    }

    public String getDownload_expiry ()
    {
        return download_expiry;
    }

    public void setDownload_expiry (String download_expiry)
    {
        this.download_expiry = download_expiry;
    }

    public String getBackordered ()
    {
        return backordered;
    }

    public void setBackordered (String backordered)
    {
        this.backordered = backordered;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getRating_count ()
    {
        return rating_count;
    }

    public void setRating_count (String rating_count)
    {
        this.rating_count = rating_count;
    }

    public List<TagData> getTags ()
    {
        return tags;
    }

    public void setTags (List<TagData> tags)
    {
        this.tags = tags;
    }

    public String getDate_on_sale_to ()
    {
        return date_on_sale_to;
    }

    public void setDate_on_sale_to (String date_on_sale_to)
    {
        this.date_on_sale_to = date_on_sale_to;
    }

    public String getSold_individually ()
    {
        return sold_individually;
    }

    public void setSold_individually (String sold_individually)
    {
        this.sold_individually = sold_individually;
    }

    public List<Object> getUnit ()
    {
        return unit;
    }

    public void setUnit (List<Object> unit)
    {
        this.unit = unit;
    }

    public String getBackorders ()
    {
        return backorders;
    }

    public void setBackorders (String backorders)
    {
        this.backorders = backorders;
    }

    public String getShipping_taxable ()
    {
        return shipping_taxable;
    }

    public void setShipping_taxable (String shipping_taxable)
    {
        this.shipping_taxable = shipping_taxable;
    }

    public String getParent_id ()
    {
        return parent_id;
    }

    public void setParent_id (String parent_id)
    {
        this.parent_id = parent_id;
    }

    public String getDownload_limit ()
    {
        return download_limit;
    }

    public void setDownload_limit (String download_limit)
    {
        this.download_limit = download_limit;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getShipping_class ()
    {
        return shipping_class;
    }

    public void setShipping_class (String shipping_class)
    {
        this.shipping_class = shipping_class;
    }

    public String getDifferential_taxation ()
    {
        return differential_taxation;
    }

    public void setDifferential_taxation (String differential_taxation)
    {
        this.differential_taxation = differential_taxation;
    }

    public String getButton_text ()
    {
        return button_text;
    }

    public void setButton_text (String button_text)
    {
        this.button_text = button_text;
    }

    public String getPermalink ()
    {
        return permalink;
    }

    public void setPermalink (String permalink)
    {
        this.permalink = permalink;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<Object> getCross_sell_ids ()
    {
        return cross_sell_ids;
    }

    public void setCross_sell_ids (List<Object> cross_sell_ids)
    {
        this.cross_sell_ids = cross_sell_ids;
    }

    public String getShort_description ()
    {
        return short_description;
    }

    public void setShort_description (String short_description)
    {
        this.short_description = short_description;
    }

    public String getVirtual ()
    {
        return virtual;
    }

    public void setVirtual (String virtual)
    {
        this.virtual = virtual;
    }

    public String getDownloadable ()
    {
        return downloadable;
    }

    public void setDownloadable (String downloadable)
    {
        this.downloadable = downloadable;
    }

    public String getMenu_order ()
    {
        return menu_order;
    }

    public void setMenu_order (String menu_order)
    {
        this.menu_order = menu_order;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getDate_on_sale_to_gmt ()
    {
        return date_on_sale_to_gmt;
    }

    public void setDate_on_sale_to_gmt (String date_on_sale_to_gmt)
    {
        this.date_on_sale_to_gmt = date_on_sale_to_gmt;
    }

    public String getDate_on_sale_from_gmt ()
    {
        return date_on_sale_from_gmt;
    }

    public void setDate_on_sale_from_gmt (String date_on_sale_from_gmt)
    {
        this.date_on_sale_from_gmt = date_on_sale_from_gmt;
    }

    public String getRegular_price ()
    {
        return regular_price;
    }

    public void setRegular_price (String regular_price)
    {
        this.regular_price = regular_price;
    }

    public String getBackorders_allowed ()
    {
        return backorders_allowed;
    }

    public void setBackorders_allowed (String backorders_allowed)
    {
        this.backorders_allowed = backorders_allowed;
    }

    public List<Object> getSale_price_label ()
    {
        return sale_price_label;
    }

    public void setSale_price_label (List<Object> sale_price_label)
    {
        this.sale_price_label = sale_price_label;
    }

    public List<Object> getDownloads ()
    {
        return downloads;
    }

    public void setDownloads (List<Object> downloads)
    {
        this.downloads = downloads;
    }

    public String getReviews_allowed ()
    {
        return reviews_allowed;
    }

    public void setReviews_allowed (String reviews_allowed)
    {
        this.reviews_allowed = reviews_allowed;
    }

    public List<String> getVariations ()
    {
        return variations;
    }

    public void setVariations (List<String> variations)
    {
        this.variations = variations;
    }

    public List<CategoriesData> getCategories ()
    {
        return categories;
    }

    public void setCategories (List<CategoriesData> categories)
    {
        this.categories = categories;
    }

    public String getTotal_sales ()
    {
        return total_sales;
    }

    public void setTotal_sales (String total_sales)
    {
        this.total_sales = total_sales;
    }

    public String getOn_sale ()
    {
        return on_sale;
    }

    public void setOn_sale (String on_sale)
    {
        this.on_sale = on_sale;
    }

    public String getManage_stock ()
    {
        return manage_stock;
    }

    public void setManage_stock (String manage_stock)
    {
        this.manage_stock = manage_stock;
    }

    public List<Default_attributesData> getDefault_attributes ()
    {
        return default_attributes;
    }

    public void setDefault_attributes (List<Default_attributesData> default_attributes)
    {
        this.default_attributes = default_attributes;
    }

    public String getPurchase_note ()
    {
        return purchase_note;
    }

    public void setPurchase_note (String purchase_note)
    {
        this.purchase_note = purchase_note;
    }

    public String getDate_created ()
    {
        return date_created;
    }

    public void setDate_created (String date_created)
    {
        this.date_created = date_created;
    }

    public String getTax_class ()
    {
        return tax_class;
    }

    public void setTax_class (String tax_class)
    {
        this.tax_class = tax_class;
    }

    public String getDate_created_gmt ()
    {
        return date_created_gmt;
    }

    public void setDate_created_gmt (String date_created_gmt)
    {
        this.date_created_gmt = date_created_gmt;
    }

    public String getAverage_rating ()
    {
        return average_rating;
    }

    public void setAverage_rating (String average_rating)
    {
        this.average_rating = average_rating;
    }

    public String getStock_quantity ()
    {
        return stock_quantity;
    }

    public void setStock_quantity (String stock_quantity)
    {
        this.stock_quantity = stock_quantity;
    }

    public Unit_priceData getUnit_price ()
    {
        return unit_price;
    }

    public void setUnit_price (Unit_priceData unit_price)
    {
        this.unit_price = unit_price;
    }

    public String getSale_price ()
    {
        return sale_price;
    }

    public void setSale_price (String sale_price)
    {
        this.sale_price = sale_price;
    }

    public String getShipping_class_id ()
    {
        return shipping_class_id;
    }

    public void setShipping_class_id (String shipping_class_id)
    {
        this.shipping_class_id = shipping_class_id;
    }

    public String getDate_modified ()
    {
        return date_modified;
    }

    public void setDate_modified (String date_modified)
    {
        this.date_modified = date_modified;
    }

    public String getService ()
    {
        return service;
    }

    public void setService (String service)
    {
        this.service = service;
    }

    public List<String> getRelated_ids ()
    {
        return related_ids;
    }

    public void setRelated_ids (List<String> related_ids)
    {
        this.related_ids = related_ids;
    }

    public List<Object> getAttributes ()
    {
        return attributes;
    }

    public void setAttributes (List<Object> attributes)
    {
        this.attributes = attributes;
    }

    public String getMini_desc ()
    {
        return mini_desc;
    }

    public void setMini_desc (String mini_desc)
    {
        this.mini_desc = mini_desc;
    }

    public String getMin_age ()
    {
        return min_age;
    }

    public void setMin_age (String min_age)
    {
        this.min_age = min_age;
    }

    public Dimensions getDimensions ()
    {
        return dimensions;
    }

    public void setDimensions (Dimensions dimensions)
    {
        this.dimensions = dimensions;
    }

    /*@Override
    public String toString()
    {
        return "{upSell_ids = "+upsell_ids+", featured = "+featured+", purchasable = "+purchasable+", grouped_products = "+grouped_products+", free_shipping = "+free_shipping+", tax_status = "+tax_status+", catalog_visibility = "+catalog_visibility+", delivery_time = "+delivery_time+", type = "+type+", external_url = "+external_url+", price = "+price+", meta_data = "+meta_data+", id = "+id+", sku = "+sku+", slug = "+slug+", date_on_sale_from = "+date_on_sale_from+", shipping_required = "+shipping_required+", sale_price_regular_label = "+sale_price_regular_label+", date_modified_gmt = "+date_modified_gmt+", images = "+images+", stock_status = "+stock_status+", price_html = "+price_html+", download_expiry = "+download_expiry+", backordered = "+backordered+", weight = "+weight+", rating_count = "+rating_count+", tags = "+tags+", date_on_sale_to = "+date_on_sale_to+", sold_individually = "+sold_individually+", unit = "+unit+", backorders = "+backorders+", shipping_taxable = "+shipping_taxable+", parent_id = "+parent_id+", download_limit = "+download_limit+", name = "+name+", shipping_class = "+shipping_class+", differential_taxation = "+differential_taxation+", button_text = "+button_text+", permalink = "+permalink+", status = "+status+", cross_sell_ids = "+cross_sell_ids+", short_description = "+short_description+", virtual = "+virtual+", downloadable = "+downloadable+", menu_order = "+menu_order+", description = "+description+", date_on_sale_to_gmt = "+date_on_sale_to_gmt+", date_on_sale_from_gmt = "+date_on_sale_from_gmt+", regular_price = "+regular_price+", backorders_allowed = "+backorders_allowed+", sale_price_label = "+sale_price_label+", downloads = "+downloads+", reviews_allowed = "+reviews_allowed+", variations = "+variations+", categories = "+categories+", total_sales = "+total_sales+", on_sale = "+on_sale+", manage_stock = "+manage_stock+", default_attributes = "+default_attributes+", purchase_note = "+purchase_note+", date_created = "+date_created+", tax_class = "+tax_class+", date_created_gmt = "+date_created_gmt+", average_rating = "+average_rating+", stock_quantity = "+stock_quantity+", unit_price = "+unit_price+", sale_price = "+sale_price+", shipping_class_id = "+shipping_class_id+", date_modified = "+date_modified+", service = "+service+", related_ids = "+related_ids+", attributes = "+attributes+", mini_desc = "+mini_desc+", min_age = "+min_age+", dimensions = "+dimensions+"}";
    }*/

    @Override
    public String toString() {
        return "{" +
                "upsell_ids=" + upsell_ids +
                ", featured='" + featured + '\'' +
                ", purchasable='" + purchasable + '\'' +
                ", grouped_products=" + grouped_products +
                ", free_shipping='" + free_shipping + '\'' +
                ", tax_status='" + tax_status + '\'' +
                ", catalog_visibility='" + catalog_visibility + '\'' +
                ", delivery_time=" + delivery_time +
                ", type='" + type + '\'' +
                ", external_url='" + external_url + '\'' +
                ", price='" + price + '\'' +
                ", meta_data=" + meta_data +
                ", id='" + id + '\'' +
                ", sku='" + sku + '\'' +
                ", slug='" + slug + '\'' +
                ", date_on_sale_from='" + date_on_sale_from + '\'' +
                ", shipping_required='" + shipping_required + '\'' +
                ", sale_price_regular_label=" + sale_price_regular_label +
                ", date_modified_gmt='" + date_modified_gmt + '\'' +
                ", images=" + images +
                ", stock_status='" + stock_status + '\'' +
                ", price_html='" + price_html + '\'' +
                ", download_expiry='" + download_expiry + '\'' +
                ", backordered='" + backordered + '\'' +
                ", weight='" + weight + '\'' +
                ", rating_count='" + rating_count + '\'' +
                ", tags=" + tags +
                ", date_on_sale_to='" + date_on_sale_to + '\'' +
                ", sold_individually='" + sold_individually + '\'' +
                ", unit=" + unit +
                ", backorders='" + backorders + '\'' +
                ", shipping_taxable='" + shipping_taxable + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", download_limit='" + download_limit + '\'' +
                ", name='" + name + '\'' +
                ", shipping_class='" + shipping_class + '\'' +
                ", differential_taxation='" + differential_taxation + '\'' +
                ", button_text='" + button_text + '\'' +
                ", permalink='" + permalink + '\'' +
                ", status='" + status + '\'' +
                ", cross_sell_ids=" + cross_sell_ids +
                ", short_description='" + short_description + '\'' +
                ", virtual='" + virtual + '\'' +
                ", downloadable='" + downloadable + '\'' +
                ", menu_order='" + menu_order + '\'' +
                ", description='" + description + '\'' +
                ", date_on_sale_to_gmt='" + date_on_sale_to_gmt + '\'' +
                ", date_on_sale_from_gmt='" + date_on_sale_from_gmt + '\'' +
                ", regular_price='" + regular_price + '\'' +
                ", backorders_allowed='" + backorders_allowed + '\'' +
                ", sale_price_label=" + sale_price_label +
                ", downloads=" + downloads +
                ", reviews_allowed='" + reviews_allowed + '\'' +
                ", variations=" + variations +
                ", categories=" + categories +
                ", total_sales='" + total_sales + '\'' +
                ", on_sale='" + on_sale + '\'' +
                ", manage_stock='" + manage_stock + '\'' +
                ", default_attributes=" + default_attributes +
                ", purchase_note='" + purchase_note + '\'' +
                ", date_created='" + date_created + '\'' +
                ", tax_class='" + tax_class + '\'' +
                ", date_created_gmt='" + date_created_gmt + '\'' +
                ", average_rating='" + average_rating + '\'' +
                ", stock_quantity='" + stock_quantity + '\'' +
                ", unit_price=" + unit_price +
                ", sale_price='" + sale_price + '\'' +
                ", shipping_class_id='" + shipping_class_id + '\'' +
                ", date_modified='" + date_modified + '\'' +
                ", service='" + service + '\'' +
                ", related_ids=" + related_ids +
                ", attributes=" + attributes +
                ", mini_desc='" + mini_desc + '\'' +
                ", min_age='" + min_age + '\'' +
                ", dimensions=" + dimensions +
                '}';
    }
}
