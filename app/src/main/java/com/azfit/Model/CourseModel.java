package com.azfit.Model;

public class CourseModel {

    String id, name, image, price, user_count, time, rate, course_owner;

    public CourseModel(String id, String name, String image, String price, String user_count, String time, String rate, String course_owner) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.price = price;
        this.user_count = user_count;
        this.time = time;
        this.rate = rate;
        this.course_owner = course_owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUser_count() {
        return user_count;
    }

    public void setUser_count(String user_count) {
        this.user_count = user_count;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCourse_owner() {
        return course_owner;
    }

    public void setCourse_owner(String course_owner) {
        this.course_owner = course_owner;
    }
}
