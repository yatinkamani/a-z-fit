package com.azfit.Model.Orders;

public class PaymentDetails {

    public String method_id;
    public String method_title;
    public boolean paid;

    public String getMethod_id() {
        return method_id;
    }

    public void setMethod_id(String method_id) {
        this.method_id = method_id;
    }

    public String getMethod_title() {
        return method_title;
    }

    public void setMethod_title(String method_title) {
        this.method_title = method_title;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public String toString() {
        return "{" +
                "method_id='" + method_id + '\'' +
                ", method_title='" + method_title + '\'' +
                ", paid=" + paid +
                '}';
    }
}
