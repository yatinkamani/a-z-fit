package com.azfit.Model.Orders;

public class TaxLine {

    public int id;
    public int rate_id;
    public String code;
    public String title;
    public String total;
    public String compound;

    public TaxLine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRate_id() {
        return rate_id;
    }

    public void setRate_id(int rate_id) {
        this.rate_id = rate_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCompound() {
        return compound;
    }

    public void setCompound(String compound) {
        this.compound = compound;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", rate_id=" + rate_id +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", total='" + total + '\'' +
                ", compound='" + compound + '\'' +
                '}';
    }
}
