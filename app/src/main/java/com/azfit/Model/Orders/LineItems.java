package com.azfit.Model.Orders;

import java.util.List;

public class LineItems {
    public int id;
    public String subtotal;
    public String subtotal_tax;
    public String total;
    public String total_tax;
    public String price;
    public int quantity;
    public String tax_class;
    public String name;
    public String product_id;
    public String sku;
    public List<Object> meta;
    public String expire;
    public String status;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal_tax() {
        return subtotal_tax;
    }

    public void setSubtotal_tax(String subtotal_tax) {
        this.subtotal_tax = subtotal_tax;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax_class() {
        return tax_class;
    }

    public void setTax_class(String tax_class) {
        this.tax_class = tax_class;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public List<Object> getMeta() {
        return meta;
    }

    public void setMeta(List<Object> meta) {
        this.meta = meta;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", subtotal='" + subtotal + '\'' +
                ", subtotal_tax='" + subtotal_tax + '\'' +
                ", total='" + total + '\'' +
                ", total_tax='" + total_tax + '\'' +
                ", price='" + price + '\'' +
                ", quantity=" + quantity +
                ", tax_class='" + tax_class + '\'' +
                ", name='" + name + '\'' +
                ", product_id=" + product_id +
                ", sku='" + sku + '\'' +
                ", meta=" + meta +
                '}';
    }
}
