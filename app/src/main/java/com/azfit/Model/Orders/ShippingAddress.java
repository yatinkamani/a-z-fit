package com.azfit.Model.Orders;

public class ShippingAddress {
	String first_name, last_name, company, address_1, address_2, city, state,
			postcode, country;

	public ShippingAddress() {
	}

	public ShippingAddress(String first_name, String last_name,
						   String company, String address_1, String address_2, String city,
						   String state, String postcode, String country) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.company = company;
		this.address_1 = address_1;
		this.address_2 = address_2;
		this.city = city;
		this.state = state;
		this.postcode = postcode;
		this.country = country;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress_1() {
		return address_1;
	}

	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}

	public String getAddress_2() {
		return address_2;
	}

	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "{" +
				"first_name='" + first_name + '\'' +
				", last_name='" + last_name + '\'' +
				", company='" + company + '\'' +
				", address_1='" + address_1 + '\'' +
				", address_2='" + address_2 + '\'' +
				", city='" + city + '\'' +
				", state='" + state + '\'' +
				", postcode='" + postcode + '\'' +
				", country='" + country + '\'' +
				'}';
	}
}
