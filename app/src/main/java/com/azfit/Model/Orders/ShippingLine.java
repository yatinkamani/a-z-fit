package com.azfit.Model.Orders;

public class ShippingLine {

    public int id;
    public String method_id;
    public String method_title;
    public String total;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMethod_id() {
        return method_id;
    }

    public void setMethod_id(String method_id) {
        this.method_id = method_id;
    }

    public String getMethod_title() {
        return method_title;
    }

    public void setMethod_title(String method_title) {
        this.method_title = method_title;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", method_id='" + method_id + '\'' +
                ", method_title='" + method_title + '\'' +
                ", total='" + total + '\'' +
                '}';
    }
}
