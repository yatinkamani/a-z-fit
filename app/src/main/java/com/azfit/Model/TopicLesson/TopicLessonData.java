package com.azfit.Model.TopicLesson;

import java.util.List;

public class TopicLessonData {

    public String ID;
    public String post_title;
    public String post_content;
    public String post_name;
    public String course_id;
    public List<Object> attachments;
    public String thumbnail;
    public List<Videos> video;

    public String isComplete;

    public String getID() {
        return ID;
    }

    public void setiD(String ID) {
        this.ID = ID;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public List<Object> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Object> attachments) {
        this.attachments = attachments;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<Videos> getVideo() {
        return video;
    }

    public void setVideo(List<Videos> video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return "{" +
                "iD='" + ID + '\'' +
                ", post_title='" + post_title + '\'' +
                ", post_content='" + post_content + '\'' +
                ", post_name='" + post_name + '\'' +
                ", course_id='" + course_id + '\'' +
                ", attachments=" + attachments +
                ", thumbnail='" + thumbnail + '\'' +
                ", video=" + video +
                '}';
    }
}
