package com.azfit.Model.TopicLesson;

import java.util.List;

public class TopicLessonModel {

    public String status_code;
    public String message;
    public List<TopicLessonData> data;

    public String isSubscribe;

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TopicLessonData> getData() {
        return data;
    }

    public void setData(List<TopicLessonData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "status_code='" + status_code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
