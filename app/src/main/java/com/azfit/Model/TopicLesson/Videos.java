package com.azfit.Model.TopicLesson;

public class Videos {

    public String source;
    public String source_video_id;
    public String poster;
    public String source_external_url;
    public String source_youtube;
    public String source_vimeo;
    public String source_embedded;
    public RunTime runtime;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource_video_id() {
        return source_video_id;
    }

    public void setSource_video_id(String source_video_id) {
        this.source_video_id = source_video_id;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getSource_external_url() {
        return source_external_url;
    }

    public void setSource_external_url(String source_external_url) {
        this.source_external_url = source_external_url;
    }

    public String getSource_youtube() {
        return source_youtube;
    }

    public void setSource_youtube(String source_youtube) {
        this.source_youtube = source_youtube;
    }

    public String getSource_vimeo() {
        return source_vimeo;
    }

    public void setSource_vimeo(String source_vimeo) {
        this.source_vimeo = source_vimeo;
    }

    public String getSource_embedded() {
        return source_embedded;
    }

    public void setSource_embedded(String source_embedded) {
        this.source_embedded = source_embedded;
    }

    public RunTime getRuntime() {
        return runtime;
    }

    public void setRuntime(RunTime runtime) {
        this.runtime = runtime;
    }

    @Override
    public String toString() {
        return "{" +
                "source='" + source + '\'' +
                ", source_video_id='" + source_video_id + '\'' +
                ", poster='" + poster + '\'' +
                ", source_external_url='" + source_external_url + '\'' +
                ", source_youtube='" + source_youtube + '\'' +
                ", source_vimeo='" + source_vimeo + '\'' +
                ", source_embedded='" + source_embedded + '\'' +
                ", runtime=" + runtime +
                '}';
    }
}
