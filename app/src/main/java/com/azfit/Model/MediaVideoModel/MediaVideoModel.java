package com.azfit.Model.MediaVideoModel;

public class MediaVideoModel {

    String id;
    String mime_type;
    String source_url;
    String media_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    @Override
    public String toString() {
        return "MediaVideoModel{" +
                "id='" + id + '\'' +
                ", mime_type='" + mime_type + '\'' +
                ", source_url='" + source_url + '\'' +
                ", media_type='" + media_type + '\'' +
                '}';
    }
}
