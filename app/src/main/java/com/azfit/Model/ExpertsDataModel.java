package com.azfit.Model;

public class ExpertsDataModel {

    String id, name, field, image;

    public ExpertsDataModel(String id, String name, String field, String image) {
        this.id = id;
        this.name = name;
        this.field = field;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
