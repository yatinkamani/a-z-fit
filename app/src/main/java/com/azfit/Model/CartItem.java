package com.azfit.Model;

import com.azfit.Model.Product.ProductData;

import java.io.Serializable;

public class CartItem implements Serializable {

    private ProductData product;
//    private ProductVariationModel model;
    private int qty;
    private double cartTotal;
    String variation;

    public double getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(double cartTotal) {
        this.cartTotal = cartTotal;
    }

    public CartItem() {
    }

    public CartItem(ProductData product, int qty, String variation) {
        super();
        this.product = product;
        this.qty = qty;
        this.variation = variation;
    }

    public ProductData getProduct() {
        return product;
    }

    public void setProduct(ProductData product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }
}
