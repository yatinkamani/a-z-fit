package com.azfit.Model.ShippingMethod;

public class Settings {

    private Title title;

    private Requires requires;

    private Min_amount min_amount;

    private Ignore_discounts ignore_discounts;

    public void setTitle(Title title){
        this.title = title;
    }
    public Title getTitle(){
        return this.title;
    }
    public void setRequires(Requires requires){
        this.requires = requires;
    }
    public Requires getRequires(){
        return this.requires;
    }
    public void setMin_amount(Min_amount min_amount){
        this.min_amount = min_amount;
    }
    public Min_amount getMin_amount(){
        return this.min_amount;
    }
    public void setIgnore_discounts(Ignore_discounts ignore_discounts){
        this.ignore_discounts = ignore_discounts;
    }
    public Ignore_discounts getIgnore_discounts(){
        return this.ignore_discounts;
    }

    @Override
    public String toString() {
        return "{" +
                "title=" + title +
                ", requires=" + requires +
                ", min_amount=" + min_amount +
                ", ignore_discounts=" + ignore_discounts +
                '}';
    }
}
