package com.azfit.Model.ShippingMethod;

public class Requires {

    private String id;

    private String label;

    private String description;

    private String type;

    private String value;

    private String defaults;

    private String tip;

    private String placeholder;

    private Option options;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setLabel(String label){
        this.label = label;
    }
    public String getLabel(){
        return this.label;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getType(){
        return this.type;
    }
    public void setValue(String value){
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
    public void setDefault(String defaults){
        this.defaults = defaults;
    }
    public String getDefault(){
        return this.defaults;
    }
    public void setTip(String tip){
        this.tip = tip;
    }
    public String getTip(){
        return this.tip;
    }
    public void setPlaceholder(String placeholder){
        this.placeholder = placeholder;
    }
    public String getPlaceholder(){
        return this.placeholder;
    }
    public void setOption(Option options){
        this.options = options;
    }
    public Option getOption(){
        return this.options;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                ", defaults='" + defaults + '\'' +
                ", tip='" + tip + '\'' +
                ", placeholder='" + placeholder + '\'' +
                ", options=" + options +
                '}';
    }
}
