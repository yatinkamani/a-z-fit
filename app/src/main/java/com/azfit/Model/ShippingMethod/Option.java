package com.azfit.Model.ShippingMethod;

public class Option {

//    private String ;

    private String coupon;

    private String min_amount;

    private String either;

    private String both;

    /*public void set(String ){
        this. = ;
    }
    public String get(){
        return this.;
    }*/
    public void setCoupon(String coupon){
        this.coupon = coupon;
    }
    public String getCoupon(){
        return this.coupon;
    }
    public void setMin_amount(String min_amount){
        this.min_amount = min_amount;
    }
    public String getMin_amount(){
        return this.min_amount;
    }
    public void setEither(String either){
        this.either = either;
    }
    public String getEither(){
        return this.either;
    }
    public void setBoth(String both){
        this.both = both;
    }
    public String getBoth(){
        return this.both;
    }

    @Override
    public String toString() {
        return "{" +
                "coupon='" + coupon + '\'' +
                ", min_amount='" + min_amount + '\'' +
                ", either='" + either + '\'' +
                ", both='" + both + '\'' +
                '}';
    }
}
