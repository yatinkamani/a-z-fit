package com.azfit.Model.ShippingMethod;

public class Shipping {

    private int id;

    private int instance_id;

    private String title;

    private int order;

    private boolean enabled;

    private String method_id;

    private String method_title;

    private String method_description;

    private Settings settings;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setInstance_id(int instance_id){
        this.instance_id = instance_id;
    }
    public int getInstance_id(){
        return this.instance_id;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setOrder(int order){
        this.order = order;
    }
    public int getOrder(){
        return this.order;
    }
    public void setEnabled(boolean enabled){
        this.enabled = enabled;
    }
    public boolean getEnabled(){
        return this.enabled;
    }
    public void setMethod_id(String method_id){
        this.method_id = method_id;
    }
    public String getMethod_id(){
        return this.method_id;
    }
    public void setMethod_title(String method_title){
        this.method_title = method_title;
    }
    public String getMethod_title(){
        return this.method_title;
    }
    public void setMethod_description(String method_description){
        this.method_description = method_description;
    }
    public String getMethod_description(){
        return this.method_description;
    }
    public void setSettings(Settings settings){
        this.settings = settings;
    }
    public Settings getSettings(){
        return this.settings;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", instance_id=" + instance_id +
                ", title='" + title + '\'' +
                ", order=" + order +
                ", enabled=" + enabled +
                ", method_id='" + method_id + '\'' +
                ", method_title='" + method_title + '\'' +
                ", method_description='" + method_description + '\'' +
                ", settings=" + settings +
                '}';
    }
}
