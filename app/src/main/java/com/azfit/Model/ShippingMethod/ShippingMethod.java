package com.azfit.Model.ShippingMethod;

import java.util.List;

public class ShippingMethod {

    private List<Shipping> shipping;

    public void setShipping(List<Shipping> shipping){
        this.shipping = shipping;
    }

    public List<Shipping> getShipping(){
        return this.shipping;
    }

    @Override
    public String toString() {
        return "ShippingLocation{" +
                "shipping=" + shipping +
                '}';
    }
}
