package com.azfit.Model;

import java.util.List;

public class PriceDataModel {

    String course_id, product_id, name, plane_mode, price, price_mode;
    List<ServicesModel> list;

    public PriceDataModel(String id, String name, String plane_mode, String price, String price_mode, List<ServicesModel> list) {
        this.course_id = id;
        this.name = name;
        this.plane_mode = plane_mode;
        this.price = price;
        this.price_mode = price_mode;
        this.list = list;
    }

    public PriceDataModel(String course_id, String product_id, String name, String plane_mode, String price, String price_mode) {
        this.product_id = product_id;
        this.course_id = course_id;
        this.name = name;
        this.plane_mode = plane_mode;
        this.price = price;
        this.price_mode = price_mode;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlane_mode() {
        return plane_mode;
    }

    public void setPlane_mode(String plane_mode) {
        this.plane_mode = plane_mode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_mode() {
        return price_mode;
    }

    public void setPrice_mode(String price_mode) {
        this.price_mode = price_mode;
    }

    public List<ServicesModel> getList() {
        return list;
    }

    public void setList(List<ServicesModel> list) {
        this.list = list;
    }
}
