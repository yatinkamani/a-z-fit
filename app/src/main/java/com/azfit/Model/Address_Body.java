package com.azfit.Model;

import com.google.gson.annotations.SerializedName;

public class Address_Body {

    @SerializedName("first_name")
    String first_name;
    @SerializedName("last_name")
    String last_name;
    @SerializedName("company")
    String company;
    @SerializedName("address_1")
    String address_1;
    @SerializedName("address_2")
    String address_2;
    @SerializedName("city")
    String city;
    @SerializedName("postcode")
    String postcode;
    @SerializedName("country")
    String country;
    @SerializedName("state")
    String state;
    @SerializedName("email")
    String email;
    @SerializedName("phone")
    String phone;

    public Address_Body(String first_name, String last_name, String company, String address_1, String address_2, String city, String postcode, String country, String state, String email, String phone) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.company = company;
        this.address_1 = address_1;
        this.address_2 = address_2;
        this.city = city;
        this.postcode = postcode;
        this.country = country;
        this.state = state;
        this.email = email;
        this.phone = phone;
    }
}
