package com.azfit.Model.Category;

import com.google.gson.annotations.SerializedName;

public class CategoryData {

    private String parent;
    private String term_taxonomy_id;
    @SerializedName(value = "count", alternate = {"count1", "count2"})
    private String count;
    private String name;
    private String description;
    private String term_id;
    private String taxonomy;
    private String term_group;
    private String slug;
    private String meta_value;

    public String getMeta_value() {
        return meta_value;
    }

    public void setMeta_value(String meta_value) {
        this.meta_value = meta_value;
    }

    public String getParent ()
    {
        return parent;
    }

    public void setParent (String parent)
    {
        this.parent = parent;
    }

    public String getTerm_taxonomy_id ()
    {
        return term_taxonomy_id;
    }

    public void setTerm_taxonomy_id (String term_taxonomy_id) {
        this.term_taxonomy_id = term_taxonomy_id;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getTerm_id ()
    {
        return term_id;
    }

    public void setTerm_id (String term_id)
    {
        this.term_id = term_id;
    }

    public String getTaxonomy ()
    {
        return taxonomy;
    }

    public void setTaxonomy (String taxonomy)
    {
        this.taxonomy = taxonomy;
    }

    public String getTerm_group ()
    {
        return term_group;
    }

    public void setTerm_group (String term_group)
    {
        this.term_group = term_group;
    }

    public String getSlug ()
    {
        return slug;
    }

    public void setSlug (String slug)
    {
        this.slug = slug;
    }

    @Override
    public String toString() {
        return "{" +
                "parent='" + parent + '\'' +
                ", term_taxonomy_id='" + term_taxonomy_id + '\'' +
                ", count='" + count + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", term_id='" + term_id + '\'' +
                ", taxonomy='" + taxonomy + '\'' +
                ", term_group='" + term_group + '\'' +
                ", slug='" + slug + '\'' +
                ", meta_value='" + meta_value + '\'' +
                '}';
    }
}
