package com.azfit.Model.Category;

import java.util.List;

public class CategoryModel {

    String status;
    String message;
    List<CategoryData> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CategoryData> getResult() {
        return result;
    }

    public void setResult(List<CategoryData> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }

}
