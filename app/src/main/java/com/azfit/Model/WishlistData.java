package com.azfit.Model;

public class WishlistData {


    String id;
    String name;
    String image;
    String rate;
    String categories;
    String level;
    String course_id;
    String cat_id;
    String course_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public WishlistData(String id, String name, String image, String rate, String categories, String level) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.rate = rate;
        this.categories = categories;
        this.level = level;
    }

    public WishlistData(String id, String name, String image, String rate, String categories, String level, String course_id, String cat_id, String course_name) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.rate = rate;
        this.categories = categories;
        this.level = level;
        this.course_id = course_id;
        this.cat_id = cat_id;
        this.course_name = course_name;
    }
}
