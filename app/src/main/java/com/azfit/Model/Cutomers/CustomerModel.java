package com.azfit.Model.Cutomers;

public class CustomerModel {

    private String total_spent;

    private String role;

    private String last_order_id;

    private String created_at;

    private String last_name;

    private String last_order_date;

    private Billing_address billing_address;

    private String orders_count;

    private String avatar_url;

    private String id;

    private Shipping_address shipping_address;

    private String first_name;

    private String email;

    private String username;

    public String getTotal_spent ()
    {
        return total_spent;
    }

    public void setTotal_spent (String total_spent)
    {
        this.total_spent = total_spent;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getLast_order_id ()
    {
        return last_order_id;
    }

    public void setLast_order_id (String last_order_id)
    {
        this.last_order_id = last_order_id;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getLast_order_date ()
    {
        return last_order_date;
    }

    public void setLast_order_date (String last_order_date)
    {
        this.last_order_date = last_order_date;
    }

    public Billing_address getBilling_address ()
    {
        return billing_address;
    }

    public void setBilling_address (Billing_address billing_address)
    {
        this.billing_address = billing_address;
    }

    public String getOrders_count ()
    {
        return orders_count;
    }

    public void setOrders_count (String orders_count)
    {
        this.orders_count = orders_count;
    }

    public String getAvatar_url ()
    {
        return avatar_url;
    }

    public void setAvatar_url (String avatar_url)
    {
        this.avatar_url = avatar_url;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Shipping_address getShipping_address ()
    {
        return shipping_address;
    }

    public void setShipping_address (Shipping_address shipping_address)
    {
        this.shipping_address = shipping_address;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    @Override
    public String toString() {
        return "{" +
                "total_spent='" + total_spent + '\'' +
                ", role='" + role + '\'' +
                ", last_order_id='" + last_order_id + '\'' +
                ", created_at='" + created_at + '\'' +
                ", last_name='" + last_name + '\'' +
                ", last_order_date='" + last_order_date + '\'' +
                ", billing_address=" + billing_address +
                ", orders_count='" + orders_count + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                ", id='" + id + '\'' +
                ", shipping_address=" + shipping_address +
                ", first_name='" + first_name + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
