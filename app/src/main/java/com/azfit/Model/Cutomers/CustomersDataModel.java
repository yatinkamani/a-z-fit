package com.azfit.Model.Cutomers;

public class CustomersDataModel {

    private String status;

    private String message;

    private CustomerModel customer;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomerModel getCustomer () {
        return customer;
    }

    public void setCustomer (CustomerModel customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return " {" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", customer=" + customer +
                '}';
    }
}
