package com.azfit.Model.AnnounceMentModel;

public class AnnouncementData {

    String ID;
    String post_title;
    String post_name;
    String post_content;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    @Override
    public String toString() {
        return ".{" +
                "ID='" + ID + '\'' +
                ", post_title='" + post_title + '\'' +
                ", post_name='" + post_name + '\'' +
                ", post_content='" + post_content + '\'' +
                '}';
    }
}
