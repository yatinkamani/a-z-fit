package com.azfit.Model.AnnounceMentModel;

import java.util.List;

public class AnnouncementModel {

    String status_code;
    String message ;
    List<AnnouncementData> data;

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnnouncementData> getData() {
        return data;
    }

    public void setData(List<AnnouncementData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "AnnouncementModel{" +
                "status_code='" + status_code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
