package com.azfit.Model.Sevices;

import android.graphics.drawable.Drawable;

public class ServiceData {

    String product_id;
    String service_id;
    String service_name;
    Drawable service_image;
    String service_description;

    public ServiceData() {
    }

    public ServiceData(String product_id, String service_id, String service_name, Drawable service_image, String service_description) {
        this.product_id = product_id;
        this.service_id = service_id;
        this.service_name = service_name;
        this.service_image = service_image;
        this.service_description = service_description;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public Drawable getService_image() {
        return service_image;
    }

    public void setService_image(Drawable service_image) {
        this.service_image = service_image;
    }

    public String getService_description() {
        return service_description;
    }

    public void setService_description(String service_description) {
        this.service_description = service_description;
    }
}
