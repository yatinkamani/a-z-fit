package com.azfit.Model.Course;

public class CourseData {

    private String comment_count;

    private String post_title;

    private String post_author;

    private String menu_order;

    private String pinged;

    private String post_modified_gmt;

    private String post_excerpt;

    private String comment_status;

    private String post_parent;

    private String post_mime_type;

    private String post_content;

    private String ping_status;

    private String post_name;

    private String post_date;

    private String post_password;

    private String guid;

    private String to_ping;

    private String post_modified;

    private String post_type;

    private String ID;

    private String post_status;

    private String post_content_filtered;

    private String post_date_gmt;

    private String _tutor_course_benefits;

    private String _course_duration;

    private String _tutor_course_target_audience;
    private String _tutor_course_requirements;
    private String _tutor_course_price_type;
    private String _tutor_course_level;
    private String _tutor_course_product_id;
    private String course_image;
    private String display_name;
    private String user_login;
    private String no_of_lesson;

    private String complete_lesson;
    private String status;


    public String getComment_count ()
    {
        return comment_count;
    }

    public void setComment_count (String comment_count)
    {
        this.comment_count = comment_count;
    }

    public String getPost_title ()
    {
        return post_title;
    }

    public void setPost_title (String post_title)
    {
        this.post_title = post_title;
    }

    public String getPost_author ()
    {
        return post_author;
    }

    public void setPost_author (String post_author)
    {
        this.post_author = post_author;
    }

    public String getMenu_order ()
    {
        return menu_order;
    }

    public void setMenu_order (String menu_order)
    {
        this.menu_order = menu_order;
    }

    public String getPinged ()
    {
        return pinged;
    }

    public void setPinged (String pinged)
    {
        this.pinged = pinged;
    }

    public String getPost_modified_gmt ()
    {
        return post_modified_gmt;
    }

    public void setPost_modified_gmt (String post_modified_gmt) {
        this.post_modified_gmt = post_modified_gmt;
    }

    public String getPost_excerpt ()
    {
        return post_excerpt;
    }

    public void setPost_excerpt (String post_excerpt)
    {
        this.post_excerpt = post_excerpt;
    }

    public String getComment_status ()
    {
        return comment_status;
    }

    public void setComment_status (String comment_status)
    {
        this.comment_status = comment_status;
    }

    public String getPost_parent ()
    {
        return post_parent;
    }

    public void setPost_parent (String post_parent)
    {
        this.post_parent = post_parent;
    }

    public String getPost_mime_type ()
    {
        return post_mime_type;
    }

    public void setPost_mime_type (String post_mime_type)
    {
        this.post_mime_type = post_mime_type;
    }

    public String getPost_content ()
    {
        return post_content;
    }

    public void setPost_content (String post_content)
    {
        this.post_content = post_content;
    }

    public String getPing_status ()
    {
        return ping_status;
    }

    public void setPing_status (String ping_status)
    {
        this.ping_status = ping_status;
    }

    public String getPost_name ()
    {
        return post_name;
    }

    public void setPost_name (String post_name)
    {
        this.post_name = post_name;
    }

    public String getPost_date ()
    {
        return post_date;
    }

    public void setPost_date (String post_date)
    {
        this.post_date = post_date;
    }

    public String getPost_password ()
    {
        return post_password;
    }

    public void setPost_password (String post_password)
    {
        this.post_password = post_password;
    }

    public String getGuid ()
    {
        return guid;
    }

    public void setGuid (String guid)
    {
        this.guid = guid;
    }

    public String getTo_ping ()
    {
        return to_ping;
    }

    public void setTo_ping (String to_ping)
    {
        this.to_ping = to_ping;
    }

    public String getPost_modified ()
    {
        return post_modified;
    }

    public void setPost_modified (String post_modified)
    {
        this.post_modified = post_modified;
    }

    public String getPost_type ()
    {
        return post_type;
    }

    public void setPost_type (String post_type)
    {
        this.post_type = post_type;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getPost_status ()
    {
        return post_status;
    }

    public void setPost_status (String post_status)
    {
        this.post_status = post_status;
    }

    public String getPost_content_filtered ()
    {
        return post_content_filtered;
    }

    public void setPost_content_filtered (String post_content_filtered) {
        this.post_content_filtered = post_content_filtered;
    }

    public String getPost_date_gmt ()
    {
        return post_date_gmt;
    }

    public void setPost_date_gmt (String post_date_gmt)
    {
        this.post_date_gmt = post_date_gmt;
    }

    public String get_tutor_course_benefits() {
        return _tutor_course_benefits;
    }

    public void set_tutor_course_benefits(String _tutor_course_benefits) {
        this._tutor_course_benefits = _tutor_course_benefits;
    }

    public String get_course_duration() {
        return _course_duration;
    }

    public void set_course_duration(String _course_duration) {
        this._course_duration = _course_duration;
    }

    public String get_tutor_course_target_audience() {
        return _tutor_course_target_audience;
    }

    public void set_tutor_course_target_audience(String _tutor_course_target_audience) {
        this._tutor_course_target_audience = _tutor_course_target_audience;
    }

    public String get_tutor_course_requirements() {
        return _tutor_course_requirements;
    }

    public void set_tutor_course_requirements(String _tutor_course_requirements) {
        this._tutor_course_requirements = _tutor_course_requirements;
    }

    public String get_tutor_course_price_type() {
        return _tutor_course_price_type;
    }

    public void set_tutor_course_price_type(String _tutor_course_price_type) {
        this._tutor_course_price_type = _tutor_course_price_type;
    }

    public String get_tutor_course_level() {
        return _tutor_course_level;
    }

    public void set_tutor_course_level(String _tutor_course_level) {
        this._tutor_course_level = _tutor_course_level;
    }

    public String get_tutor_course_product_id() {
        return _tutor_course_product_id;
    }

    public void set_tutor_course_product_id(String _tutor_course_product_id) {
        this._tutor_course_product_id = _tutor_course_product_id;
    }

    public String getCourse_image() {
        return course_image;
    }

    public void setCourse_image(String course_image) {
        this.course_image = course_image;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getNo_of_lesson() {
        return no_of_lesson;
    }

    public void setNo_of_lesson(String no_of_lesson) {
        this.no_of_lesson = no_of_lesson;
    }

    public String getComplete_lesson() {
        return complete_lesson;
    }

    public void setComplete_lesson(String complete_lesson) {
        this.complete_lesson = complete_lesson;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
                "comment_count='" + comment_count + '\'' +
                ", post_title='" + post_title + '\'' +
                ", post_author='" + post_author + '\'' +
                ", menu_order='" + menu_order + '\'' +
                ", pinged='" + pinged + '\'' +
                ", post_modified_gmt='" + post_modified_gmt + '\'' +
                ", post_excerpt='" + post_excerpt + '\'' +
                ", comment_status='" + comment_status + '\'' +
                ", post_parent='" + post_parent + '\'' +
                ", post_mime_type='" + post_mime_type + '\'' +
                ", post_content='" + post_content + '\'' +
                ", ping_status='" + ping_status + '\'' +
                ", post_name='" + post_name + '\'' +
                ", post_date='" + post_date + '\'' +
                ", post_password='" + post_password + '\'' +
                ", guid='" + guid + '\'' +
                ", to_ping='" + to_ping + '\'' +
                ", post_modified='" + post_modified + '\'' +
                ", post_type='" + post_type + '\'' +
                ", ID='" + ID + '\'' +
                ", post_status='" + post_status + '\'' +
                ", post_content_filtered='" + post_content_filtered + '\'' +
                ", post_date_gmt='" + post_date_gmt + '\'' +
                ", _tutor_course_benefits='" + _tutor_course_benefits + '\'' +
                ", _course_duration='" + _course_duration + '\'' +
                ", _tutor_course_target_audience='" + _tutor_course_target_audience + '\'' +
                ", _tutor_course_requirements='" + _tutor_course_requirements + '\'' +
                ", _tutor_course_price_type='" + _tutor_course_price_type + '\'' +
                ", _tutor_course_level='" + _tutor_course_level + '\'' +
                ", _tutor_course_product_id='" + _tutor_course_product_id + '\'' +
                ", course_image='" + course_image + '\'' +
                ", display_name='" + display_name + '\'' +
                ", user_login='" + user_login + '\'' +
                ", no_lesson_count='" + no_of_lesson + '\'' +
                '}';
    }
}
