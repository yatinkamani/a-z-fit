package com.azfit.Model.Course;

import java.util.List;

public class Course {

    private List<CourseData> result;

    private String message;

    private String status;

    public List<CourseData> getResult ()
    {
        return result;
    }

    public void setResult (List<CourseData> result)
    {
        this.result = result;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", message = "+message+", status = "+status+"]";
    }

}
