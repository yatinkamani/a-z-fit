package com.azfit.Model.Details;

public class SubTopicModel {

    String topic_id;
    String subTopic_id;
    String subTopic_name;
    String subTopic_duration;

    public SubTopicModel() {
    }

    public SubTopicModel(String topic_id, String subTopic_id, String subTopic_name, String subTopic_duration) {
        this.topic_id = topic_id;
        this.subTopic_id = subTopic_id;
        this.subTopic_name = subTopic_name;
        this.subTopic_duration = subTopic_duration;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getSubTopic_id() {
        return subTopic_id;
    }

    public void setSubTopic_id(String subTopic_id) {
        this.subTopic_id = subTopic_id;
    }

    public String getSubTopic_name() {
        return subTopic_name;
    }

    public void setSubTopic_name(String subTopic_name) {
        this.subTopic_name = subTopic_name;
    }

    public String getSubTopic_duration() {
        return subTopic_duration;
    }

    public void setSubTopic_duration(String subTopic_duration) {
        this.subTopic_duration = subTopic_duration;
    }
}
