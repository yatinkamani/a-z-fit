package com.azfit.Model.Details;

import java.util.List;

public class TopicModel {

    String id;
    String topicName;
    String topicDescription;
    List<SubTopicModel> topicModels;

    public TopicModel() {
    }

    public TopicModel(String id, String topicName, String topicDescription, List<SubTopicModel> topicModels) {
        this.id = id;
        this.topicName = topicName;
        this.topicDescription = topicDescription;
        this.topicModels = topicModels;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicDescription() {
        return topicDescription;
    }

    public void setTopicDescription(String topicDescription) {
        this.topicDescription = topicDescription;
    }

    public List<SubTopicModel> getTopicModels() {
        return topicModels;
    }

    public void setTopicModels(List<SubTopicModel> topicModels) {
        this.topicModels = topicModels;
    }
}
