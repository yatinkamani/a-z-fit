package com.azfit.Model.Topic;

public class TopicData {

    public String ID;
    public String post_title;
    public String post_content;
    public String post_name;

    public String getiD() {
        return ID;
    }

    public void setiD(String iD) {
        this.ID = iD;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    @Override
    public String toString() {
        return "{" +
                "iD='" + ID + '\'' +
                ", post_title='" + post_title + '\'' +
                ", post_content='" + post_content + '\'' +
                ", post_name='" + post_name + '\'' +
                '}';
    }
}
