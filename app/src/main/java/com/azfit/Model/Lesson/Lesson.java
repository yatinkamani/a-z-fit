package com.azfit.Model.Lesson;

import java.util.List;

public class Lesson {

    private List<LessonData> result;

    private String message;

    private String status;

    public List<LessonData> getResult ()
    {
        return result;
    }

    public void setResult (List<LessonData> result)
    {
        this.result = result;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", message = "+message+", status = "+status+"]";
    }
}
