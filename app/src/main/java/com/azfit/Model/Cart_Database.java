package com.azfit.Model;

public class Cart_Database {

    String id, quantity, product_id, total_product;

    public Cart_Database() {
    }

    public Cart_Database(String id, String quantity, String product_id, String total_product) {
        this.id = id;
        this.quantity = quantity;
        this.product_id = product_id;
        this.total_product = total_product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getTotal_product() {
        return total_product;
    }

    public void setTotal_product(String total_product) {
        this.total_product = total_product;
    }
}
