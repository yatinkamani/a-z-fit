package com.azfit.CartData;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.azfit.CourseDatabase.DatabaseHelperCourse;
import com.azfit.Model.Cart_Database;

import java.util.ArrayList;
import java.util.List;

public class DBManager {

    private Databasehelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new Databasehelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String total, String product_id,String Quantity) {
        SQLiteDatabase db = new Databasehelper(context).getWritableDatabase();
        //adding user name in users table
        ContentValues contentValue = new ContentValues();
        contentValue.put(Databasehelper.CART_TOTAL, total);
        contentValue.put(Databasehelper.PRODUCT_ID, product_id);
        contentValue.put(Databasehelper.QUANTITY, Quantity);
        db.insert(Databasehelper.TABLE_NAME, null, contentValue);
    }

    public List<Cart_Database> fetch() {
        SQLiteDatabase db = new Databasehelper(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + Databasehelper.TABLE_NAME;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Cart_Database> cart_databases = new ArrayList<>();
        if (cursor != null) {

        if (cursor.moveToFirst()){
            do{
                String id = cursor.getString(cursor.getColumnIndex(Databasehelper._ID));
                String Quantity = cursor.getString(cursor.getColumnIndex(Databasehelper.QUANTITY));
                String Product_id = cursor.getString(cursor.getColumnIndex(Databasehelper.PRODUCT_ID));
                String Total = cursor.getString(cursor.getColumnIndex(Databasehelper.CART_TOTAL));
                cart_databases.add(new Cart_Database(id,Quantity,Product_id,Total));
            }while (cursor.moveToNext());
        }
        }
        return cart_databases;
    }

    public int update( String product_id, String Quantity) {
        SQLiteDatabase db = new Databasehelper(context).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Databasehelper.QUANTITY, Quantity);
        int i = db.update(Databasehelper.TABLE_NAME, contentValues,  Databasehelper.PRODUCT_ID + " = " + product_id, null);

        return i;
    }

    public void Delete(String product_id) {
        SQLiteDatabase db = new Databasehelper(context).getWritableDatabase();
        db.delete(Databasehelper.TABLE_NAME, Databasehelper.PRODUCT_ID + "=" + product_id, null);
    }

    public void removeTable(){
        SQLiteDatabase db = new Databasehelper(context).getWritableDatabase();
        db.delete(Databasehelper.TABLE_NAME,null,null);
    }
    public void removeDatabase() {
        context.deleteDatabase(Databasehelper.DB_NAME);
    }

}
