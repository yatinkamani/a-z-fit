package com.azfit.CartData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelpers extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "FULL_CART";

    // Table columns
    public static final String _ID = "_id";
    public static final String QUANTITY = "quantity";
    public static final String CART_TOTAL = "CartTotal";
    public static final String PRODUCT_ID = "product_id";

    // Table Name
    public static final String TABLE_NAME_PRODUCT = "PRODUCT";

    // Table columns
    public static final String P_ID                 = "id";
    public static final String P_TITLE              = "title";
    public static final String P_type               = "type";
    public static final String P_status             = "status";
    public static final String P_permalink          = "permalink";
    public static final String P_sku                = "sku";
    public static final String P_price              = "price";
    public static final String P_regular_price      = "regular_price";
    public static final String P_sale_price         = "sale_price";
    public static final String P_price_html         = "price_html";
    public static final String P_manage_stock       = "manage_stock";
    public static final String P_stock_quantity     = "stock_quantity";
    public static final String P_stock_status       = "stock_status";
    public static final String P_product_url        = "product_url";
    public static final String P_weight             = "weight";
    public static final String P_description        = "description";
    public static final String P_short_description  = "short_description";
    public static final String P_average_rating     = "average_rating";
    public static final String P_rating_count       = "rating_count";
    public static final String P_featured_src       = "featured_src";
    public static final String P_downloadable       = "downloadable";
    public static final String P_virtuals           = "virtuals";
    public static final String P_on_sale            = "on_sale";
    public static final String P_featured           = "featured";

    // Table Name
    public static final String TABLE_NAME_IMAGES = "CART_IMAGES";

    // Table columns
    public static final String I_ID         = "I_id";
    public static final String I_SRC        = "I_src";
    public static final String I_Thumbnail  = "I_Thumb";
    public static final String I_TITLE      = "I_title";

    // Table Name
    public static final String TABLE_NAME_CATEGORY = "CATEGORY";

    // Table columns
    public static final String C_ID     = "c_id";
    public static final String C_NAME   = "c_name";

    // Table Name
    public static final String TABLE_NAME_RELATED_ID = "RELATED_ID";

    // Table columns
    public static final String R_ID = "c_id";

    // Table Name
    public static final String TABLE_NAME_PVM = "PVM";

    // Table columns
    public static final String V_ID                 = "V_id";
    public static final String V_regular_price      = "V_regular_price";
    public static final String V_price              = "V_price";
    public static final String V_name               = "V_name";
    public static final String V_permalink          = "V_permalink";
    public static final String V_size_name          = "V_size_name";

    // Table Name
    public static final String TABLE_NAME_ATTRIBUTE = "ATRIBUTES";

    // Table columns
    public static final String A_name       = "A_name";
    public static final String A_option     = "A_option";

    // Table Name
    public static final String TABLE_NAME_MARGIN = "MARGIN";

    // Table columns
    public static final String M_minQuantity    = "A_minQuantity";
    public static final String M_maxQuantity    = "A_maxQuantity";
    public static final String M_unitPrice      = "A_unitPrice";
    public static final String M_type           = "A_type";

    // Database Information
    static final String DB_NAME = "CART_ITEM_DATABASE.DB";

    // database version
    static final int DB_VERSION = 2;

    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + QUANTITY + " TEXT NOT NULL," + PRODUCT_ID + " TEXT NOT NULL, " + CART_TOTAL + " TEXT);";

    private static final String CREATE_TABLE_PRODUCT = "create table " + TABLE_NAME_PRODUCT + "(" + P_ID
            + " TEXT NOT NULL, " + P_TITLE + " TEXT NOT NULL," + P_type + " TEXT, " + P_status
            + " TEXT, " + P_permalink + " TEXT," + P_sku + " TEXT, " + P_price
            + " TEXT, " + P_regular_price + " TEXT," + P_sale_price + " TEXT, " + P_price_html
            + " TEXT," + P_manage_stock + " TEXT," + P_stock_quantity + " TEXT, " + P_stock_status + " TEXT, " + P_product_url
            + " TEXT," + P_weight + " TEXT," + P_description + " TEXT, " + P_short_description
            + " TEXT," + P_average_rating + " TEXT," + P_rating_count + " TEXT, " + P_featured_src
            + " TEXT," + P_downloadable + " TEXT," + P_virtuals + " TEXT, " + P_on_sale
            + " TEXT," + P_featured + " TEXT );";

    private static final String CREATE_TABLE_IMAGES = "create table " + TABLE_NAME_IMAGES + "(" + P_ID
            + " TEXT NOT NULL, " + I_ID + " TEXT NOT NULL, " + I_TITLE + " TEXT, " + I_SRC + " TEXT ," + I_Thumbnail + " TEXT );";

    private static final String CREATE_TABLE_CATEGORY = "create table " + TABLE_NAME_CATEGORY + "(" + P_ID
            + " TEXT, " + C_ID + " TEXT, " + C_NAME + " TEXT);";

    private static final String CREATE_TABLE_RELATED_ID = "create table " + TABLE_NAME_RELATED_ID + "(" + P_ID
            + " TEXT NOT NULL, " + R_ID + " TEXT NOT NULL);";

    private static final String CREATE_TABLE_PVM = "create table " + TABLE_NAME_PVM + "(" + P_ID
            + " TEXT NOT NULL, " + V_ID + " INTEGER NOT NULL, " + V_regular_price + " TEXT NOT NULL, " + V_price
            + " TEXT NOT NULL, " + V_name + " TEXT NOT NULL, " + V_size_name + " TEXT NOT NULL, " + V_permalink + " TEXT );";

    private static final String CREATE_TABLE_ATTRIBUTE = "create table " + TABLE_NAME_ATTRIBUTE + "(" + P_ID
            + " TEXT NOT NULL, " + A_name + " TEXT NOT NULL, " + A_option + " TEXT NOT NULL);";

    private static final String CREATE_TABLE_MARGIN = "create table " + TABLE_NAME_MARGIN + "(" + P_ID
            + " TEXT NOT NULL, " + M_maxQuantity + " TEXT NOT NULL, " + M_minQuantity
            + " TEXT NOT NULL, " + M_unitPrice + " TEXT NOT NULL, " + M_type + " TEXT NOT NULL);";

    // Creating table query

    public DatabaseHelpers(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_PRODUCT);
        db.execSQL(CREATE_TABLE_IMAGES);
        db.execSQL(CREATE_TABLE_CATEGORY);
        db.execSQL(CREATE_TABLE_RELATED_ID);
//        db.execSQL(CREATE_TABLE_PVM);
//        db.execSQL(CREATE_TABLE_ATTRIBUTE);
//        db.execSQL(CREATE_TABLE_MARGIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PRODUCT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_IMAGES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_RELATED_ID);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PVM);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ATTRIBUTE);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MARGIN);
        onCreate(db);
    }
}
