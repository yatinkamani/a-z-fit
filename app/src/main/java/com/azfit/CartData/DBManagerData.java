package com.azfit.CartData;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.azfit.CourseDatabase.DatabaseHelperCourse;
import com.azfit.Model.CartItem;
import com.azfit.Model.Cart_Database;
import com.azfit.Model.Product.CategoriesData;
import com.azfit.Model.Product.ImagesData;
import com.azfit.Model.Product.ProductData;

import java.util.ArrayList;
import java.util.List;

public class DBManagerData {

    private DatabaseHelpers dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManagerData(Context c) {
        context = c;
    }

    public DBManagerData open() throws SQLException {
        dbHelper = new DatabaseHelpers(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String total, String product_id, String Quantity, ProductData product) {
        SQLiteDatabase db = new DatabaseHelpers(context).getWritableDatabase();
        //adding user name in users table

        if (product != null){

            ContentValues contentValue = new ContentValues();
            contentValue.put(DatabaseHelpers.CART_TOTAL, total);
            contentValue.put(DatabaseHelpers.PRODUCT_ID, product_id);
            contentValue.put(DatabaseHelpers.QUANTITY, Quantity);
            db.insert(DatabaseHelpers.TABLE_NAME, null, contentValue);

            ContentValues product_data = new ContentValues();
            product_data.put(DatabaseHelpers.P_ID, product.getId().equals("null") ? " " : product.getId());
            product_data.put(DatabaseHelpers.P_TITLE, product.getName().equals("null") ? " " : product.getName());
            product_data.put(DatabaseHelpers.P_type, product.getType().equals("null") ? " " : product.getType());
            product_data.put(DatabaseHelpers.P_status, product.getStatus().equals("null") ? " " : product.getStatus());
            product_data.put(DatabaseHelpers.P_permalink, product.getPermalink().equals("null") ? " " : product.getPermalink());
            product_data.put(DatabaseHelpers.P_sku, product.getSku().equals("null") ? " " : product.getSku());
            product_data.put(DatabaseHelpers.P_price, product.getPrice().equals("null") ? " " : product.getPrice());
            product_data.put(DatabaseHelpers.P_regular_price, product.getRegular_price().equals("null") ? " " : product.getRegular_price());
            product_data.put(DatabaseHelpers.P_sale_price, product.getSale_price().equals("null") ? " " : product.getSale_price());
            product_data.put(DatabaseHelpers.P_price_html, product.getPrice_html().equals("null") ? " " : product.getPrice_html());
            product_data.put(DatabaseHelpers.P_manage_stock, product.getManage_stock().equals("null") ? " " : product.getManage_stock());
            product_data.put(DatabaseHelpers.P_stock_quantity, String.valueOf(product.getStock_quantity()).equals("null") ? " " : product.getStock_quantity());
            product_data.put(DatabaseHelpers.P_stock_status, product.getStock_status().equals("null") ? "" : product.getStock_status());
            product_data.put(DatabaseHelpers.P_product_url,product.getExternal_url().equals("null") ? " " : product.getExternal_url());
            product_data.put(DatabaseHelpers.P_weight,product.getWeight().equals("null") ? " " : product.getWeight());
            product_data.put(DatabaseHelpers.P_description,product.getDescription().equals("null") ? " " : product.getDescription());
            product_data.put(DatabaseHelpers.P_short_description,product.getShort_description().equals("null") ? " " : product.getShort_description());
            product_data.put(DatabaseHelpers.P_average_rating,product.getAverage_rating().equals("null") ? " " : product.getAverage_rating());
            product_data.put(DatabaseHelpers.P_rating_count,product.getRating_count().equals("null") ? " " : product.getRating_count());
            product_data.put(DatabaseHelpers.P_featured_src,product.getFeatured().equals("null") ? " " : product.getFeatured());
            product_data.put(DatabaseHelpers.P_downloadable,product.getDownloadable().equals("null") ? " " : product.getDownloadable());
            product_data.put(DatabaseHelpers.P_virtuals,product.getVirtual().equals("null") ? " " : product.getVirtual());
            product_data.put(DatabaseHelpers.P_on_sale,product.getOn_sale().equals("null") ? " " : product.getOn_sale());
            product_data.put(DatabaseHelpers.P_featured,product.getFeatured().equals("null") ? " " : product.getFeatured());
            db.insert(DatabaseHelpers.TABLE_NAME_PRODUCT,null, product_data);

            if (product.getImages() != null){
                for (int i=0;i<product.getImages().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID, product_id);
                    contentValues.put(DatabaseHelpers.I_ID,product.getImages().get(i).getId());
                    contentValues.put(DatabaseHelpers.I_SRC,product.getImages().get(i).getSrc());
                    contentValues.put(DatabaseHelpers.I_Thumbnail,product.getImages().get(i).getThumbnail());
                    contentValues.put(DatabaseHelpers.I_TITLE,product.getImages().get(i).getName());
                    db.insert(DatabaseHelpers.TABLE_NAME_IMAGES, null, contentValues);
                }
            }

            if (product.getCategories() != null){
                for (int i=0;i<product.getCategories().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID,product_id);
                    contentValues.put(DatabaseHelpers.C_ID,product.getCategories().get(i).getId());
                    contentValues.put(DatabaseHelpers.C_NAME,product.getCategories().get(i).getName());
                    db.insert(DatabaseHelpers.TABLE_NAME_CATEGORY, null, contentValues);
                }
            }

            if (product.getRelated_ids() != null){
                for (int i=0;i<product.getRelated_ids().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID,product_id);
                    contentValues.put(DatabaseHelpers.R_ID,product.getRelated_ids().get(i));
                    db.insert(DatabaseHelpers.TABLE_NAME_RELATED_ID, null, contentValues);
                }
            }

            /*if (product.getAttributes() != null){
                for (int i=0;i<product.getAttributes().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID,product_id);
                    contentValues.put(DatabaseHelpers.A_name, ""+product.getAttributes().get(i));
                    contentValues.put(DatabaseHelpers.A_option, ""+product.getAttributes().get(i));
                    db.insert(DatabaseHelpers.TABLE_NAME_ATTRIBUTE, null, contentValues);
                }
            }*/

            /*if (product.getVariations() != null){
                for (int i=0;i<product.getVariations().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID,product_id);
                    contentValues.put(DatabaseHelpers.V_ID, ""+product.getVariations().get(i));
                    contentValues.put(DatabaseHelpers.V_name, ""+product.getVariations().get(i));
                    contentValues.put(DatabaseHelpers.V_price,product.getVariations().get(i));
                    contentValues.put(DatabaseHelpers.V_regular_price,product.getVariations().get(i));
                    contentValues.put(DatabaseHelpers.V_size_name,product.getVariations().get(i));
                    contentValues.put(DatabaseHelpers.V_permalink,product.getVariations().get(i));
                    db.insert(DatabaseHelpers.TABLE_NAME_PVM, null, contentValues);
                }
            }*/

            /*if (product.getpMarginValues() != null){
                for (int i=0;i<product.getpMarginValues().size();i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DatabaseHelpers.P_ID,product_id);
                    contentValues.put(DatabaseHelpers.M_maxQuantity,product.getpMarginValues().get(i).getMaxQuantity());
                    contentValues.put(DatabaseHelpers.M_minQuantity,product.getpMarginValues().get(i).getMinQuantity());
                    contentValues.put(DatabaseHelpers.M_type,product.getpMarginValues().get(i).getType());
                    contentValues.put(DatabaseHelpers.M_unitPrice,product.getpMarginValues().get(i).getUnitPrice());
                    db.insert(DatabaseHelpers.TABLE_NAME_MARGIN, null, contentValues);
                }
            }*/
        }else {
            ContentValues contentValue = new ContentValues();
            contentValue.put(DatabaseHelpers.CART_TOTAL, total);
            contentValue.put(DatabaseHelpers.PRODUCT_ID, product_id);
            contentValue.put(DatabaseHelpers.QUANTITY, Quantity);
            db.insert(DatabaseHelpers.TABLE_NAME, null, contentValue);
        }
    }

    public List<CartItem> fetch() {
        SQLiteDatabase db = new DatabaseHelpers(context).getReadableDatabase();
        List<CartItem> cart_item = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME;
        String selectQuery_product = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_PRODUCT;
        String selectQuery_images = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_IMAGES;
//        String selectQuery_attribute = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_ATTRIBUTE;
//        String selectQuery_margin = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_MARGIN;
//        String selectQuery_pvm = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_PVM;
        String selectQuery_related_id = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_RELATED_ID;
        String selectQuery_category = "SELECT * FROM " + DatabaseHelpers.TABLE_NAME_CATEGORY;

        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Cart_Database> cart_databases = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()){
                do{
                    String id = cursor.getString(cursor.getColumnIndex(DatabaseHelpers._ID));
                    String Quantity = cursor.getString(cursor.getColumnIndex(DatabaseHelpers.QUANTITY));
                    String Product_id = cursor.getString(cursor.getColumnIndex(DatabaseHelpers.PRODUCT_ID));
                    String Total = cursor.getString(cursor.getColumnIndex(DatabaseHelpers.CART_TOTAL));
                    cart_databases.add(new Cart_Database(id,Quantity,Product_id,Total));
                }while (cursor.moveToNext());
            }
        }
        cursor.close();

        @SuppressLint("Recycle")
        Cursor cursor_product = db.rawQuery(selectQuery_product, null);
        List<ProductData> products = new ArrayList<>();
        if (cursor_product != null) {
            int ii = 0;
            if (cursor_product.moveToFirst()){
                do{
                    @SuppressLint("Recycle")
                    Cursor cursor_images = db.rawQuery(selectQuery_images, null);
                    List<ImagesData> productImages = new ArrayList<>();
                    if (cursor_images != null) {
                        if (cursor_images.moveToFirst()){
                            do{
                                if (cursor_images.getString(cursor_images.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))){
                                    String id = cursor_images.getString(cursor_images.getColumnIndex(DatabaseHelpers.I_ID));
                                    String src = cursor_images.getString(cursor_images.getColumnIndex(DatabaseHelpers.I_SRC));
                                    String path = cursor_images.getString(cursor_images.getColumnIndex(DatabaseHelpers.I_Thumbnail));
                                    String title = cursor_images.getString(cursor_images.getColumnIndex(DatabaseHelpers.I_TITLE));
                                    productImages.add(new ImagesData(id, src, title, path));
//                                    Log.e("TAG Images", ""+ii+" "+productImages.size()+" "+title);
                                }
                            }while (cursor_images.moveToNext());
                        }
                    }
                    cursor_images.close();

                    @SuppressLint("Recycle")
                    Cursor cursor_category = db.rawQuery(selectQuery_category, null);
                    List<CategoriesData> productCategories = new ArrayList<>();
                    if (cursor_category != null) {
                        if (cursor_category.moveToFirst()){
                            do{
                                if (cursor_category.getString(cursor_category.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))){

                                    String id = cursor_category.getString(cursor_category.getColumnIndex(DatabaseHelpers.C_ID));
                                    String name = cursor_category.getString(cursor_category.getColumnIndex(DatabaseHelpers.C_NAME));
                                    productCategories.add(new CategoriesData(id,name));
//                                    Log.e("TAG categorios", ""+productCategories.size()+" "+name);
                                }
                            }while (cursor_category.moveToNext());
                        }
                    }
                    cursor_category.close();

                    @SuppressLint("Recycle")
                    Cursor cursor_related_id = db.rawQuery(selectQuery_related_id, null);
                    List<String> relatedProductIds = new ArrayList<>();
                    if (cursor_related_id != null) {
                        if (cursor_related_id.moveToFirst()){
                            do{
//                                Log.e("TAG related_ids", ""+ii+" "+cursor_related_id.getString(cursor_related_id.getColumnIndex(DatabaseHelpers.P_ID))+" "+cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)));
                                if (cursor_related_id.getString(cursor_related_id.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))){
                                    String id = cursor_related_id.getString(cursor_related_id.getColumnIndex(DatabaseHelpers.R_ID));
                                    relatedProductIds.add(id);
//                                    Log.e("TAG related_ids", ""+ii+" "+relatedProductIds.size()+" "+id);
                                }
                            }while (cursor_related_id.moveToNext());

                        }
                    }

                    cursor_related_id.close();

                    /*@SuppressLint("Recycle")
                    Cursor cursor_pvm = db.rawQuery(selectQuery_pvm, null);
                    List<ProductVariationModel> productVariationModels = new ArrayList<>();
                    if (cursor_pvm != null) {
                        if (cursor_pvm.moveToFirst()){
                            do{
                                if (cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))) {

                                    String id = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_ID));
                                    String name = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_name));
                                    String permalink = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_permalink));
                                    String reguler_price = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_regular_price));
                                    String price = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_price));
                                    String size_name = cursor_pvm.getString(cursor_pvm.getColumnIndex(DatabaseHelpers.V_size_name));
                                    productVariationModels.add(new ProductVariationModel(id,permalink,reguler_price,price,name,size_name));
//                                    Log.e("TAG productAttributes", ""+productVariationModels.size()+" "+id);
                                }
                            }while (cursor_pvm.moveToNext());
                        }
                    }*/
                    /*@SuppressLint("Recycle")
                    Cursor cursor_attribute = db.rawQuery(selectQuery_attribute, null);
                    List<ProductAttributes> productAttributes = new ArrayList<>();
                    if (cursor_attribute != null) {
                        if (cursor_attribute.moveToFirst()){
                            do{
                                if (cursor_attribute.getString(cursor_attribute.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))){
                                    ProductAttributes attributes = new ProductAttributes();
                                    String name = cursor_attribute.getString(cursor_attribute.getColumnIndex(DatabaseHelpers.A_name));
                                    String option = cursor_attribute.getString(cursor_attribute.getColumnIndex(DatabaseHelpers.A_option));
                                    attributes.setAtrbt_name(name);
                                    attributes.setAtrbt_option(option);
                                    productAttributes.add(attributes);
//                                    Log.e("TAG productAttributes", ""+productAttributes.size()+" "+attributes);
                                }
                            }while (cursor_attribute.moveToNext());
                        }
                    }
*/
                    /*@SuppressLint("Recycle")
                    Cursor cursor_margin = db.rawQuery(selectQuery_margin, null);
                    List<MarginValues> marginValues = new ArrayList<>();
                    if (cursor_margin != null) {
                        if (cursor_margin.moveToFirst()){
                            do{
                                if (cursor_margin.getString(cursor_margin.getColumnIndex(DatabaseHelpers.P_ID)).equals(cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID)))){
                                    MarginValues values = new MarginValues();
                                    String max = cursor_margin.getString(cursor_margin.getColumnIndex(DatabaseHelpers.M_maxQuantity));
                                    String min = cursor_margin.getString(cursor_margin.getColumnIndex(DatabaseHelpers.M_minQuantity));
                                    String type = cursor_margin.getString(cursor_margin.getColumnIndex(DatabaseHelpers.M_type));
                                    String unitPrice = cursor_margin.getString(cursor_margin.getColumnIndex(DatabaseHelpers.M_unitPrice));
                                    values.setUnitPrice(unitPrice);
                                    values.setMaxQuantity(max);
                                    values.setMinQuantity(min);
                                    values.setType(type);
                                    marginValues.add(values);
//                                    Log.e("TAG MArgin", ""+marginValues.size()+" "+values);
                                }
                            }while (cursor_margin.moveToNext());
                        }
                    }*/

                    String p_id                = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_ID               ));
                    String p_title             = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_TITLE            ));
                    String p_type              = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_type             ));
                    String p_status            = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_status           ));
                    String p_permalink         = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_permalink        ));
                    String P_sku               = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_sku              ));
                    String P_price             = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_price            ));
                    String P_regular_price     = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_regular_price    ));
                    String P_sale_price        = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_sale_price       ));
                    String P_price_html        = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_price_html       ));
                    String P_manage_stock      = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_manage_stock     ));
                    String P_stock_quantity    = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_stock_quantity   ));
                    String P_stock_status      = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_stock_status   ));
                    String P_product_url       = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_product_url      ));
                    String P_weight            = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_weight           ));
                    String P_description       = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_description      ));
                    String P_short_description = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_short_description));
                    String P_average_rating    = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_average_rating   ));
                    String P_rating_count      = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_rating_count     ));
                    String P_featured_src      = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_featured_src     ));
                    String P_downloadable      = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_downloadable     ));
                    String P_virtuals          = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_virtuals         ));
                    String P_on_sale           = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_on_sale          ));
                    String P_featured          = cursor_product.getString(cursor_product.getColumnIndex(DatabaseHelpers.P_featured         ));
                    /*products.add(new Product(p_title,p_id,p_type,
                            p_status,p_permalink,P_sku,P_price,P_regular_price,
                            P_sale_price,P_price_html,P_manage_stock,P_stock_quantity,null,P_weight,
                            P_description,P_short_description,P_average_rating,
                            P_rating_count,null,P_downloadable.toLowerCase().equals("true")? true :false ,
                            P_virtuals.toLowerCase().equals("true") ? true : false,P_on_sale.toLowerCase().equals("true") ? true : false,
                            P_featured.toLowerCase().equals("true") ? true : false,productCategories, productImages,relatedProductIds,productVariationModels,
                            productAttributes,marginValues));*/
                    /*ProductData product = new ProductData(p_title,p_id,p_type,
                            p_status,p_permalink,P_sku,P_price,P_regular_price,
                            P_sale_price,P_price_html,P_manage_stock,P_stock_quantity, P_stock_status,"null",P_weight,
                            P_description,P_short_description,P_average_rating,
                            P_rating_count,null,P_downloadable.toLowerCase().equals("true")? true :false ,
                            P_virtuals.toLowerCase().equals("true") ? true : false,P_on_sale.toLowerCase().equals("true") ? true : false,
                            P_featured.toLowerCase().equals("true") ? true : false,productCategories, productImages,relatedProductIds );*/

                    ProductData product = new ProductData();
                    product.setName(p_title);
                    product.setId(p_id);
                    product.setType(p_type);
                    product.setStatus(p_status);
                    product.setPermalink(p_permalink);
                    product.setSku(P_sku);
                    product.setPrice(P_price);
                    product.setRegular_price(P_regular_price);
                    product.setSale_price(P_sale_price);
                    product.setPrice_html(P_price_html);
                    product.setManage_stock(P_manage_stock);
                    product.setStock_quantity(P_stock_quantity);
                    product.setStock_status(P_stock_status);
                    product.setManage_stock(P_manage_stock);
                    product.setWeight(P_weight);
                    product.setDescription(P_description);
                    product.setShort_description(P_short_description);
                    product.setAverage_rating(P_average_rating);
                    product.setRating_count(P_rating_count);
                    product.setDownloadable(P_downloadable);
                    product.setVirtual(P_virtuals);
                    product.setOn_sale(P_on_sale);
                    product.setFeatured(P_featured);
                    product.setCategories(productCategories);
                    product.setImages(productImages);
                    product.setRelated_ids(relatedProductIds);
                    cart_item.add( new CartItem(product, Integer.parseInt(cart_databases.get(ii).getQuantity()), ""));

//                    Log.e("TAG Product", ""+ii+" "+product+" "+p_id);
                    ii++;
                }while (cursor_product.moveToNext());
            }
        }
        cursor_product.close();
        return cart_item;
    }

    public int update( String product_id, String Quantity) {
        SQLiteDatabase db = new DatabaseHelpers(context).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelpers.QUANTITY, Quantity);
        int i = db.update(DatabaseHelpers.TABLE_NAME, contentValues,  DatabaseHelpers.PRODUCT_ID + " = " + product_id, null);

        return i;
    }

    public void Delete(String product_id) {
        SQLiteDatabase db = new DatabaseHelpers(context).getWritableDatabase();
        db.delete(DatabaseHelpers.TABLE_NAME, DatabaseHelpers.PRODUCT_ID + "=" + product_id, null);
        db.delete(DatabaseHelpers.TABLE_NAME_PRODUCT, DatabaseHelpers.P_ID + "=" + product_id, null);
        db.delete(DatabaseHelpers.TABLE_NAME_IMAGES, DatabaseHelpers.P_ID + "=" + product_id, null);
//        db.delete(DatabaseHelpers.TABLE_NAME_ATTRIBUTE, DatabaseHelpers.P_ID + "=" + product_id, null);
//        db.delete(DatabaseHelpers.TABLE_NAME_MARGIN, DatabaseHelpers.P_ID + "=" + product_id, null);
        db.delete(DatabaseHelpers.TABLE_NAME_CATEGORY, DatabaseHelpers.P_ID + "=" + product_id, null);
//        db.delete(DatabaseHelpers.TABLE_NAME_PVM, DatabaseHelpers.P_ID + "=" + product_id, null);
        db.delete(DatabaseHelpers.TABLE_NAME_RELATED_ID, DatabaseHelpers.P_ID + "=" + product_id, null);
    }

    public void removeTable(){
        SQLiteDatabase db = new DatabaseHelpers(context).getWritableDatabase();
        db.delete(DatabaseHelpers.TABLE_NAME,null,null);
        db.delete(DatabaseHelpers.TABLE_NAME_PRODUCT,null,null);
        db.delete(DatabaseHelpers.TABLE_NAME_IMAGES,null,null);
//        db.delete(DatabaseHelpers.TABLE_NAME_ATTRIBUTE,null,null);
//        db.delete(DatabaseHelpers.TABLE_NAME_MARGIN,null,null);
        db.delete(DatabaseHelpers.TABLE_NAME_CATEGORY,null,null);
//        db.delete(DatabaseHelpers.TABLE_NAME_PVM,null,null);
        db.delete(DatabaseHelpers.TABLE_NAME_RELATED_ID,null,null);
    }

    public void removeDatabase() {
        context.deleteDatabase(DatabaseHelpers.DB_NAME);
    }
}
