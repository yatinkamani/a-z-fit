package com.azfit.CartData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Databasehelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "CART";

    // Table columns
    public static final String _ID = "_id";
    public static final String QUANTITY = "quantity";
    public static final String CART_TOTAL = "CartTotal";
    public static final String PRODUCT_ID = "product_id";

    // Database Information
    static final String DB_NAME = "MY_CART.DB";

    // database version
    static final int DB_VERSION = 1;

    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + QUANTITY + " TEXT NOT NULL," + PRODUCT_ID + " TEXT NOT NULL, " + CART_TOTAL + " TEXT);";


    // Creating table query

    public Databasehelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
