package com.azfit.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azfit.Adapter.AdapterCart;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.CartData.DBManagerData;
import com.azfit.Model.CartItem;
import com.azfit.Model.CouponCode.CouponData;
import com.azfit.Model.CouponCode.CouponModel;
import com.azfit.Model.Orders.LineItems;
import com.azfit.Model.Orders.OrdersModel;
import com.azfit.Model.ShippingMethod.Settings;
import com.azfit.Model.ShippingMethod.ShippingMethod;
import com.azfit.PayPal.PayPalConfig;
import com.azfit.R;
import com.azfit.fragment.ProfileFragment;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.azfit.utils.RecyclerTouchListener;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.utils.AppUtils.instance;
import static com.azfit.utils.AppUtils.roundTwoDecimals;
import static com.azfit.utils.AppUtils.show_toast_short;
import static com.azfit.utils.AppUtils.validationText;

public class CartActivity extends AppCompatActivity implements View.OnClickListener {

    //cart Item
    RelativeLayout counterValuePanel, counterPanel;
    TextView txt_count;

    RecyclerView rcv_cartItem;

    EditText ed_couponCode;
    Button btn_apply;

    TextView txt_subTotal, txt_tax, txt_total;

    Button btn_checkout;
    ImageButton btn_payPal;
    AdapterCart adapterCart;

    DBManagerData dbManager;
    List<CartItem> cartItem = new ArrayList<>();

    // no data
    View no_data;
    TextView txt_msg;
    Button btn_retry;

    FrameLayout frame_view;
    LinearLayout lin_coupon;
    TextView txt_coupon;

    //PayPal intent request code to track onActivityResult method
    public static final int PAY_PAL_REQUEST_CODE = 123;

    //Payment Amount
    private String paymentAmount;
    SessionManager manager;
    LoaderView loaderView;

    boolean isCouponApply = false;
    double coupon_price = 0;
    double total = 0;
    int min_amount = 0;
    int min_voucher_amount = -1;
    int max_voucher_amount = -1;
    String[] product_ids = null;
    CouponData couponData = null;

    //PayPal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            .environment(PayPalConfig.PAYPAL_ENVIRONMENT)
            // .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        dbManager = new DBManagerData(CartActivity.this);
        manager = new SessionManager(CartActivity.this);
        AppUtils.validation = this;
        loaderView = new LoaderView(CartActivity.this);
        cartItem = dbManager.fetch();
        instance = this;

        initView();

    }

    public void initView() {

        counterValuePanel = findViewById(R.id.counterValuePanel);
        counterPanel = findViewById(R.id.counterPanel);
        txt_count = findViewById(R.id.count);

        rcv_cartItem = findViewById(R.id.rcv_cartItem);

        ed_couponCode = findViewById(R.id.ed_couponCode);
        btn_apply = findViewById(R.id.btn_apply);

        txt_subTotal = findViewById(R.id.txt_subTotal);
        txt_tax = findViewById(R.id.txt_tax);
        txt_total = findViewById(R.id.txt_total);

        no_data = findViewById(R.id.no_data);
        txt_msg = findViewById(R.id.txt_message);
        txt_msg.setText(getString(R.string.cart_is_empty));
        btn_retry = findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(this);
        btn_retry.setVisibility(View.GONE);

        frame_view = findViewById(R.id.frame_view);

        btn_checkout = findViewById(R.id.btn_checkout);
        btn_payPal = findViewById(R.id.btn_payPal);

        lin_coupon = findViewById(R.id.lin_coupon);
        txt_coupon = findViewById(R.id.txt_coupon);

        btn_checkout.setOnClickListener(this);
        btn_payPal.setOnClickListener(this);
        btn_apply.setOnClickListener(this);

        setCartList();
        payPalServices(true);
    }

    public void setCartList() {
        cartItem = dbManager.fetch();
        buildCounterDrawable();
        if (cartItem != null && cartItem.size() > 0) {
            no_data.setVisibility(View.GONE);
            frame_view.setVisibility(View.VISIBLE);
            rcv_cartItem.setLayoutManager(new LinearLayoutManager(CartActivity.this));
            rcv_cartItem.setHasFixedSize(true);
            rcv_cartItem.setItemAnimator(new DefaultItemAnimator());
            adapterCart = new AdapterCart(CartActivity.this, cartItem);
            rcv_cartItem.setAdapter(adapterCart);

            setCartTotal(cartItem);

            rcv_cartItem.addOnItemTouchListener(new RecyclerTouchListener(CartActivity.this, rcv_cartItem, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    cartItem = dbManager.fetch();
                    Log.e("Tag Clicked", "" + position);

                    TextView txt_price = view.findViewById(R.id.txt_product_price);

                    Button btn_plus = view.findViewById(R.id.btn_plus);
                    TextView txt_quantity = view.findViewById(R.id.txt_quantity);

                    btn_plus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {

                                int newQty = cartItem.get(position).getQty() + 1;
                                Log.e("TAG", "newQty==>: " + newQty);
                                dbManager.update(cartItem.get(position).getProduct().getId(), "" + newQty);

                                setUpTotal(dbManager.fetch().get(position), txt_price);
                                setCartTotal(dbManager.fetch());
                                txt_quantity.setText("" + newQty);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                    Button btn_minus = view.findViewById(R.id.btn_minus);

                    btn_minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (cartItem.get(position).getQty() == 1) {

                                new AlertDialog.Builder(CartActivity.this)
                                        .setTitle(getString(R.string.app_name))
                                        .setMessage(R.string.do_you_really_want_to_remove_this_product)
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(R.string.ok,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int whichButton) {

                                                        dbManager.Delete(cartItem.get(position).getProduct().getId());
                                                        if (dbManager.fetch().size() > 0) {
                                                            adapterCart.UpdateData(dbManager.fetch());
                                                        }
                                                        buildCounterDrawable();
                                                        setCartList();
                                                    }
                                                })
                                        .setNegativeButton(R.string.no, null)
                                        .show();

                            } else {
                                int newQty = cartItem.get(position).getQty() - 1;
                                Log.e("TAG", "newQty==>: " + newQty);
                                dbManager.update(cartItem.get(position).getProduct().getId(), "" + newQty);

                                setUpTotal(dbManager.fetch().get(position), txt_price);
                                setCartTotal(dbManager.fetch());
                                txt_quantity.setText("" + newQty);
                            }
                        }
                    });
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));

        } else {
            no_data.setVisibility(View.VISIBLE);
            txt_msg.setText(R.string.cart_is_empty);
            btn_retry.setVisibility(View.GONE);
            frame_view.setVisibility(View.GONE);
        }

    }

    public void setCartTotal(@NotNull List<CartItem> cartItem) {

        total = 0;
        for (int i = 0; i < cartItem.size(); i++) {
            double tot = 0;
            if (cartItem.get(i).getProduct().getPrice() != null && !cartItem.get(i).getProduct().getPrice().equals("null")) {
                try {
                    tot = Float.valueOf(cartItem.get(i).getProduct().getPrice()) * cartItem.get(i).getQty();
                } catch (Exception e) {
                    tot = 0;
                }
                total = total + tot;
            }

            /*if (product_ids != null && product_ids.length>0){

                for (String id : product_ids){
                    if (id.toLowerCase().equals(cartItem.get(i).getProduct().getId())){
                        isValidProduct = true;
                        break;
                    }
                }
            }else {
                isValidProduct = true;
            }*/

            Log.e("Tag quantity", "" + total);
            Log.e("Tag quantity", "" + cartItem.get(i).getQty());
            Log.e("Tag quantity price", "" + cartItem.get(i).getProduct().getPrice());
        }

        /*if (isValidProduct && isCouponApply){

            if (min_voucher_amount != -1 && max_voucher_amount != -1){

                if (min_voucher_amount >= total || max_voucher_amount <= total){
                    show_toast_short(getApplicationContext(), "coupon code not apply");
                    lin_coupon.setVisibility(View.GONE);
                    ed_couponCode.setText("");
                    isCouponApply = false;
                }else {
                    lin_coupon.setVisibility(View.VISIBLE);
                }

            }else if (min_voucher_amount != -1 && min_voucher_amount >= total){
                show_toast_short(getApplicationContext(), "coupon code not apply");
                ed_couponCode.setText("");
                isCouponApply = false;
            }else if (max_voucher_amount != -1 && max_voucher_amount <= total){
                show_toast_short(getApplicationContext(), "coupon code not apply");
                ed_couponCode.setText("");
                lin_coupon.setVisibility(View.GONE);
                isCouponApply = false;
            } else {
                lin_coupon.setVisibility(View.VISIBLE);
            }
        }*/

        if (lin_coupon.getVisibility() == View.VISIBLE) {
            coupon_price = Double.parseDouble(txt_coupon.getText().toString().replaceAll(" €", ""));
            coupon_price = Double.parseDouble(("" + coupon_price).replaceAll("-", ""));
        } else {
            isCouponApply = false;
        }
        Log.e("Tag coupon price", "" + coupon_price);

        try {
            txt_subTotal.setText(""+roundTwoDecimals(total) + " €");
            double res = ((total * AppUtils.TAX) / 100);
            txt_tax.setText(""+roundTwoDecimals(res) + " €");
            txt_total.setText("" + roundTwoDecimals(((total + res) - (coupon_price))) + " €");

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void setUpTotal(@NotNull CartItem cartItem, TextView txt_product_price) {
        if (cartItem.getProduct().getPrice() != null && !cartItem.getProduct().getPrice().equals("null")) {
            txt_product_price.setText("€ " + roundTwoDecimals(Float.parseFloat(cartItem.getProduct().getPrice()) * cartItem.getQty()));
        }
        Log.e("tag price", "" + cartItem.getProduct().getPrice() + " * " + cartItem.getQty());
    }

    private void buildCounterDrawable() {
        txt_count = findViewById(R.id.count);
        AppUtils.refreshCartDB(txt_count, dbManager.fetch());
    }

    @Override
    public void onClick(View v) {
        if (v == btn_checkout) {

            if (manager.isLoggedIn()) {

                if (AppUtils.isNetworkAvailable(getApplicationContext())){

                    getOrderApi(false);

                }else {
                    show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
                }

            } else {
                startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), 210);
            }

        } else if (v == btn_payPal) {
            if (manager.isLoggedIn()) {
                if (total != 0) {
                    if (AppUtils.isNetworkAvailable(getApplicationContext())){

                        getOrderApi(true);

                    }else {
                        show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
                    }
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.total_amount_zero));
                }
            } else {
                startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), 210);
            }
//            Toast.makeText(getApplicationContext(), "Process under Build", Toast.LENGTH_LONG).show();
        } else if (v == btn_retry) {

            if (btn_retry.getText().equals(getString(R.string.more_service))) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {

            }
        } else if (v == btn_apply) {
            if (!validationText(ed_couponCode)) {
                return;
            }

            checkCoupon();
//            show_toast_short(getApplicationContext(), getString(R.string.invalid_coupon_code));
        }
    }

    public void payPalServices(boolean start) {

        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        if (start) {
            startService(intent);
        } else {
            if (isMyServiceRunning(PayPalService.class)) {
                stopService(intent);
            }
        }
    }

    @Override
    protected void onDestroy() {
        payPalServices(false);
        super.onDestroy();
    }

    private void getPayment() {

        /*PayPalItem[] items =
                {
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };*/
        /*PayPalItem[] items = new PayPalItem[dbManager.fetch().size()];
        for (int i=0; i<dbManager.fetch().size(); i++){
            ProductData productData = dbManager.fetch().get(i).getProduct();
            CartItem item = dbManager.fetch().get(i);
            items[i] = new PayPalItem(item.getProduct().getName(),item.getQty(),new BigDecimal(productData.getPrice()),"EUR", "Paypall");
        }*/
        /*Log.e("Tag Price", items[0].toString());
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("0.00");
        BigDecimal tax = new BigDecimal(txt_tax.getText().toString().replace(" €",""));
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(tax);*/
        //        Log.e("Tag Price Amount", amount.toString());

        //Getting the amount from editText

        paymentAmount = txt_total.getText().toString().replace(" €", "");

        //Creating a payPalPayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(paymentAmount), PayPalConfig.DEFAULT_CURRENCY, "Pay to Paypal", PayPalPayment.PAYMENT_INTENT_SALE);
        /*ShippingAddress shippingAddress =
                new ShippingAddress().recipientName(manager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name())
                        .line1(manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1())
                        .city(manager.getLoginUserDetail().getCustomer().getBilling_address().getCity())
                        .state(manager.getLoginUserDetail().getCustomer().getBilling_address().getState())
                        .postalCode(manager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode())
                        .countryCode("US");*/

       /* ShippingAddress shippingAddress =
                new ShippingAddress().recipientName("Mom Parker").line1("52 North Main St.")
                        .city("Austin").state("TX").postalCode("78729").countryCode("US");*/

//        payment.providedShippingAddress(shippingAddress);
//        payment.items(items).paymentDetails(paymentDetails);

        //Creating PayPal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAY_PAL_REQUEST_CODE);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the result is from payPal
        if (requestCode == PAY_PAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.e("paymentExample", paymentDetails);

                        /*startActivity(new Intent(this, ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", paymentAmount));*/
                        //Starting a new activity for the payment details and also putting the payment details with intent
                        //show_toast_short(getApplicationContext(), paymentDetails);
                        placeOrderApi();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == 210) {
            setCartList();
            payPalServices(true);
            Log.e("Return to Cart", "Return to cart");
        }
    }

    public String createOrderJson() {

        JSONObject order = new JSONObject();
        try {
            JSONObject order_value = new JSONObject();

            JSONObject paymentDetail = new JSONObject();
            paymentDetail.put("method_id", "paypal");
            paymentDetail.put("method_title", "Paypal");
            paymentDetail.put("paid", "true");

            JSONObject billing_address = new JSONObject();
            billing_address.put("first_name", manager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name());
            billing_address.put("last_name", manager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name());
            billing_address.put("address_1", manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1());
            billing_address.put("address_2", manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2());
            billing_address.put("city", manager.getLoginUserDetail().getCustomer().getBilling_address().getCity());
            billing_address.put("state", "");
            billing_address.put("postcode", manager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode());
            billing_address.put("country", "Germany");
            billing_address.put("email", manager.getLoginUserDetail().getCustomer().getBilling_address().getEmail());
            billing_address.put("phone", manager.getLoginUserDetail().getCustomer().getBilling_address().getPhone());
            billing_address.put("company", manager.getLoginUserDetail().getCustomer().getBilling_address().getCompany());

            JSONObject shiping_address = new JSONObject();
            shiping_address.put("first_name", manager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name());
            shiping_address.put("last_name", manager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name());
            shiping_address.put("address_1", manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1());
            shiping_address.put("address_2", manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2());
            shiping_address.put("city", manager.getLoginUserDetail().getCustomer().getBilling_address().getCity());
            shiping_address.put("postcode", manager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode());

            JSONArray lin_itemArray = new JSONArray();

            for (int i = 0; i < dbManager.fetch().size(); i++) {

                JSONObject line_item = new JSONObject();
                line_item.put("product_id", dbManager.fetch().get(i).getProduct().getId());
                line_item.put("quantity", dbManager.fetch().get(i).getQty());
                line_item.put("subtotal", roundTwoDecimals(Double.parseDouble(dbManager.fetch().get(i).getProduct().getPrice()) * dbManager.fetch().get(i).getQty()));
                line_item.put("total", roundTwoDecimals(Double.parseDouble(dbManager.fetch().get(i).getProduct().getPrice()) * dbManager.fetch().get(i).getQty()));
                lin_itemArray.put(line_item);
            }

            /*JSONArray taxLine = new JSONArray();
            JSONObject txtObject = new JSONObject();
            txtObject.put("rate_id","6");
            txtObject.put("code","DE-MWST. DE-1");
            txtObject.put("title","MwSt. DE");
            txtObject.put("total", txt_tax.getText().toString().replace(" €",""));
            txtObject.put("compound","false");
            taxLine.put(txtObject);*/

            JSONArray couponArray = new JSONArray();
            if (isCouponApply && couponData != null) {
                JSONObject couponObject = new JSONObject();
                couponObject.put("id", couponData.getID());
                couponObject.put("code", couponData.getCoupon_code());
                couponObject.put("amount", couponData.getCoupon_amount());
                couponArray.put(couponObject);
            }

            JSONArray shipping_linesArray = new JSONArray();
            JSONObject shipping_lines = new JSONObject();
            shipping_lines.put("method_id", "free_shipping");
            shipping_lines.put("method_title", "Free shipping");
            shipping_lines.put("total", "0");
            shipping_linesArray.put(shipping_lines);

            order_value.put("payment_details", paymentDetail);
            order_value.put("billing_address", billing_address);
            order_value.put("shipping_address", shiping_address);
            order_value.put("customer_id", manager.getUserId());
            order_value.put("note", "");
            order_value.put("line_items", lin_itemArray);
//            order_value.put("tax_lines", taxLine);
            if (isCouponApply && couponData != null) {
                order_value.put("coupon_lines", couponArray);
            }
            order_value.put("shipping_lines", shipping_linesArray);
            order.put("order", order_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return order.toString();
    }

    public void placeOrderApi() {

        if (!this.isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

        if (createOrderJson() == null) {
            show_toast_short(getApplicationContext(), "something wrong");
            return;
        }

        service.getCustomer("get_product", "1");

        LogUtils.error(BillingActivity.class, "Tag json   " + createOrderJson());

        service.create_order(AppUtils.CREATE_ORDER, createOrderJson()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null) {

                        try {
                            JSONObject object = new JSONObject((response.body().string()));
                            JSONObject order = object.getJSONObject("order");
                            String order_number = order.getString("order_number");
                            frame_view.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            txt_msg.setText(getString(R.string.your_order_is_successfully_places_your_order_number_is) + order_number);
                            btn_retry.setVisibility(View.VISIBLE);
                            btn_retry.setText(getString(R.string.more_service));
                            Toast.makeText(getApplicationContext(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                            dbManager.removeTable();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }

                    } else {

                        Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.order_successfully));
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                placeOrderApi();
            }
        });
    }

    public void getShippingMethod() {

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getShippingMethod().enqueue(new Callback<ShippingMethod>() {
            @Override
            public void onResponse(@NotNull Call<ShippingMethod> call, @NotNull Response<ShippingMethod> response) {

                if (response.isSuccessful() && response.body() != null) {
                    Log.e("Tag response shipping", response.body().toString());
                    if (response.body().getShipping() != null && response.body().getShipping().size() > 0) {
                        Settings settings = response.body().getShipping().get(0).getSettings();
                        min_amount = Integer.parseInt(settings.getMin_amount().getValue());

                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ShippingMethod> call, @NotNull Throwable t) {
                Log.e("Tag Shipping Error", t.toString());
            }
        });
    }

    public void checkCoupon() {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.checkCoupon(AppUtils.CHECK_COUPON, ed_couponCode.getText().toString()).enqueue(new Callback<CouponModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NotNull Call<CouponModel> call, @NotNull Response<CouponModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    couponData = new CouponData();
                    if (response.body() != null && response.body().isStatus() && response.body().getCouponData() != null && response.body().getCouponData().size() > 0) {
                        CouponData data = response.body().getCouponData().get(0);
                        if (data.getProduct_ids() != null && !data.getProduct_ids().equals("null")) {
                            product_ids = new String[data.getProduct_ids().split(",").length];

                            for (int i = 0; i < data.getProduct_ids().split(",").length; i++) {
                                product_ids[i] = data.getProduct_ids().split(",")[i];
                            }
                        }

                        if (data.getMinimum_amount() != null && !data.getMinimum_amount().equals("null")) {
                            min_voucher_amount = Integer.parseInt(data.getMinimum_amount());
                        }

                        if (data.getMaximum_amount() != null && !data.getMaximum_amount().equals("null")) {
                            max_voucher_amount = Integer.parseInt(data.getMinimum_amount());
                        }

                        lin_coupon.setVisibility(View.VISIBLE);
                        isCouponApply = true;
                        couponData = data;
                        txt_coupon.setText("-" + response.body().getCouponData().get(0).getCoupon_amount() + " €");
                        setCartTotal(dbManager.fetch());

                    } else {
                        lin_coupon.setVisibility(View.GONE);
                        ed_couponCode.setText("");
                        Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.order_successfully));
                }

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponModel> call, @NonNull Throwable t) {
                Log.e("Tag Shipping Error", t.toString());
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                show_toast_short(getApplicationContext(), getString(R.string.something_went_wrong));
            }
        });
    }

    public void getOrderApi(boolean isPayPal) {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            if (!this.isFinishing()){
                loaderView.show();
            }
            ApiService apiService = RetrofitClient.createService(ApiService.class);
            apiService.getOrders(AppUtils.MY_ORDERS, manager.getUserId()).enqueue(new Callback<OrdersModel>() {
                @Override
                public void onResponse(@NotNull Call<OrdersModel> call, @NotNull Response<OrdersModel> response) {

                    LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                    LogUtils.error(LoginActivity.class, "Tag Response Order  " + response);

                    boolean valid = false;
                    if (response.isSuccessful() && response.body() != null) {

                        try {
                            LogUtils.error(LoginActivity.class, "Tag Response Order" + new JSONObject(response.body().toString()).toString(2));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        orders = new ArrayList<>();
                        if (response.body().getOrders() != null && response.body().getOrders().size() > 0) {

                            for (int i = 0; i < response.body().getOrders().size(); i++) {

//                                if (response.body().getOrders().get(i).getStatus().equals("completed")){
                                if (response.body().getOrders().get(i).getLine_items() != null &&
                                        response.body().getOrders().get(i).getLine_items().size() > 0) {

                                    if (dbManager != null && dbManager.fetch().size() > 0) {
                                        for (CartItem item : dbManager.fetch()) {

                                            for (LineItems lineItem : response.body().getOrders().get(i).getLine_items()) {

                                                if (lineItem.getProduct_id().equals(item.getProduct().getId())) {

                                                    if (response.body().getOrders().get(i).getStatus().equals("completed")) {

                                                        Log.e("Tag check Date", AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()));
                                                        if (AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()).equals("1")) {
                                                            valid = true;
                                                        }else {
                                                            valid = false;
                                                            break;
                                                        }
                                                    } else if (response.body().getOrders().get(i).getStatus().equals("cancelled") || response.body().getOrders().get(i).getStatus().equals("failed")) {
                                                        valid = true;
                                                    } else {
                                                        valid = false;
                                                        break;
                                                    }
                                                } else {
                                                    valid = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!valid) {
                                    break;
                                }
                            }
                        } else {
                            valid = true;
                        }
                    } else {
                        valid = true;
                    }

                    if (valid){
                        if (isPayPal){
                            getPayment();
                        }else {
                            Intent intent = new Intent(getApplicationContext(), BillingActivity.class);
                            if (isCouponApply) {
                                intent.putExtra("couponData", new Gson().toJson(couponData));
                            }
                            startActivity(intent);
                        }
                    } else {
                        show_toast_short(getApplicationContext(), getString(R.string.your_product_course_aleready_n_ordered));
                    }

                    if (loaderView != null && loaderView.isShowing()) {
                        loaderView.dismiss();
                    }

                }

                @Override
                public void onFailure(@NotNull Call<OrdersModel> call, @NotNull Throwable t) {

                    LogUtils.error(ProfileFragment.class, t.getMessage());

                    if (loaderView != null && loaderView.isShowing()) {
                        loaderView.dismiss();
                    }
                }
            });
        } else {
        }
    }

    @Override
    protected void onResume() {
        buildCounterDrawable();
        super.onResume();
    }

}