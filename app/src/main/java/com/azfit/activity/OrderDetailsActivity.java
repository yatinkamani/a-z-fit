package com.azfit.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.azfit.Model.Orders.BillingAddress;
import com.azfit.Model.Orders.LineItems;
import com.azfit.Model.Orders.Order;
import com.azfit.Model.Orders.ShippingAddress;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView txt_order_date_number;
    LinearLayout lin_products, lin_billing, lin_shipping;
    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        initView();

    }

    public void initView() {

        txt_order_date_number = findViewById(R.id.txt_order_date_number);
        lin_products = findViewById(R.id.lin_products);
        lin_billing = findViewById(R.id.lin_billing);
        lin_shipping = findViewById(R.id.lin_shipping);

        getIntentData();
    }

    public void getIntentData() {

        if (getIntent() != null && getIntent().getStringExtra("orders") != null) {

            order = new Gson().fromJson(getIntent().getStringExtra("orders"), Order.class);

            lin_products.removeAllViews();
            lin_billing.removeAllViews();
            lin_shipping.removeAllViews();
            initializeView();
            productDetails(order);
            Log.e("Tag Shipping", order.getShipping_address().toString());
            shippingDetailFill(order.getShipping_address());
            Log.e("Tag Billing", order.getBilling_address().toString());
            billingDetailFill(order.getBilling_address());


        } else {
            finish();
        }

    }

    public void productDetails(@NotNull Order order) {

        if (order.getLine_items() != null && order.getLine_items().size() > 0) {

            for (LineItems items : order.getLine_items()) {
                addNewProduct(items.getName() + " * " + items.getQuantity(), items.getTotal()+" €");
            }
        }

        if (order.getTotal() != null) {
            addNewProduct(getString(R.string.subtotal), order.getTotal()+" €");
        }

        if (order.getShipping_methods() != null) {
            addNewProduct(getString(R.string.delivary), order.getShipping_methods());
        }

        if (order.getPayment_details() != null) {
            addNewProduct(getString(R.string.payment), order.getPayment_details().getMethod_title());
        }

        if (order.getTotal() != null) {
            addNewProduct(getString(R.string.total), order.getTotal()+ " €");
        }

        if (order.getCart_tax() != null && !order.getCart_tax().equals("0.00")) {
            addNewProduct(getString(R.string.incl_19_vat), order.getCart_tax()+" €");
        }

        if (order.getNote() != null && !order.getNote().equals("")) {
            addNewProduct(getString(R.string.note), order.getNote());
        }
    }

    public void shippingDetailFill(@NotNull ShippingAddress shippingAddress) {

        int total_field = 0;
        if (!shippingAddress.getFirst_name().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getFirst_name());
        }

        if (!shippingAddress.getLast_name().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getLast_name());
        }

        if (!shippingAddress.getCompany().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getCompany());
        }

        if (!shippingAddress.getAddress_1().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getAddress_1());
        }

        if (!shippingAddress.getAddress_2().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getAddress_2());
        }

        if (!shippingAddress.getCity().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getCity());
        }

        if (!shippingAddress.getState().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getState());
        }

        if (!shippingAddress.getPostcode().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getPostcode());
        }

        if (!shippingAddress.getCountry().equals("")) {
            total_field++;
            addShippingAddress(shippingAddress.getCountry());
        }

        if (total_field < 2) {
            addShippingAddress(getString(R.string.no_address));
        }

    }

    public void billingDetailFill(@NotNull BillingAddress billingAddress) {

        int total_field = 0;
        if (!billingAddress.getFirst_name().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getFirst_name(), false,false);
        }

        if (!billingAddress.getLast_name().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getLast_name(),false,false);
        }

        if (!billingAddress.getCompany().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getCompany(),false,false);
        }

        if (!billingAddress.getAddress_1().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getAddress_1(),false,false);
        }

        if (!billingAddress.getAddress_2().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getAddress_2(),false,false);
        }

        if (!billingAddress.getCity().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getCity(),false,false);
        }

        if (!billingAddress.getState().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getState(),false,false);
        }

        if (!billingAddress.getPostcode().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getPostcode(),false,false);
        }

        if (!billingAddress.getCountry().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getCountry(),false,false);
        }

        if (!billingAddress.getEmail().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getEmail(),true,false);
        }

        if (!billingAddress.getPhone().equals("")) {
            total_field++;
            addBillingAddress(billingAddress.getPhone(),false,true);
        }

        if (total_field < 2) {
            addBillingAddress(getString(R.string.no_address),false,false);
        }

    }

    private void addNewProduct(String name, String value) {

        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.view_order_line, null);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (order.getStatus().equals("completed")){

                    if (AppUtils.getCheckDateExpire(order.getCompleted_at()).equals("-1") ||
                            AppUtils.getCheckDateExpire(order.getCompleted_at()).equals("0")){

                        if (order.getLine_items() != null && order.getLine_items().size() > 0) {

                            for (LineItems items : order.getLine_items()) {

                                if ((items.getName() + " * " + items.getQuantity()).equals(name) && (items.getTotal()+" €").equals(value)){
                                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                                    intent.putExtra("product_id", ""+items.getProduct_id());
                                    startActivity(intent);
                                }
                            }
                        }
                    } else {
                        AppUtils.show_toast_short(getApplicationContext(), getString(R.string.expire_course_order));
                    }

                }else {
                    AppUtils.show_toast_short(getApplicationContext(), "pending");
                }
            }
        });

        TextView textName = view.findViewById(R.id.txt_name);
        TextView textValue = view.findViewById(R.id.txt_value);

        textName.setText(name);
        textValue.setText(value);

        lin_products.addView(view);

    }

    private void addShippingAddress(String name) {

        TextView textView = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        textView.setPadding(10,10,10,10);
        textView.setLayoutParams(params);
        textView.setTextColor(getResources().getColor(R.color.black));

//        textView.setTextSize(getResources().getDimension(R.dimen.sp_16));
        textView.setText(name);

        lin_shipping.addView(textView);

    }

    private void addBillingAddress(String name, boolean isMail, boolean isPhone) {

        TextView textView = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        params.setMargins(5, 5, 5, 5);
        params.gravity = Gravity.CENTER|Gravity.START;
        textView.setLayoutParams(params);
        textView.setTextColor(getResources().getColor(R.color.black));
        textView.setPadding(10,10,10,10);

//        textView.setTextSize(getResources().getDimension(R.dimen.sp_16));
        textView.setText(name);
        if (isMail){
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_mail,0,0,0);
            textView.setCompoundDrawablePadding(10);
        }

        if (isPhone){
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_phone_call,0,0,0);
            textView.setCompoundDrawablePadding(10);
        }

        Log.e("tag String name", "yes got " + name + "  it");

        lin_billing.addView(textView);

    }

    public void initializeView() {

        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.orders));
        builder.append(order.getOrder_number());
        builder.append(getString(R.string.of));
        builder.append(AppUtils.getFormattedDateTZ(order.getCreated_at()));
        builder.append(getString(R.string.is_currently));
        builder.append(order.getStatus());

        txt_order_date_number.setText(builder.toString());


    }
}