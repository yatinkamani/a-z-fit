package com.azfit.activity;

import android.os.Bundle;

import com.azfit.Adapter.AdapterPrices;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.PriceDataModel;
import com.azfit.Model.ServicesModel;
import com.azfit.R;
import com.azfit.utils.LoaderView;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity {

    AppBarLayout app_bar;
    CollapsingToolbarLayout toolbar_layout;
    LinearLayout lin_collapse_layout;
    ImageView img_logo;
    TextView txt_title, txt_slogan;
    Toolbar toolbar;
    TextView txt_content_title;
    RecyclerView rcv_prices;
    AdapterPrices adapterPrices;

    List<CourseData> courseData = new ArrayList<>();

    LoaderView loaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        loaderView = new LoaderView(RegistrationActivity.this);
        initView();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_layout.setTitle(getString(R.string.register));

    }

    public void initView() {

        app_bar = findViewById(R.id.app_bar);
        toolbar_layout = findViewById(R.id.toolbar_layout);
        lin_collapse_layout = findViewById(R.id.lin_collapse_layout);
        img_logo = findViewById(R.id.img_logo);
        txt_title = findViewById(R.id.txt_title);
        txt_slogan = findViewById(R.id.txt_slogan);
        toolbar = findViewById(R.id.toolbar);

        txt_content_title = findViewById(R.id.txt_content_title);
        rcv_prices = findViewById(R.id.rcv_subscription);
        setRecycleView();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void setRecycleView() {

        rcv_prices.setLayoutManager(new LinearLayoutManager(RegistrationActivity.this));
        rcv_prices.setHasFixedSize(true);
        rcv_prices.setItemAnimator(new DefaultItemAnimator());
        adapterPrices = new AdapterPrices(RegistrationActivity.this, getList());
        rcv_prices.setAdapter(adapterPrices);

        /*RecyclerTouchListener touchListener = new RecyclerTouchListener(this, rcv_prices, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


            }

            @Override
            public void onLongClick(View view, int position) {

            }

        });*/

    }

    /*public void productCourseApi() {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCourse(AppUtils.COURSE, "").enqueue(new Callback<Course>() {

            @Override
            public void onResponse(@NotNull Call<Course> call, @NotNull Response<Course> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
//                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null && response.body().getResult().size() > 0) {

                        for (CourseData course : response.body().getResult()) {
//                            if (productData.getName().toLowerCase().equals(course.getPost_title().toLowerCase())){

//                            if (!course_id.equals("")){

//                                if (course.getID().equals(course_id)) {
                                    if (course.get_tutor_course_product_id() != null){
                                        courseData.add(course);
                                    }
//                                    courseData = course;
//                                    productTopic();
//                                    courseAnnouncement();
//                                }
//                            } else if (!product_id.equals("")){
                                if (course.get_tutor_course_product_id() != null &&
                                        course.get_tutor_course_product_id().equals(product_id)) {
                                    course.set_tutor_course_price_type("free");
                                    courseData = course;

                                }
//                            }
                        }

                        *//*if (courseData != null) {

                            lin_course_about.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                txt_about_course.setText(Html.fromHtml(courseData.getPost_excerpt(), Html.FROM_HTML_MODE_LEGACY));
                            } else {
                                txt_about_course.setText(Html.fromHtml(courseData.getPost_excerpt()));
                            }
                            if (courseData.get_tutor_course_benefits() != null && !courseData.get_tutor_course_benefits().equals("null")) {

                                try {
                                    String s = courseData.get_tutor_course_benefits().replaceAll("\r\n", "\n\u2022 \t");
                                    Log.e("Tag spot", s);
                                    txt_what_learn.setText("\u2022 \t" + s);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if (courseData.get_tutor_course_level() != null && !courseData.get_tutor_course_level().equals("null")) {
                                txt_course_level.setText(getString(R.string.course_level)+courseData.get_tutor_course_level().replaceAll("_", " "));
                            }else {
                                txt_course_level.setText(getString(R.string.course_level)+" --");

                            }
                            txt_product_name.setText(courseData.getPost_title());
                            if (courseData.get_course_duration() != null && !courseData.get_course_duration().equals("null")) {

                                try {
                                    String c_d = courseData.get_course_duration();
                                    String duration = c_d.split("\"")[3] + "h " +
                                            c_d.split("\"")[7] + "m " +
                                            c_d.split("\"")[11] + "s ";

                                    txt_duration.setText(duration);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if (courseData.get_tutor_course_product_id() != null && !courseData.get_tutor_course_product_id().equals("null")) {
                                productDetailApi(courseData.get_tutor_course_product_id());
                            } else {
                                btn_buy.setVisibility(View.GONE);
                                txt_price.setText(getString(R.string.zero_zerozero));
                                txt_rate.setText(getString(R.string.zero_zero));
                            }

                            if (courseData.get_tutor_course_price_type().equals("free")) {
                                initializeCourseButton();
                            }

                            if (courseData.getDisplay_name() != null && courseData.getDisplay_name().length()>0){
                                txt_product_owner.setText(getString(R.string.von)+courseData.getDisplay_name());
                            }else {
                                txt_product_owner.setText("--");
                            }

                            txt_update_date.setText(AppUtils.getFormattedDateWithTime(courseData.getPost_modified()));

                            if (courseData.getNo_of_lesson() != null && !courseData.getNo_of_lesson().equals("null")){
                                txt_total_course_time.setText(courseData.getNo_of_lesson() + getString(R.string.topic) + txt_duration.getText());
                            }

                            if (courseData.getCourse_image() != null && courseData.getCourse_image().length()>0){

                                Glide.with(getApplicationContext())
                                        .asBitmap()
                                        .error(getResources().getDrawable(R.drawable.ic_no_photo))
                                        .placeholder(getResources().getDrawable(R.drawable.loader_progress))
                                        .load(AppUtils.CATEGORY_IMAGE_BASE_URL+courseData.getCourse_image()).
                                        into(new BitmapImageViewTarget(img_product_image) {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                img_product_image.setImageBitmap(resource);
                                            }
                                        });
                            }

                            wishList();

                        }*//* else {
                            lin_course_about.setVisibility(View.GONE);
                        }

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplication(), getString(R.string.course_detail_not_found), Toast.LENGTH_LONG).show();
                        lin_course_about.setVisibility(View.GONE);
                    }

                    btn_course_retry.setVisibility(View.GONE);
                } else {
                    btn_course_retry.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplication(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Course> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_course_retry.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }
        });
    }*/

    public List<PriceDataModel> getList() {
        List<PriceDataModel> models = new ArrayList<>();

        List<ServicesModel> models1 = new ArrayList<>();
        models1.add(new ServicesModel("1", "Ayurveda", ""));
        models1.add(new ServicesModel("2", "Oats", ""));
        models1.add(new ServicesModel("3", "Bowls", ""));
        models1.add(new ServicesModel("4", "Soul food", ""));
        models1.add(new ServicesModel("5", "Vegan", ""));
        models1.add(new ServicesModel("6", "Sugar free", ""));
        models.add(new PriceDataModel("1", "Nutrition", "monthly payment","9.95", "Per month",models1));

        List<ServicesModel> models2 = new ArrayList<>();
        models2.add(new ServicesModel("1", "Taping", ""));
        models2.add(new ServicesModel("2", "Acupressure", ""));
        models2.add(new ServicesModel("3", "Reflex zones", ""));
        models2.add(new ServicesModel("4", "Cupping", ""));
        models2.add(new ServicesModel("5", "Flossing", ""));
        models2.add(new ServicesModel("6", "Happy hands", ""));
        models2.add(new ServicesModel("6", "Treat fascia yourself", ""));
        models.add(new PriceDataModel("1", "Halth", "monthly payment","9.95", "Per month",models2));

        List<ServicesModel> models4 = new ArrayList<>();
        models4.add(new ServicesModel("1", "Booty", ""));
        models4.add(new ServicesModel("2", "Asana yoga", ""));
        models4.add(new ServicesModel("3", "Shoulder", ""));
        models4.add(new ServicesModel("4", "Karuna Yoga", ""));
        models4.add(new ServicesModel("5", "Tabata", ""));
        models4.add(new ServicesModel("6", "Beginner yoga", ""));
        models4.add(new ServicesModel("6", "HIIT", ""));
        models4.add(new ServicesModel("6", "Vinyasa yoga", ""));
        models4.add(new ServicesModel("6", "Mobility", ""));
        models.add(new PriceDataModel("1", "Sports", "monthly payment","9.95", "Per month",models4));

        List<ServicesModel> models3 = new ArrayList<>();
        models3.add(new ServicesModel("1", "Access to all topics", ""));
        models3.add(new ServicesModel("2", "Access to over 300+ lessons", ""));
        models3.add(new ServicesModel("3", "Constantly new courses and content", ""));
        models3.add(new ServicesModel("4", "Reports & documentation", ""));
        models.add(new PriceDataModel("1", "All-in-one", "monthly payment","9.95", "Per month",models3));


        return models;
    }

}