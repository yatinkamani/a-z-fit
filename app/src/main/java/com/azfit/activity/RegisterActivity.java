package com.azfit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.Model.Cutomers.CustomersDataModel;
import com.azfit.R;
import com.azfit.shared_preference.RememberLoginSession;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.utils.AppUtils.isNetworkAvailable;
import static com.azfit.utils.AppUtils.show_toast_short;
import static com.azfit.utils.AppUtils.validationEmail;
import static com.azfit.utils.AppUtils.validationPassword;
import static com.azfit.utils.AppUtils.validationPasswordMatch;
import static com.azfit.utils.AppUtils.validationText;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    SessionManager manager;
    RememberLoginSession loginSession;

    EditText ed_first_name, ed_userName, ed_last_name, ed_email, ed_password, ed_confirm_password;
    Button btn_submit;
    LoaderView loaderView;

    TextView txt_signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        BindView();
        AppUtils.validation = this;

        loaderView = new LoaderView(RegisterActivity.this);
        manager = new SessionManager(this);
        loginSession = new RememberLoginSession(this);

        btn_submit.setOnClickListener(this);
        txt_signIn.setOnClickListener(this);

    }

    public void BindView() {

        ed_first_name = findViewById(R.id.ed_firstName);
        ed_userName = findViewById(R.id.ed_userName);
        ed_last_name = findViewById(R.id.ed_lastName);
        ed_email = findViewById(R.id.ed_email);
        ed_password = findViewById(R.id.ed_password);
        ed_confirm_password = findViewById(R.id.ed_confirm_password);
        btn_submit = findViewById(R.id.btn_submit);
        txt_signIn = findViewById(R.id.txt_signIn);

    }

    @Override
    public void onClick(View v) {

        if (v == btn_submit){

            if (!validationText(ed_first_name)){
                return;

            } else if (!validationText(ed_userName)){
                return;

            } else if (!validationText(ed_last_name)){
                return;

            } else if (!validationEmail(ed_email)){
                return;

            }else if (!validationText(ed_password)){
                return;

            }  else if (!validationPassword(ed_password)){
                return;

            } else if (!validationText(ed_confirm_password)){
                return;

            } else if (!validationPasswordMatch(ed_password, ed_confirm_password)){
                return;

            } else {

                if (isNetworkAvailable(getApplicationContext())){
                    RegisterApi();
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
                }
            }
        } else if (txt_signIn == v){
//            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
    }

    public void RegisterApi() {
        if (!this.isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

        service.customerRegister(AppUtils.REGISTER, ""+ed_email.getText().toString(),
                ""+ed_password.getText().toString(), ""+ed_first_name.getText().toString(),
                ""+ed_last_name.getText().toString(), ""+ed_userName.getText().toString()).enqueue(
                new Callback<CustomersDataModel>() {
                    @Override
                    public void onResponse(@NotNull Call<CustomersDataModel> call, @NotNull Response<CustomersDataModel> response) {

                        Log.e("Tag Response Status", ""+response.isSuccessful());
                        Log.e("Tag Response Default", ""+response);

                        if (response.isSuccessful() && response.body() != null){
                            Log.e("Tag Response", ""+response.body());

                            if (response.body().getCustomer() != null){
                                manager.setIsLogin();
                                manager.setUserId(response.body().getCustomer().getId());
                                manager.setLoginUserDetail(response.body());
                                manager.setUserEmail(response.body().getCustomer().getEmail());

                                loginSession.setUserId(response.body().getCustomer().getId());

                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                            }
                        }
                        if (loaderView != null && loaderView.isShowing()){
                            loaderView.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<CustomersDataModel> call, @NotNull Throwable t) {
                        Log.e("Tag Error", t.getMessage());
                        if (loaderView != null && loaderView.isShowing()){
                            loaderView.dismiss();
                        }
                    }
                }
        );
    }
}