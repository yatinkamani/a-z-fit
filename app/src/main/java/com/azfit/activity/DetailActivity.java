package com.azfit.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azfit.Adapter.AdapterAnnouncement;
import com.azfit.Adapter.AdapterTopic;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.CartData.DBManagerData;
import com.azfit.CourseDatabase.CourseDBManager;
import com.azfit.Custome.WrappingLinearLayoutManager;
import com.azfit.Model.AnnounceMentModel.AnnouncementModel;
import com.azfit.Model.CartItem;
import com.azfit.Model.Course.Course;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.Orders.LineItems;
import com.azfit.Model.Orders.OrdersModel;
import com.azfit.Model.Product.ProductData;
import com.azfit.Model.Product.SearchProductModel;
import com.azfit.Model.Topic.TopicData;
import com.azfit.Model.Topic.TopicModel;
import com.azfit.Model.WishlistData;
import com.azfit.R;
import com.azfit.WishlistData.WishlistDBManager;
import com.azfit.fragment.ProfileFragment;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.BonceInterpolate;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.gson.Gson;
import com.moos.library.HorizontalProgressView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.utils.AppUtils.show_toast_short;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    DBManagerData dbManager;
    WishlistDBManager WishDbManager;
    CourseDBManager courseDBManager;
    List<CartItem> cartItem = new ArrayList<>();
    List<TopicData> topicData = new ArrayList<>();

    // no data
    View no_data;
    TextView txt_msg;
    Button btn_retry;

    //cart
    RelativeLayout counterValuePanel, counterPanel;
    TextView txt_count;

    LinearLayout lin_view;
    RoundedImageView img_product_image;
    ImageView img_fav;
    TextView txt_product_name, txt_product_owner, txt_course_level, txt_price, txt_rate;
    TextView txt_categories, txt_duration, txt_update_date;
    TextView txt_total_enroll;
    Button btn_buy, btn_startLesson;

    TextView txt_about_course, txt_what_learn;
    Button btn_course_retry, btn_topic_retry;
    LinearLayout lin_course_about;

    TextView txt_total_course_time;

    TextView txtCat, txtDur, txtLU;

    RecyclerView rcv_topics;

    ProductData productData;
    CourseData courseData = new CourseData();

    AdapterTopic adapterTopic;

    LoaderView loaderView;

    String cat_name = " ", course_id = "";
    private String product_id = "";

    SessionManager sessionManager;

    LinearLayout lin_course_progress;
    HorizontalProgressView progress_course;
    TextView progress_text;

    RecyclerView rcv_announcement;
    TextView txt_no_announcement;
    Button btn_announcement_retry;

    SwipeRefreshLayout swipe;
    boolean isRefresh = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        loaderView = new LoaderView(DetailActivity.this);
        dbManager = new DBManagerData(DetailActivity.this);
        WishDbManager = new WishlistDBManager(DetailActivity.this);
        courseDBManager = new CourseDBManager(DetailActivity.this);
        cartItem = dbManager.fetch();

        sessionManager = new SessionManager(DetailActivity.this);
        initView();
        getIntentData();

    }

    private void buildCounterDrawable() {
        txt_count = findViewById(R.id.count);
        AppUtils.refreshCartDB(txt_count, dbManager.fetch());

    }

    public void getIntentData() {

        if (getIntent() != null) {

            productData = new Gson().fromJson(getIntent().getStringExtra("product"), ProductData.class);

            if (getIntent().getStringExtra("course_id") != null) {
                course_id = getIntent().getStringExtra("course_id");
                productCourseApi(course_id,"");
            }else if(getIntent().getStringExtra("product_id") != null){
                product_id = getIntent().getStringExtra("product_id");
                productCourseApi("", product_id);
            }else {
                no_data.setVisibility(View.VISIBLE);
                lin_view.setVisibility(View.GONE);
            }

            if (getIntent().getStringExtra("cat_name") != null) {
                cat_name = getIntent().getStringExtra("cat_name");
                txt_categories.setText(cat_name);
            }

            /*if (getIntent().getStringExtra("product_id") != null){
//                product_id = getIntent().getStringExtra("product_id");
                product_id = "2072";
                productDetailApi("2072");
            }*/

//            setUpView();
        }
    }

    public void setUpView() {
        lin_view.setVisibility(View.VISIBLE);
        no_data.setVisibility(View.GONE);
        txt_product_name.setText(courseData.getPost_title());
        txt_rate.setText(productData.getAverage_rating());
        txt_price.setText(Html.fromHtml(productData.getPrice()+" €/Month"));
        /*if (productData.getImages() != null && productData.getImages().size() > 0) {

            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(productData.getImages().get(0).getSrc()).
                    into(new BitmapImageViewTarget(img_product_image) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            img_product_image.setImageBitmap(resource);
                        }
                    });
        }*/

        if (courseData.getCourse_image() != null && courseData.getCourse_image().length()>0){

            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL+courseData.getCourse_image())
                    .placeholder(getResources().getDrawable(R.drawable.ic_no_photo))
                    .into(new BitmapImageViewTarget(img_product_image) {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            img_product_image.setImageBitmap(resource);
                        }
                    });
        }

        /*if (productData.getCategories().size() > 0) {
            txt_categories.setText(productData.getCategories().get(0).getName());
        } else {
            txt_categories.setText("");
        }*/
        wishList();
//        productCourseApi();
    }

    public void wishList() {
        img_fav.setVisibility(View.VISIBLE);
        if (WishDbManager != null && WishDbManager.fetch().size() > 0) {
            for (WishlistData d : WishDbManager.fetch()) {

                if (d.getCourse_id().equals(courseData.getID())) {
                    img_fav.setVisibility(View.GONE);
                    break;
                }
            }
        } else {
            img_fav.setVisibility(View.VISIBLE);
        }
    }

    public void initView() {

        no_data = findViewById(R.id.no_data);
        txt_msg = findViewById(R.id.txt_message);
        btn_retry = findViewById(R.id.btn_retry);

        counterValuePanel = findViewById(R.id.counterValuePanel);
        counterPanel = findViewById(R.id.counterPanel);
        txt_count = findViewById(R.id.count);
        lin_view = findViewById(R.id.lin_view);

        img_product_image = findViewById(R.id.img_product_image);
        img_fav = findViewById(R.id.img_fav);
        img_fav.setVisibility(View.GONE);
        txt_product_name = findViewById(R.id.txt_product_name);
        txt_product_owner = findViewById(R.id.txt_product_owner);
        txt_course_level = findViewById(R.id.txt_course_level);
        txt_price = findViewById(R.id.txt_price);
        txt_rate = findViewById(R.id.txt_rate);

        txt_categories = findViewById(R.id.txt_categories);
        txt_categories.setSelected(true);
        txtCat = findViewById(R.id.txtCat);
        txtCat.setSelected(true);
        txt_duration = findViewById(R.id.txt_duration);
        txt_duration.setSelected(true);
        txtDur = findViewById(R.id.txtDur);
        txtDur.setSelected(true);
        txt_update_date = findViewById(R.id.txt_update_date);
        txt_update_date.setSelected(true);
        txtLU = findViewById(R.id.txtLU);
        txtLU.setSelected(true);

        txt_total_enroll = findViewById(R.id.txt_total_enroll);
        btn_buy = findViewById(R.id.btn_buy);
        btn_startLesson = findViewById(R.id.btn_startLesson);

        txt_about_course = findViewById(R.id.txt_about_course);
        txt_what_learn = findViewById(R.id.txt_what_learn);
        btn_course_retry = findViewById(R.id.btn_course_retry);
        btn_topic_retry = findViewById(R.id.btn_topic_retry);
        btn_course_retry.setOnClickListener(this);
        btn_topic_retry.setOnClickListener(this);
        lin_course_about = findViewById(R.id.lin_course_about);

        txt_total_course_time = findViewById(R.id.txt_total_course_time);

        rcv_topics = findViewById(R.id.rcv_topics);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isRefresh = true;
                getIntentData();
            }
        });

        rcv_announcement = findViewById(R.id.rcv_announcement);
        txt_no_announcement = findViewById(R.id.txt_no_announcement);
        btn_announcement_retry = findViewById(R.id.btn_announcement_retry);

        buildCounterDrawable();

        btn_buy.setOnClickListener(this);
        btn_startLesson.setOnClickListener(this);
        btn_retry.setOnClickListener(this);
        counterValuePanel.setOnClickListener(this);
        counterPanel.setOnClickListener(this);
        img_fav.setOnClickListener(this);
        btn_announcement_retry.setOnClickListener(this);

    }

    public void setRefreshDismiss(){
        if (swipe != null && swipe.isRefreshing()){
            swipe.setRefreshing(false);
            isRefresh = false;
        }
    }

    public void setRefreshEnable(){
        if (swipe != null && !swipe.isRefreshing()){
            swipe.setRefreshing(true);
        }
    }

    public void initializeTopic() {

        if (topicData != null && topicData.size() > 0) {
            rcv_topics.setVisibility(View.VISIBLE);
            btn_topic_retry.setVisibility(View.GONE);

            rcv_topics.setLayoutManager(new WrappingLinearLayoutManager(getApplicationContext()));
//            rcv_topics.setItemAnimator(new DefaultItemAnimator());
            rcv_topics.setHasFixedSize(false);
            rcv_topics.setNestedScrollingEnabled(false);

            adapterTopic = new AdapterTopic(DetailActivity.this, topicData, courseData);
            rcv_topics.setAdapter(adapterTopic);
        } else {
            rcv_topics.setVisibility(View.GONE);
            btn_topic_retry.setVisibility(View.VISIBLE);
        }
    }

    public boolean checkIsCart() {

        boolean isCart = false;
        if (dbManager.fetch() != null && dbManager.fetch().size() > 0 && productData != null) {

            for (CartItem item : dbManager.fetch()) {
                if (item.getProduct().getId().equals(productData.getId())) {
                    isCart = true;
                    break;
                }
            }
        }
        return isCart;
    }

    @Override
    public void onClick(View v) {

        if (v == btn_buy) {

            BonceInterpolate.clickViewButton(getApplicationContext(), v);
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            if (!checkIsCart() && productData != null) {
                dbManager.insert("1", productData.getId(), "1", productData);
                buildCounterDrawable();
                startActivity(intent);
            } else {
                startActivity(intent);
            }

        } else if (v == btn_retry) {
            if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                productTopic();
            } else {
                AppUtils.show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
            }
        } else if (v == btn_topic_retry) {
            if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                productTopic();
            } else {
                AppUtils.show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
            }
        }else if (v == btn_announcement_retry) {
            if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                courseAnnouncement();
            } else {
                AppUtils.show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
            }
        } else if (v == btn_course_retry) {

            if (AppUtils.isNetworkAvailable(getApplicationContext())) {
//                productDetailApi(product_id);
                if (!course_id.equals("")){
                    productCourseApi(course_id,"");
                }else {
                    productCourseApi("",product_id);
                }
            } else {
                AppUtils.show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
            }

        } else if (v == img_fav) {
            BonceInterpolate.clickViewButton(getApplicationContext(), v);
            if (productData != null && courseData != null) {
                WishDbManager.insert(productData.getId(), cat_name, courseData.getCourse_image(), productData.getName(), productData.getAverage_rating(), courseData.getID(), " ", courseData.get_tutor_course_level(), courseData.getPost_title());
            } else {
                WishDbManager.insert("no", cat_name, courseData.getCourse_image(), courseData.getPost_title(), "0.00", courseData.getID(), " ", courseData.get_tutor_course_level(), courseData.getPost_title());
            }
            AppUtils.show_toast_short(getApplicationContext(), getString(R.string.wishlist_added_successfully));
            img_fav.setVisibility(View.GONE);
            wishList();

        } else if (v == counterPanel) {
            BonceInterpolate.clickViewButton(getApplicationContext(),v);
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            startActivity(intent);
        } else if (v == btn_startLesson){

            BonceInterpolate.clickViewButton(getApplicationContext(), v);
            if (btn_startLesson.getText().toString().equals(getString(R.string.start_lesson))){

                if (!courseDBManager.checkStartCourse(course_id)){
                    courseDBManager.insertCourse(courseData.getID(), courseData.getPost_title(), courseData.getNo_of_lesson());
                }
                initializeCourseButton();

            }else if (btn_startLesson.getText().toString().equals(getString(R.string.complete_course))){

                if (courseDBManager.getFreeActiveCourseById(courseData.getID()) != null){

                    if (courseDBManager.getFreeActiveCourseById(courseData.getID()).getStatus().equals("0")){

                        int left = Integer.parseInt(courseDBManager.getFreeActiveCourseById(courseData.getID()).getNo_of_lesson()) -
                                Integer.parseInt(courseDBManager.getFreeActiveCourseById(courseData.getID()).getComplete_lesson());

                        show_toast_short(getApplicationContext(), left+ getString(R.string.lesson_left_for_this_course));
                    }
                }
                Log.e("Tag DB Course", ""+courseDBManager.getFreeActiveCourseById(courseData.getID()));
            }
        }
    }

    //    public List<TopicModel> getTopicList() {
//
//        List<TopicModel> topicModels = new ArrayList<>();
//
//        List<SubTopicModel> models = new ArrayList<>();
//        models.add(new SubTopicModel("1","1","Warm Up","6:00"));
//        models.add(new SubTopicModel("1","2","Warm Up 2","0:30"));
//        models.add(new SubTopicModel("1","3","Warm Up 3","2:00"));
//        models.add(new SubTopicModel("1","4","Warm Up 4","7:00"));
//        models.add(new SubTopicModel("1","5","Warm Up 5","1:20"));
//        models.add(new SubTopicModel("1","6","Warm Up 6","3:10"));
//        topicModels.add(new TopicModel("1","Taping","",models));
//
//        List<SubTopicModel> models1 = new ArrayList<>();
//        models1.add(new SubTopicModel("2","1","Dynamic Back Stretch","6:00"));
//        models1.add(new SubTopicModel("2","2","Dynamic Back Stretch 2","0:30"));
//        models1.add(new SubTopicModel("2","3","Dynamic Back Stretch 3","2:00"));
//        models1.add(new SubTopicModel("2","4","Dynamic Back Stretch 4","7:00"));
//        models1.add(new SubTopicModel("2","5","Dynamic Back Stretch 5","1:20"));
//        models1.add(new SubTopicModel("2","6","Dynamic Back Stretch 6","3:10"));
//        topicModels.add(new TopicModel("2","Akupressur","",models1));
//
//        List<SubTopicModel> models2 = new ArrayList<>();
//        models2.add(new SubTopicModel("3","1","Knee Push Ups","6:00"));
//        models2.add(new SubTopicModel("3","2","Knee Push Ups 2","0:30"));
//        models2.add(new SubTopicModel("3","3","Knee Push Ups 3","2:00"));
//        models2.add(new SubTopicModel("3","4","Knee Push Ups 4","7:00"));
//        models2.add(new SubTopicModel("3","5","Knee Push Ups 5","1:20"));
//        models2.add(new SubTopicModel("3","6","Knee Push Ups 6","3:10"));
//        topicModels.add(new TopicModel("3","Cupping","",models2));
//
//        List<SubTopicModel> models3 = new ArrayList<>();
//        models3.add(new SubTopicModel("4","1","Bicycle Kick","6:00"));
//        models3.add(new SubTopicModel("4","2","Bicycle Kick 2","0:30"));
//        models3.add(new SubTopicModel("4","3","Bicycle Kick 3","2:00"));
//        models3.add(new SubTopicModel("4","4","Bicycle Kick 4","7:00"));
//        models3.add(new SubTopicModel("4","5","Bicycle Kick 5","1:20"));
//        models3.add(new SubTopicModel("4","6","Bicycle Kick 6","3:10"));
//        topicModels.add(new TopicModel("4","Massages","",models3));
//
//        return topicModels;
//    }

    public void getOrderApi(String product_id) {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            if (!this.isFinishing()){
                loaderView.show();
            }
            ApiService apiService = RetrofitClient.createService(ApiService.class);
            apiService.getOrders(AppUtils.MY_ORDERS, sessionManager.getUserId()).enqueue(new Callback<OrdersModel>() {
                @Override
                public void onResponse(@NotNull Call<OrdersModel> call, @NotNull Response<OrdersModel> response) {

                    LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                    LogUtils.error(LoginActivity.class, "Tag Response Order  " + response);

                    boolean valid = false;
                    if (response.isSuccessful() && response.body() != null) {

                        try {
                            LogUtils.error(LoginActivity.class, "Tag Response Order" + new JSONObject(response.body().toString()).toString(2));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        orders = new ArrayList<>();
                        if (response.body().getOrders() != null && response.body().getOrders().size() > 0) {

                            for (int i = 0; i < response.body().getOrders().size(); i++) {

//                                if (response.body().getOrders().get(i).getStatus().equals("completed")){
                                if (response.body().getOrders().get(i).getLine_items() != null &&
                                        response.body().getOrders().get(i).getLine_items().size() > 0) {

                                  /*  if (dbManager != null && dbManager.fetch().size() > 0) {
                                        for (CartItem item : dbManager.fetch()) {*/

                                            for (LineItems lineItem : response.body().getOrders().get(i).getLine_items()) {

                                                if (lineItem.getProduct_id().equals(product_id)) {

                                                    if (response.body().getOrders().get(i).getStatus().equals("completed")) {

                                                        Log.e("Tag check Date", AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()));
                                                        if (AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()).equals("1")) {
                                                            valid = true;
                                                        }else {
                                                            valid = false;
                                                            break;
                                                        }
                                                    } else if (response.body().getOrders().get(i).getStatus().equals("cancelled") || response.body().getOrders().get(i).getStatus().equals("failed")) {
                                                        valid = true;
                                                    } else {
                                                        valid = false;
                                                        break;
                                                    }
                                                }else {
                                                    valid = true;
                                                }
                                            }
                                       /* }
                                    }*/
                                }
                                if (!valid) {
                                    break;
                                }
                            }
                        } else {
                            valid = true;
                        }

                    } else {
                        valid = true;
                    }

                    if (!valid){
                        courseData.set_tutor_course_price_type("free");
                        /*if (courseData.get_tutor_course_price_type().equals("free")){
                            btn_buy.setVisibility(View.GONE);
                            if (courseDBManager.getFreeActiveCourseById(courseData.getID()) != null){
                                if (courseDBManager.getFreeActiveCourseById(courseData.getID()).getStatus().equals("1")){
                                    btn_startLesson.setVisibility(View.GONE);
                                }else {
                                    btn_startLesson.setVisibility(View.VISIBLE);
                                    btn_startLesson.setText(getString(R.string.complete_course));
                                }
                            }else {
                                btn_startLesson.setVisibility(View.VISIBLE);
                                btn_startLesson.setText(getString(R.string.start_lesson));
                            }
                        }*/
                        initializeCourseButton();
                    }

                    Log.e("Tag validate", ""+valid);

                    if (loaderView != null && loaderView.isShowing()) {
                        loaderView.dismiss();
                    }
                    setRefreshDismiss();

                }

                @Override
                public void onFailure(@NotNull Call<OrdersModel> call, @NotNull Throwable t) {

                    LogUtils.error(ProfileFragment.class, t.getMessage());

                    if (loaderView != null && loaderView.isShowing()) {
                        loaderView.dismiss();
                    }
                    setRefreshDismiss();
                }
            });
        }
    }

    private void initializeCourseButton() {
        if (sessionManager.isLoggedIn() && courseData.get_tutor_course_price_type().equals("free")){
            btn_buy.setVisibility(View.GONE);
            if (courseDBManager != null && courseDBManager.getFreeActiveCourseById(courseData.getID()) != null){
                if (courseDBManager.getFreeActiveCourseById(courseData.getID()).getStatus().equals("1")){
                    btn_startLesson.setVisibility(View.GONE);
                }else {
                    btn_startLesson.setVisibility(View.VISIBLE);
                    btn_startLesson.setText(getString(R.string.complete_course));
                }
                setCourseProgress();
            }else {
                btn_startLesson.setVisibility(View.VISIBLE);
                btn_startLesson.setText(getString(R.string.start_lesson));
            }
        } else if (courseData.get_tutor_course_price_type().equals("free")){
            btn_buy.setVisibility(View.GONE);
            if (courseDBManager != null && courseDBManager.getFreeActiveCourseById(courseData.getID()) != null){
                if (courseDBManager.getFreeActiveCourseById(courseData.getID()).getStatus().equals("1")){
                    btn_startLesson.setVisibility(View.GONE);
                }else {
                    btn_startLesson.setVisibility(View.VISIBLE);
                    btn_startLesson.setText(getString(R.string.complete_course));
                }
                setCourseProgress();
            }else {
                btn_startLesson.setVisibility(View.VISIBLE);
                btn_startLesson.setText(getString(R.string.start_lesson));
            }
        }
    }

    public void productDetailApi(String string) {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getFilteredProduct(string).enqueue(new Callback<SearchProductModel>() {
            @Override
            public void onResponse(@NotNull Call<SearchProductModel> call, @NotNull Response<SearchProductModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body().getAll_products() != null && response.body().getAll_products().size() > 0) {

                        productData = response.body().getAll_products().get(0);

                        setUpView();

                    } else {
                        no_data.setVisibility(View.VISIBLE);
                        lin_view.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                    }

                } else {
                    no_data.setVisibility(View.VISIBLE);
                    lin_view.setVisibility(View.GONE);
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();
            }

            @Override
            public void onFailure(@NotNull Call<SearchProductModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                no_data.setVisibility(View.VISIBLE);
                lin_view.setVisibility(View.GONE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();
            }
        });
    }

    public void productCourseApi(final String course_id, final String product_id) {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCourse(AppUtils.COURSE, "").enqueue(new Callback<Course>() {

            @Override
            public void onResponse(@NotNull Call<Course> call, @NotNull Response<Course> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();

//                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null && response.body().getResult().size() > 0) {

                        for (CourseData course : response.body().getResult()) {
//                            if (productData.getName().toLowerCase().equals(course.getPost_title().toLowerCase())){

                            if (!course_id.equals("")){

                                if (course.getID().equals(course_id)) {
                                    courseData = course;
                                    productTopic();
                                    courseAnnouncement();
                                    if (sessionManager.isLoggedIn()){
                                        if (courseData.get_tutor_course_product_id() != null){
                                            getOrderApi(courseData.get_tutor_course_product_id());
                                        }
                                    }
                                }

                            } else if (!product_id.equals("")){
                                if (course.get_tutor_course_product_id() != null &&
                                        course.get_tutor_course_product_id().equals(product_id)) {
                                    course.set_tutor_course_price_type("free");
                                    courseData = course;
                                    productTopic();
                                    courseAnnouncement();
                                }
                            }
                        }

                        if (courseData != null) {

                            lin_course_about.setVisibility(View.VISIBLE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                txt_about_course.setText(Html.fromHtml(courseData.getPost_excerpt(), Html.FROM_HTML_MODE_LEGACY));
                            } else {
                                txt_about_course.setText(Html.fromHtml(courseData.getPost_excerpt()));
                            }
                            if (courseData.get_tutor_course_benefits() != null && !courseData.get_tutor_course_benefits().equals("null")) {

                                try {
                                    String s = courseData.get_tutor_course_benefits().replaceAll("\r\n", "\n\u2022 \t");
                                    Log.e("Tag spot", s);
                                    txt_what_learn.setText("\u2022 \t" + s);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if (courseData.get_tutor_course_level() != null && !courseData.get_tutor_course_level().equals("null")) {
                                txt_course_level.setText(getString(R.string.course_level)+courseData.get_tutor_course_level().replaceAll("_", " "));
                            }else {
                                txt_course_level.setText(getString(R.string.course_level)+" --");

                            }
                            txt_product_name.setText(courseData.getPost_title());
                            if (courseData.get_course_duration() != null && !courseData.get_course_duration().equals("null")) {

                                try {
                                    String c_d = courseData.get_course_duration();
                                    String duration = c_d.split("\"")[3] + "h " +
                                            c_d.split("\"")[7] + "m " +
                                            c_d.split("\"")[11] + "s ";

                                    txt_duration.setText(duration);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if (courseData.get_tutor_course_product_id() != null && !courseData.get_tutor_course_product_id().equals("null")) {
                                productDetailApi(courseData.get_tutor_course_product_id());
                            } else {
                                courseData.set_tutor_course_price_type("free");
                                btn_buy.setVisibility(View.GONE);
                                txt_price.setText(getString(R.string.zero_zerozero));
                                txt_rate.setText(getString(R.string.zero_zero));
                            }

                            if (courseData.get_tutor_course_price_type().equals("free")) {
                                initializeCourseButton();
                            }

                            if (courseData.getDisplay_name() != null && courseData.getDisplay_name().length()>0){
                                txt_product_owner.setText(getString(R.string.von)+courseData.getDisplay_name());
                            }else {
                                txt_product_owner.setText("--");
                            }

                            txt_update_date.setText(AppUtils.getFormattedDateWithTime(courseData.getPost_modified()));

                            if (courseData.getNo_of_lesson() != null && !courseData.getNo_of_lesson().equals("null")){
                                txt_total_course_time.setText(courseData.getNo_of_lesson() + getString(R.string.topic) + txt_duration.getText());
                            }

                            if (courseData.getCourse_image() != null && courseData.getCourse_image().length()>0){

                                Glide.with(getApplicationContext())
                                        .asBitmap()
                                        .error(getResources().getDrawable(R.drawable.ic_no_photo))
                                        .placeholder(getResources().getDrawable(R.drawable.loader_progress))
                                        .load(AppUtils.CATEGORY_IMAGE_BASE_URL+courseData.getCourse_image()).
                                        into(new BitmapImageViewTarget(img_product_image) {
                                            @Override
                                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                img_product_image.setImageBitmap(resource);
                                            }
                                        });
                            }

                            if (courseData.getPost_type() != null && courseData.getPost_type().equals("") && !cat_name.equals(" ")){
                                txt_categories.setText(courseData.getPost_type());
                            }

                            txt_total_enroll.setText(courseData.getMenu_order());

                            wishList();

                        } else {
                            lin_course_about.setVisibility(View.GONE);

                        }

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplication(), getString(R.string.course_detail_not_found), Toast.LENGTH_LONG).show();
                        lin_course_about.setVisibility(View.GONE);

                    }

                    btn_course_retry.setVisibility(View.GONE);
                } else {

                    btn_course_retry.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplication(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Course> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_course_retry.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();
            }
        });
    }

    public void productTopic() {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createServiceWoo(ApiService.class);

        service.getTopic(courseData.getID()).enqueue(new Callback<TopicModel>() {

            @Override
            public void onResponse(@NotNull Call<TopicModel> call, @NotNull Response<TopicModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null && response.body().getData().size() > 0) {

                        topicData = response.body().getData();
                        initializeTopic();

                        /*for (TopicData course : response.body().getData()){
                            if (productData.getName().toLowerCase().equals(course.getPost_title().toLowerCase())){
                                courseData = new CourseData();
                                courseData = course;
                            }
                        }*/

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplication(), getString(R.string.course_topic_not_found), Toast.LENGTH_LONG).show();
                        btn_topic_retry.setVisibility(View.VISIBLE);
                    }

                } else {
                    btn_topic_retry.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplication(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<TopicModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_topic_retry.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();
            }
        });
    }

    public void courseAnnouncement() {

        rcv_announcement.setVisibility(View.GONE);
        txt_no_announcement.setVisibility(View.GONE);

        if (!this.isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createServiceWoo(ApiService.class);
        service.getAnnouncementLesson(courseData.getID()).enqueue(new Callback<AnnouncementModel>() {

            @Override
            public void onResponse(@NotNull Call<AnnouncementModel> call, @NotNull Response<AnnouncementModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null && response.body().getData().size() > 0) {

                        rcv_announcement.setLayoutManager(new WrappingLinearLayoutManager(getApplicationContext()));
                        rcv_announcement.setNestedScrollingEnabled(false);
                        rcv_announcement.setHasFixedSize(false);
                        AdapterAnnouncement adapterAnnouncement = new AdapterAnnouncement(getApplicationContext(), response.body().getData());
                        rcv_announcement.setAdapter(adapterAnnouncement);
                        btn_announcement_retry.setVisibility(View.GONE);
                        rcv_announcement.setVisibility(View.VISIBLE);

                    } else {

//                        Toast.makeText(getApplication(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                        btn_announcement_retry.setVisibility(View.GONE);
                        txt_no_announcement.setVisibility(View.VISIBLE);
                    }

                } else {
                    btn_announcement_retry.setVisibility(View.VISIBLE);
//                    Toast.makeText(getApplication(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(@NotNull Call<AnnouncementModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_announcement_retry.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
                setRefreshDismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        buildCounterDrawable();
        setCourseProgress();
        if (topicData != null && topicData.size() > 0){
            initializeTopic();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (loaderView != null && loaderView.isShowing()) {
            loaderView.dismiss();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (loaderView != null && loaderView.isShowing()) {
            loaderView.dismiss();
        }
        super.onDestroy();
    }

    public void setCourseProgress() {

        lin_course_progress = findViewById(R.id.lin_course_progress);
        progress_course = findViewById(R.id.progress_course);
        progress_text = findViewById(R.id.progress_text);

        if (courseDBManager != null && courseDBManager.getFreeActiveCourseById(courseData.getID()) != null) {
            lin_course_progress.setVisibility(View.VISIBLE);

            String t = courseDBManager.getFreeActiveCourseById(courseData.getID()).getNo_of_lesson();
            String c = courseDBManager.getFreeActiveCourseById(courseData.getID()).getComplete_lesson();
            Log.e("Tag progress T C", t+" "+c);
            int total_lesson = Integer.parseInt(t);
            int complete_lesson = Integer.parseInt(c);

            int end = Math.abs((complete_lesson * 100) / total_lesson);
            Log.e("Tag progress end", ""+end);
            progress_course.setEndProgress(end);
            progress_course.setProgressDuration(1500);
            progress_course.startProgressAnimation();

            progress_course.setProgressViewUpdateListener(new HorizontalProgressView.HorizontalProgressUpdateListener() {
                @Override
                public void onHorizontalProgressStart(View view) {

                }

                @Override
                public void onHorizontalProgressUpdate(View view, float progress) {
                    int progressInt = (int) progress;
                    progress_text.setText(progressInt+"  %");
                }

                @Override
                public void onHorizontalProgressFinished(View view) {

                }
            });
        }
    }

}