package com.azfit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.Model.Cutomers.CustomersDataModel;
import com.azfit.R;
import com.azfit.shared_preference.RememberLoginSession;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.utils.AppUtils.instance;
import static com.azfit.utils.AppUtils.isNetworkAvailable;
import static com.azfit.utils.AppUtils.show_toast_short;
import static com.azfit.utils.AppUtils.validationText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    SessionManager manager;
    RememberLoginSession loginSession;

    EditText ed_userName, ed_password;
    CheckBox checkbox;
    TextView txt_forgot, text_sign_up;
    Button btn_submit;
    LinearLayout lin_google, lin_facebook;
    LoaderView loaderView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        BindView();
        AppUtils.validation = this;
        loaderView = new LoaderView(LoginActivity.this);
        manager = new SessionManager(this);
        loginSession = new RememberLoginSession(this);

        btn_submit.setOnClickListener(this);
        txt_forgot.setOnClickListener(this);
        text_sign_up.setOnClickListener(this);
        lin_google.setOnClickListener(this);
        lin_facebook.setOnClickListener(this);

        passwordVisibility();
    }

    public void BindView() {

        ed_password = findViewById(R.id.ed_password);
        ed_userName = findViewById(R.id.ed_userName);

        txt_forgot = findViewById(R.id.txt_forgot);
        text_sign_up = findViewById(R.id.text_sign_up);

        lin_google = findViewById(R.id.lin_google);
        lin_facebook = findViewById(R.id.lin_facebook);

        btn_submit = findViewById(R.id.btn_submit);
        checkbox = findViewById(R.id.checkbox);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void passwordVisibility() {
        checkbox.setChecked(false);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int start,end;
                Log.i("inside checkbox change",""+isChecked);
                if(!isChecked){
                    start = ed_password.getSelectionStart();
                    end = ed_password.getSelectionEnd();
                    ed_password.setTransformationMethod(new PasswordTransformationMethod());;
                    ed_password.setSelection(start,end);
                }else{
                    start=ed_password.getSelectionStart();
                    end=ed_password.getSelectionEnd();
                    ed_password.setTransformationMethod(null);
                    ed_password.setSelection(start,end);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v == btn_submit){

            if (!validationText(ed_userName)) {
                return;

            } else if (!validationText(ed_password)) {
                return;

            } else {
                if (isNetworkAvailable(getApplicationContext())){
                    LoginApi();
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
                }
            }
        } else if (v == text_sign_up) {
            Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            startActivity(intent);
        } else if (v == txt_forgot) {
            Intent intent = new Intent(getApplicationContext(), ForgetActivity.class);
            startActivity(intent);
        } else if (v == lin_google) {
            /*Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
            startActivity(intent);*/
            Toast.makeText(getApplicationContext(), "Google sign in not connect to server", Toast.LENGTH_LONG).show();
        } else if (v == lin_facebook) {

            /*Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
            startActivity(intent);*/

            Toast.makeText(getApplicationContext(), "credential invalid", Toast.LENGTH_LONG).show();
        }

    }

    public void LoginApi() {

        if (!this.isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCustomer("get_product","1");

        service.customerLogin(AppUtils.LOGIN, ed_userName.getText().toString(),
                ed_password.getText().toString()).enqueue(new Callback<CustomersDataModel>() {
            @Override
            public void onResponse(@NotNull Call<CustomersDataModel> call, @NotNull Response<CustomersDataModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                if (response.isSuccessful() && response.body() != null){

                    LogUtils.error(LoginActivity.class, "Tag Response "+response.body());

                    if (response.body().getCustomer() != null){
                        manager.setIsLogin();
                        manager.setUserId(response.body().getCustomer().getId());
                        manager.setLoginUserDetail(response.body());
                        manager.setUserEmail(response.body().getCustomer().getEmail());
                        manager.setLoginUserDetail(response.body());

                        loginSession.setUserId(response.body().getCustomer().getId());

                        if (instance == null){
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }
                        instance = null;
                        setResult(RESULT_OK);
                        finish();
                        Toast.makeText(getApplicationContext(), getString(R.string.successfully_login), Toast.LENGTH_LONG).show();
                    } else if (response.body().getStatus().equals("false")) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                    }
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<CustomersDataModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }
        });
    }

}