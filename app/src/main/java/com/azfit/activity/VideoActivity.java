package com.azfit.activity;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.azfit.CourseDatabase.CourseDBManager;
import com.azfit.Model.TopicLesson.TopicLessonData;
import com.azfit.R;
import com.google.gson.Gson;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoActivity extends AppCompatActivity {

    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    CourseDBManager courseDBManager;
    TopicLessonData topicLessonData;
    int orientation;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        final int orientation = display.getOrientation();*/
        setContentView(R.layout.activity_view_videos);

        topicLessonData = new Gson().fromJson(getIntent().getStringExtra("lessonData"), TopicLessonData.class);

        courseDBManager = new CourseDBManager(VideoActivity.this);
        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        webView = (WebView) findViewById(R.id.webView);

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSaveFormData(true);
        // webView.loadUrl("https://www.youtube.com/embed/47yJ2XCRLZs");

        if (savedInstanceState != null) {
            if (topicLessonData.getVideo() != null && topicLessonData.getVideo().size() > 0) {
                if (!topicLessonData.getVideo().get(0).getSource_external_url().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_external_url());
                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_external_url());
                } else if (!topicLessonData.getVideo().get(0).getSource_vimeo().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_vimeo());
                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_vimeo());
                } else if (!topicLessonData.getVideo().get(0).getSource_youtube().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_youtube());
                    String video = "<iframe class=\"youtube-player\" style=\"border: 0; width: 100%; height: 100%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src="
                            + topicLessonData.getVideo().get(0).getSource_youtube() +
                            "?autoplay=1"
                            + "&fs=0\" frameborder=\"0\">\n"
                            + "</iframe>\n";

//                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_youtube());
                    webView.loadData(video, "text/html", "UTF-8");
                }
            } else if (topicLessonData.getAttachments() != null && topicLessonData.getAttachments().size() > 0 && !topicLessonData.getAttachments().get(0).equals("")) {
//                changeOrientation(true);
                Log.e("file_path", topicLessonData.getAttachments().get(0).toString());
                webView.loadUrl(topicLessonData.getAttachments().get(0).toString());
            } else if (topicLessonData.getPost_content() != null && !topicLessonData.getPost_content().equals("")) {
//                changeOrientation(false);
                Log.e("file_path", topicLessonData.getPost_content());
                webView.loadData(topicLessonData.getPost_content(), "text/html", "UTF-8");
            } else if (topicLessonData.getThumbnail() != null && !topicLessonData.getThumbnail().equals("null") && !topicLessonData.getThumbnail().equals("")) {
                Log.e("file_path", topicLessonData.getThumbnail());
//                changeOrientation(false);
                webView.loadUrl(topicLessonData.getThumbnail());
            }
        } else {
            if (topicLessonData.getVideo() != null && topicLessonData.getVideo().size() > 0) {
                if (!topicLessonData.getVideo().get(0).getSource_external_url().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_external_url());
                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_external_url());
                } else if (!topicLessonData.getVideo().get(0).getSource_vimeo().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_vimeo());
                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_vimeo());
                } else if (!topicLessonData.getVideo().get(0).getSource_youtube().equals("")) {
//                    changeOrientation(true);
                    Log.e("file_path", topicLessonData.getVideo().get(0).getSource_youtube());

                    String video = "<iframe class=\"youtube-player\" style=\"border: 0; width: 100%; height: 100%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src=\"http://www.youtube.com/embed/"
                            + extractYTId(topicLessonData.getVideo().get(0).getSource_youtube()) +
                            "?autoplay=1"
                            + "&fs=0\" frameborder=\"0\">\n"
                            + "</iframe>\n";

//                    webView.loadUrl(topicLessonData.getVideo().get(0).getSource_youtube());
                    webView.loadData(video, "text/html", "UTF-8");
                }
            } else if (topicLessonData.getAttachments() != null && topicLessonData.getAttachments().size() > 0 && !topicLessonData.getAttachments().get(0).equals("")) {
//                changeOrientation(true);
                Log.e("file_path", topicLessonData.getAttachments().get(0).toString());
                webView.loadUrl(topicLessonData.getAttachments().get(0).toString());
            } else if (topicLessonData.getPost_content() != null && !topicLessonData.getPost_content().equals("")) {
//                changeOrientation(false);
                Log.e("file_path", topicLessonData.getPost_content());
                webView.loadData(topicLessonData.getPost_content(), "text/html", "UTF-8");
            } else if (topicLessonData.getThumbnail() != null && !topicLessonData.getThumbnail().equals("null") && !topicLessonData.getThumbnail().equals("")) {
                Log.e("file_path", topicLessonData.getThumbnail());
//                changeOrientation(false);
                webView.loadUrl(topicLessonData.getThumbnail());
            }
        }
        AddDatabase();

    }

    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
        }
        return vId;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("lessonData", new Gson().toJson(topicLessonData));
    }

    public void AddDatabase() {
        boolean isAdded = false;
        if (courseDBManager.checkCompleteLesson(topicLessonData.getCourse_id(), topicLessonData.getID())) {
            isAdded = true;
        }

        if (!isAdded) {
            courseDBManager.insertLesson(topicLessonData.getCourse_id(), topicLessonData.getID(), topicLessonData.getPost_title());
        }
    }

    public void changeOrientation(boolean setLandscape) {

        if (setLandscape) {
            if (orientation != Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else {
            if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (inCustomView()) {
            hideCustomView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(VideoActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(VideoActivity.this)
                .setTitle(getString(R.string.app_name))
                .setMessage(R.string.complate_this_lesson)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                VideoActivity.this.finish();
                                boolean isAdded = false;

                                if (courseDBManager.checkCompleteLesson(topicLessonData.getCourse_id(), topicLessonData.getID()) &&
                                        courseDBManager.checkPlayingLesson(topicLessonData.getCourse_id(), topicLessonData.getID()).equals("1")) {
                                    isAdded = true;
                                }

                                if (!isAdded) {
                                    courseDBManager.completeLesson(topicLessonData);
                                }
                                /*for (TopicLessonData data : courseDBManager.getCompleteLessonById(topicLessonData.getCourse_id())){
                                    if (data.getID().equals(topicLessonData.getID())){
                                        isAdded = true;
                                        break;
                                    }
                                }*/

                            }
                        })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        VideoActivity.this.finish();
                    }
                })
                .show();

        overridePendingTransition(0, 0);
    }

}
