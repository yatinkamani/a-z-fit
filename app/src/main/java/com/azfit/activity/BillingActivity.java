package com.azfit.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.CartData.DBManagerData;
import com.azfit.Model.CartItem;
import com.azfit.Model.CouponCode.CouponData;
import com.azfit.PayPal.PayPalConfig;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.activity.CartActivity.PAY_PAL_REQUEST_CODE;
import static com.azfit.utils.AppUtils.instance;
import static com.azfit.utils.AppUtils.roundTwoDecimals;
import static com.azfit.utils.AppUtils.show_toast_short;

public class BillingActivity extends AppCompatActivity implements View.OnClickListener {

    SessionManager sessionManager;
    DBManagerData dbManager;

    LinearLayout lin_main, lin_view, lin_billing;
    RelativeLayout rel_signIn, rel_couponCode;
    AutoCompleteTextView AutoTxt_salutation;
    EditText ed_name, ed_surname, ed_companyName, ed_streetName, ed_room, ed_zipCode, ed_cityArea, ed_telephone, ed_email, ed_comment;

    TextView txt_subTotal, txt_total, txt_tax, txt_finalTax;

    LinearLayout lin_ordered;
    Button btn_buyNow, btn_more;
    TextView txt_order_msg;

    LoaderView loaderView;
    boolean isCoupon = false;
    CouponData couponData = null;

    RelativeLayout rel_coupon;
    TextView txt_coupon;

    carbon.widget.LinearLayout lin_cod;
    LinearLayout lin_overlay;

    ImageButton btn_payPal;

    //PayPal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            .environment(PayPalConfig.PAYPAL_ENVIRONMENT)
            // .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);
    private String paymentAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        AppUtils.validation = this;
        sessionManager = new SessionManager(BillingActivity.this);
        dbManager = new DBManagerData(BillingActivity.this);
        loaderView = new LoaderView(BillingActivity.this);

        initView();

        if (sessionManager.isLoggedIn()) {
            rel_signIn.setVisibility(View.GONE);
            fillDetail();
        } else {
            rel_signIn.setVisibility(View.VISIBLE);
        }
    }

    public void initView() {

        lin_main = findViewById(R.id.lin_main);
        lin_view = findViewById(R.id.lin_view);
        lin_billing = findViewById(R.id.lin_billing);

        rel_signIn = findViewById(R.id.rel_signIn);

        AutoTxt_salutation = findViewById(R.id.AutoTxt_salutation);

        ed_name = findViewById(R.id.ed_name);
        ed_surname = findViewById(R.id.ed_surname);
        ed_companyName = findViewById(R.id.ed_companyName);
        ed_streetName = findViewById(R.id.ed_streetName);
        ed_room = findViewById(R.id.ed_room);
        ed_zipCode = findViewById(R.id.ed_zipCode);
        ed_cityArea = findViewById(R.id.ed_cityArea);
        ed_telephone = findViewById(R.id.ed_telephone);
        ed_email = findViewById(R.id.ed_email);
        ed_comment = findViewById(R.id.ed_comment);

        txt_subTotal = findViewById(R.id.txt_subTotal);
        txt_total = findViewById(R.id.txt_total);
        txt_tax = findViewById(R.id.txt_tax);
        txt_finalTax = findViewById(R.id.txt_finalTax);

        lin_ordered = findViewById(R.id.lin_ordered);
        btn_buyNow = findViewById(R.id.btn_buyNow);
        btn_more = findViewById(R.id.btn_more);
        txt_order_msg = findViewById(R.id.txt_order_msg);

        rel_coupon = findViewById(R.id.rel_coupon);
        txt_coupon = findViewById(R.id.txt_coupon);

        rel_couponCode = findViewById(R.id.rel_couponCode);

        btn_payPal = findViewById(R.id.btn_payPal);

        lin_cod = findViewById(R.id.lin_cod);
        lin_overlay = findViewById(R.id.lin_overlay);

        btn_buyNow.setOnClickListener(this);
        rel_signIn.setOnClickListener(this);
        btn_more.setOnClickListener(this);
        rel_couponCode.setOnClickListener(this);
        btn_payPal.setOnClickListener(this);

        lin_cod.setOnClickListener(this);
        getIntentData();
        setAutoCompleteText();
        setCartTotal(dbManager.fetch());
        payPalServices(true);

    }

    public void setAutoCompleteText() {

        List<String> strings = new ArrayList<>();
        strings.add(getString(R.string.no_));
        strings.add(getString(R.string.mr_));
        strings.add(getString(R.string.ms_));
        strings.add(getString(R.string.drivers_));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BillingActivity.this, android.R.layout.simple_expandable_list_item_1, strings);
        AutoTxt_salutation.setThreshold(1);
        AutoTxt_salutation.setAdapter(adapter);
    }

    public void getIntentData() {

        if (getIntent() != null) {
            if (getIntent().getStringExtra("couponData") != null) {
                isCoupon = true;
                couponData = new Gson().fromJson(getIntent().getStringExtra("couponData"), CouponData.class);
            }
        }
    }

    public void validation() {

        if (!AppUtils.validationText(ed_name)) {
            return;
        }

        if (!AppUtils.validationText(ed_surname)) {
            return;
        }

        if (!AppUtils.validationText(ed_streetName)) {
            return;
        }

        if (!AppUtils.validationText(ed_zipCode)) {
            return;
        }

        if (!AppUtils.validationText(ed_cityArea)) {
            return;
        }

        if (!AppUtils.validationText(ed_email)) {
            return;
        }

        if (!AppUtils.validationEmail(ed_email)) {
            return;
        }

        /*if (lin_overlay.getVisibility() == View.GONE) {
            show_toast_short(getApplicationContext(), getString(R.string.choose_payment_method));
            return;
        }*/

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {
//            getOrderApi();
//            placeOrderApi();
            getPayment();
        } else {
            show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
        }

    }

    public void setCartTotal(@NotNull List<CartItem> cartItem) {

        double total = 0;

        for (int i = 0; i < cartItem.size(); i++) {
            double tot = 0;
            if (cartItem.get(i).getProduct().getPrice() != null && !cartItem.get(i).getProduct().getPrice().equals("null")) {
                try {
                    tot = Float.valueOf(cartItem.get(i).getProduct().getPrice()) * cartItem.get(i).getQty();
                } catch (Exception e) {
                    tot = 0;
                }
                total = total + tot;
            }
            Log.e("Tag quantity", "" + cartItem.get(i).getQty());
            Log.e("Tag quantity price", "" + cartItem.get(i).getProduct().getPrice());
        }

        if (isCoupon && couponData != null) {
            rel_coupon.setVisibility(View.VISIBLE);
            txt_coupon.setText("-" + roundTwoDecimals(Integer.parseInt(couponData.getCoupon_amount())) + " €");
        } else {
            rel_coupon.setVisibility(View.GONE);
        }

        txt_subTotal.setText(roundTwoDecimals(total) + " €");
        double res = ((total * AppUtils.TAX) / 100);
        txt_tax.setText(roundTwoDecimals(res) + " €");
        txt_total.setText("" + roundTwoDecimals((total + res)) + " €");
        txt_finalTax.setText("" + roundTwoDecimals(res) + " €");

    }

    public String createOrderJson() {

        JSONObject order = new JSONObject();
        try {
            JSONObject order_value = new JSONObject();

            JSONObject paymentDetail = new JSONObject();
            paymentDetail.put("method_id", "paypal");
            paymentDetail.put("method_title", "Paypal");
            paymentDetail.put("paid", "false");

            JSONObject billing_address = new JSONObject();
            billing_address.put("first_name", ed_name.getText().toString());
            billing_address.put("last_name", ed_surname.getText().toString());
            billing_address.put("address_1", ed_streetName.getText().toString());
            billing_address.put("address_2", ed_room.getText().toString());
            billing_address.put("city", ed_cityArea.getText().toString());
            billing_address.put("state", "");
            billing_address.put("postcode", ed_zipCode.getText().toString());
//            billing_address.put("postcode", "22143");
            billing_address.put("country", "Germany");
            billing_address.put("email", ed_email.getText().toString());
            billing_address.put("phone", ed_telephone.getText().toString());
            billing_address.put("company", ed_companyName.getText().toString());

            JSONObject shiping_address = new JSONObject();
            shiping_address.put("first_name", ed_name.getText().toString());
            shiping_address.put("last_name", ed_surname.getText().toString());
            shiping_address.put("address_1", ed_streetName.getText().toString());
            shiping_address.put("address_2", ed_room.getText().toString());
            shiping_address.put("city", ed_cityArea.getText().toString());
            shiping_address.put("postcode",ed_zipCode.getText().toString());
//            shipping_address.put("postcode", "22143");

            JSONArray lin_itemArray = new JSONArray();

            for (int i = 0; i < dbManager.fetch().size(); i++) {

                JSONObject line_item = new JSONObject();
                line_item.put("product_id", dbManager.fetch().get(i).getProduct().getId());
                line_item.put("quantity", dbManager.fetch().get(i).getQty());
                line_item.put("subtotal", roundTwoDecimals(Double.parseDouble(dbManager.fetch().get(i).getProduct().getPrice()) * dbManager.fetch().get(i).getQty()));
                line_item.put("total", roundTwoDecimals(Double.parseDouble(dbManager.fetch().get(i).getProduct().getPrice()) * dbManager.fetch().get(i).getQty()));
                lin_itemArray.put(line_item);
            }

            /*JSONArray taxLine = new JSONArray();
            JSONObject txtObject = new JSONObject();
            txtObject.put("rate_id","6");
            txtObject.put("code","DE-MWST. DE-1");
            txtObject.put("title","MwSt. DE");
            txtObject.put("total", txt_tax.getText().toString().replace(" €",""));
            txtObject.put("compound","false");
            taxLine.put(txtObject);*/

            JSONArray couponArray = new JSONArray();
            if (isCoupon && couponData != null) {
                JSONObject couponObject = new JSONObject();
                couponObject.put("id", couponData.getID());
                couponObject.put("code", couponData.getCoupon_code());
                couponObject.put("amount", couponData.getCoupon_amount());
                couponArray.put(couponObject);
            }

            JSONArray shipping_linesArray = new JSONArray();
            JSONObject shipping_lines = new JSONObject();
            shipping_lines.put("method_id", "free_shipping");
            shipping_lines.put("method_title", "Free shipping");
            shipping_lines.put("total", "0");
            shipping_linesArray.put(shipping_lines);

            order_value.put("payment_details", paymentDetail);
            order_value.put("billing_address", billing_address);
            order_value.put("shipping_address", shiping_address);
            order_value.put("customer_id", sessionManager.getUserId());
            order_value.put("note", ed_comment.getText().toString());
            order_value.put("line_items", lin_itemArray);
//            order_value.put("tax_lines", taxLine);
            if (isCoupon && couponData != null) {
                order_value.put("coupon_lines", couponArray);
            }
            order_value.put("shipping_lines", shipping_linesArray);
            order.put("order", order_value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return order.toString();

    }

    public void placeOrderApi() {

        if (!this.isFinishing()) {
            loaderView.show();
        }
        if (createOrderJson() == null) {
            show_toast_short(getApplicationContext(), getString(R.string.something_went_wrong));
            return;
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

//        service.getCustomer("get_product", "1");

        LogUtils.error(BillingActivity.class, "Tag error   " + createOrderJson());

        service.create_order(AppUtils.CREATE_ORDER, createOrderJson()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null) {

                        try {
                            JSONObject object = new JSONObject((response.body().string()));
                            JSONObject order = object.getJSONObject("order");
                            String order_number = order.getString("order_number");
                            lin_view.setVisibility(View.GONE);
                            lin_ordered.setVisibility(View.VISIBLE);
                            txt_order_msg.setText(getString(R.string.your_order_is_successfully_places_your_order_number_is) + order_number);
                            Toast.makeText(getApplicationContext(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                            dbManager.removeTable();
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }

                    } else {

                        Toast.makeText(getApplicationContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                    }
                } else {
                    show_toast_short(getApplicationContext(), getString(R.string.something_went_wrong));
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_payPal) {
            if (sessionManager.isLoggedIn()) {
                validation();
            }  else {
                startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), 202);
            }
        } else if (v == btn_more) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (v == rel_signIn) {

            instance = this;
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(intent, 202);
        } else if (v == rel_couponCode) {
            onBackPressed();
        } else if (v == lin_cod) {
            lin_overlay.setVisibility(View.VISIBLE);
        }
    }

    public void payPalServices(boolean start) {

        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        if (start) {
            startService(intent);
        } else {
            if (isMyServiceRunning(PayPalService.class)) {
                stopService(intent);
            }
        }
    }

    private void getPayment() {

        /*PayPalItem[] items =
                {
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };*/
        /*PayPalItem[] items = new PayPalItem[dbManager.fetch().size()];
        for (int i=0; i<dbManager.fetch().size(); i++){
            ProductData productData = dbManager.fetch().get(i).getProduct();
            CartItem item = dbManager.fetch().get(i);
            items[i] = new PayPalItem(item.getProduct().getName(),item.getQty(),new BigDecimal(productData.getPrice()),"EUR", "Paypall");
        }*/
        /*Log.e("Tag Price", items[0].toString());
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("0.00");
        BigDecimal tax = new BigDecimal(txt_tax.getText().toString().replace(" €",""));
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(tax);*/
        //        Log.e("Tag Price Amount", amount.toString());

        //Getting the amount from editText

        paymentAmount = txt_total.getText().toString().replace(" €", "");

        //Creating a payPalPayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(paymentAmount), PayPalConfig.DEFAULT_CURRENCY, "Pay to Paypal", PayPalPayment.PAYMENT_INTENT_SALE);
        /*ShippingAddress shippingAddress =
                new ShippingAddress().recipientName(manager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name())
                        .line1(manager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1())
                        .city(manager.getLoginUserDetail().getCustomer().getBilling_address().getCity())
                        .state(manager.getLoginUserDetail().getCustomer().getBilling_address().getState())
                        .postalCode(manager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode())
                        .countryCode("US");*/

       /* ShippingAddress shippingAddress =
                new ShippingAddress().recipientName("Mom Parker").line1("52 North Main St.")
                        .city("Austin").state("TX").postalCode("78729").countryCode("US");*/

//        payment.providedShippingAddress(shippingAddress);
//        payment.items(items).paymentDetails(paymentDetails);

        //Creating PayPal Payment activity intent
        Intent intent = new Intent(this, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAY_PAL_REQUEST_CODE);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 202) {
            fillDetail();
            payPalServices(true);
        } else if (requestCode == PAY_PAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.e("paymentExample", paymentDetails);

                        /*startActivity(new Intent(this, ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", paymentAmount));*/
                        //Starting a new activity for the payment details and also putting the payment details with intent
                        //show_toast_short(getApplicationContext(), paymentDetails);
                        placeOrderApi();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    protected void onDestroy() {
        payPalServices(false);
        super.onDestroy();
    }

    public void fillDetail() {

        if (sessionManager.getLoginUserDetail() != null) {

            if (sessionManager.getLoginUserDetail().getCustomer() != null) {

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name().equals("")) {
                    ed_name.setText(sessionManager.getLoginUserDetail().getCustomer().getFirst_name());
                } else {
                    ed_name.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name());
                }

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name().equals("")) {
                    ed_surname.setText(sessionManager.getLoginUserDetail().getCustomer().getLast_name());
                } else {
                    ed_surname.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name());
                }

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getEmail().equals("")) {
                    ed_email.setText(sessionManager.getLoginUserDetail().getCustomer().getEmail());
                } else {
                    ed_email.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getEmail());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1().equals("")) {
                    ed_streetName.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2().equals("")) {
                    ed_room.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCompany().equals("")) {
                    ed_companyName.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCompany());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCity().equals("")) {
                    ed_cityArea.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCity());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPhone().equals("")) {
                    ed_telephone.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPhone());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode().equals("")) {
                    ed_zipCode.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode());
                }
            }
        }
    }

}