package com.azfit.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azfit.Adapter.AdapterProductList;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.Custome.WrappingGridLayoutManager;
import com.azfit.Model.Course.Course;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.Sevices.ServiceData;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.azfit.utils.RecyclerTouchListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.siyamed.shapeimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_header_title;

    // no data
    View no_data;
    TextView txt_msg;
    Button btn_retry;

    CardView lin_view;
    RecyclerView rcv_services;
    TextView txt_serviceTitle;

    RoundedImageView img_service_image;
    TextView txt_description;
    LinearLayout lin_service_detail;

    LoaderView loaderView;
    AdapterProductList adapterProductList;

    NestedScrollView nestedScroll;

    List<CourseData> courses;
    List<CourseData> courseData;
    String category_id = "", category_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        loaderView = new LoaderView(ProductListActivity.this);

        initView();
    }

    private void initView() {

        txt_header_title = findViewById(R.id.txt_header_title);

        no_data = findViewById(R.id.no_data);
        txt_msg = findViewById(R.id.txt_message);
        btn_retry = findViewById(R.id.btn_retry);

        lin_view = findViewById(R.id.lin_view);
        rcv_services = findViewById(R.id.rcv_services);
        txt_serviceTitle = findViewById(R.id.txt_serviceTitle);

        img_service_image = findViewById(R.id.img_service_image);
        txt_description = findViewById(R.id.txt_description);
        lin_service_detail = findViewById(R.id.lin_service_detail);
        lin_service_detail.setVisibility(View.GONE);

        nestedScroll = findViewById(R.id.nestedScroll);

        btn_retry.setOnClickListener(this);

        getIntentData();

    }

    public void getIntentData() {

        if (getIntent() != null){

            category_id = getIntent().getStringExtra("cat_id");
            category_name = getIntent().getStringExtra("cat_name");

            if (category_id != null) {
                courseCategoryApi();
            }else {
                no_data.setVisibility(View.VISIBLE);
                lin_view.setVisibility(View.GONE);
            }

            if (category_name != null){
                txt_header_title.setText(category_name);
            }

        }else {
            no_data.setVisibility(View.VISIBLE);
            lin_view.setVisibility(View.GONE);
        }
    }

    public void initializeList() {

        if (courseData != null && courseData.size()>0){

            lin_view.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            rcv_services.setLayoutManager(new WrappingGridLayoutManager(getApplicationContext(), 2));
            rcv_services.setHasFixedSize(false);
            rcv_services.setNestedScrollingEnabled(false);
            adapterProductList = new AdapterProductList(ProductListActivity.this, courseData);
            rcv_services.setAdapter(adapterProductList);
            rcv_services.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcv_services, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    Button btn_more = view.findViewById(R.id.btn_read_more);
                    btn_more.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            lin_service_detail.setVisibility(View.VISIBLE);
                            txt_serviceTitle.setText(courseData.get(position).getPost_title());
                            txt_description.setText(courseData.get(position).getPost_excerpt());

                            if (courseData.get(position).getCourse_image() != null && courseData.get(position).getCourse_image().length()>0){

                                Glide.with(getApplicationContext())
                                        .asBitmap()
                                        .error(getResources().getDrawable(R.drawable.ic_no_photo))
                                        .placeholder(getResources().getDrawable(R.drawable.loader_progress))
                                        .load(AppUtils.CATEGORY_IMAGE_BASE_URL+courseData.get(position).getCourse_image())
                                        .into(new BitmapImageViewTarget(img_service_image){
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            img_service_image.setImageBitmap(resource);
                                        }
                            });
                            }
                            nestedScroll.post(new Runnable() {
                                @Override
                                public void run() {
                                    nestedScroll.fullScroll(View.FOCUS_DOWN);
                                }
                            });
                        }
                    });

                    CardView cardView = view.findViewById(R.id.cardMain);
                    cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                            intent.putExtra("course_id", courseData.get(position).getID());
                            intent.putExtra("cat_name", category_name);
                            startActivity(intent);
                        }
                    });
                }

                @Override
                public void onLongClick(View view, int position) {

                }

            }));
        }else {
            lin_view.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            btn_retry.setVisibility(View.GONE);
            txt_msg.setText(R.string.no_course_data);
        }

    }

    private List<ServiceData> getServiceList() {

        List<ServiceData> data = new ArrayList<>();

        data.add(new ServiceData("1", "1", "Taping", getResources().getDrawable(R.drawable.experts), "Die Tapeanlagen konzentrieren sich auf zwei Schwerpunkte: Den einen Schwerpunkt bildet die."));
        data.add(new ServiceData("1", "1", "AKUPRESSUR", getResources().getDrawable(R.drawable.banner), "Die Tapeanlagen konzentrieren sich auf zwei Schwerpunkte: Den einen Schwerpunkt bildet die Behandlung von alltäglichen "));
        data.add(new ServiceData("1", "1", "RÜCKEN FIT", getResources().getDrawable(R.drawable.baby), "Der andere Schwerpunkt liegt auf der Behandlung und Prävention von Sportverletzungen."));
        data.add(new ServiceData("1", "1", "CUPPING", getResources().getDrawable(R.drawable.experts), "von alltäglichen Beschwerden und Schmerzsyndromen des Bewegungsapparats. Der andere Schwerpunkt liegt auf der Behandlung und"));
        data.add(new ServiceData("1", "1", "FLOSSING", getResources().getDrawable(R.drawable.baby), "konzentrieren sich auf zwei Schwerpunkte: Den einen Schwerpunkt bildet die Behandlung von alltäglichen Beschwerden und Schmerzsyndromen des Bewegungsapparats. Der andere Schwerpunkt liegt auf der Behandlung und Prävention von Sportverletzungen."));
        data.add(new ServiceData("1", "1", "REFLEXZONEN", getResources().getDrawable(R.drawable.banner), "Schwerpunkte: Den einen Schwerpunkt bildet die Behandlung von alltäglichen Beschwerden "));
        data.add(new ServiceData("1", "1", "FASZIEN", getResources().getDrawable(R.drawable.baby), "des Bewegungsapparats. Der andere Schwerpunkt liegt auf der Behandlung und Prävention von"));
        data.add(new ServiceData("1", "1", "HAPPY HANDS", getResources().getDrawable(R.drawable.experts), "Der andere Schwerpunkt liegt auf der Behandlung und Prävention von Sportverletzungen."));

        return data;

    }

    public void courseCategoryApi () {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCourse(AppUtils.COURSE, category_id).enqueue(new Callback<Course>() {
            @Override
            public void onResponse(@NotNull Call<Course> call, @NotNull Response<Course> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null){

                    try {
                        JSONObject object = new JSONObject(response.body().toString());
                        LogUtils.error(LoginActivity.class, "Tag Response "+ object.toString(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (response.body().getResult() != null && response.body().getResult().size()>0){

                        courses = response.body().getResult();
                        courseCategoryDetailApi();

                    } else {
                        lin_view.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplication(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                    }

                }else {
                    no_data.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    lin_view.setVisibility(View.GONE);
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Course> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                no_data.setVisibility(View.VISIBLE);
                btn_retry.setVisibility(View.VISIBLE);
                lin_view.setVisibility(View.GONE);
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }
        });
    }

    public void courseCategoryDetailApi () {

        if (!this.isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCourse(AppUtils.COURSE, "").enqueue(new Callback<Course>() {
            @Override
            public void onResponse(@NotNull Call<Course> call, @NotNull Response<Course> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    try {
                        JSONObject object = new JSONObject(response.body().toString());
                        LogUtils.error(LoginActivity.class, "Tag Response "+ object.toString(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (response.body().getResult() != null && response.body().getResult().size()>0){

                        courseData = new ArrayList<>();
                        for (CourseData cou : courses){

                            for (CourseData course : response.body().getResult()){
                                if (cou.getID().equals(course.getID())){
                                    courseData.add(course);
                                }
                            }
                        }

                        initializeList();

                    } else {
                        lin_view.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplication(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                    }

                }else {
                    no_data.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    lin_view.setVisibility(View.GONE);
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Course> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                no_data.setVisibility(View.VISIBLE);
                btn_retry.setVisibility(View.VISIBLE);
                lin_view.setVisibility(View.GONE);
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == btn_retry){
            courseCategoryApi();
        }
    }
}