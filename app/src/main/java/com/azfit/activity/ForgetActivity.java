package com.azfit.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.Model.ResponseModel;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.azfit.utils.AppUtils.isNetworkAvailable;
import static com.azfit.utils.AppUtils.show_toast_short;
import static com.azfit.utils.AppUtils.validationEmail;

public class ForgetActivity extends AppCompatActivity implements View.OnClickListener {

    EditText ed_userName_Email;
    TextView txt_signIn;
    Button btn_reset_password;

    LoaderView loaderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);

        loaderView = new LoaderView(ForgetActivity.this);
        AppUtils.validation = this;
        initView();
    }

    public void initView() {

        txt_signIn = findViewById(R.id.txt_signIn);
        ed_userName_Email = findViewById(R.id.ed_userName_Email);
        btn_reset_password = findViewById(R.id.btn_reset_password);
        btn_reset_password.setOnClickListener(this);
        txt_signIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == btn_reset_password) {

            if (!validationEmail(ed_userName_Email)) {
                return;
            }

            if (isNetworkAvailable(getApplicationContext())) {
                ForgetPasswordApi();
            } else {
                show_toast_short(getApplicationContext(), getString(R.string.internet_not_connect));
            }

        } else if (v == txt_signIn) {
            finish();
        }
    }

    public void ForgetPasswordApi() {

        if (!this.isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCustomer("get_product", "1");

        service.forgetPassword(AppUtils.FORGET_PASSWORD, ed_userName_Email.getText().toString()).enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<ResponseModel> call, @NotNull Response<ResponseModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body().getStatus().equals("true")){
                        finish();
                    }
                    show_toast_short(getApplicationContext(), response.body().getMessage());

                } else {
                    show_toast_short(getApplicationContext(), response.body().getMessage());
                }

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, " Tag Error ");
                }

                LogUtils.error(ForgetActivity.class, t.getMessage());

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }

            }
        });
    }
}