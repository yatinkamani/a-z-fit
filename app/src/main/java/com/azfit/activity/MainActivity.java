package com.azfit.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.azfit.R;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.lang.reflect.Field;

import static androidx.navigation.Navigation.findNavController;
import static androidx.navigation.ui.NavigationUI.onNavDestinationSelected;

public class MainActivity extends AppCompatActivity {

    boolean isNavigate = false;
    NavController navController;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,
                R.id.nav_favourite,
                R.id.nav_book,
                R.id.nav_subscription,
                R.id.nav_notification,
                R.id.nav_profile)
                .build();

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

//        NavigationUI.setupWithNavController(this, navController, appBarConfiguration);

        NavigationUI.setupWithNavController(navView, navController);

//        NavigationUI.setupWithNavController(navView, ((NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment)).getNavController());

        /*navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

//                navController = Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment);
                */
        /*switch (item.getItemId()) {
                    case R.id.navigation_home:
                        navController.navigate(R.id.nav_home);
                        NavigationUI.onNavDestinationSelected(item,navController);
                        break;
                    case R.id.navigation_book:
                        navController.navigate(R.id.nav_book);
                        break;
                    case R.id.navigation_favorite:
                        navController.navigate(R.id.nav_favourite);
                        break;
                    case R.id.navigation_notifications:
                        navController.navigate(R.id.nav_notification);
                        break;
                    case R.id.navigation_profile:
                        navController.navigate(R.id.nav_profile);
                        break;
                }*/
        /*

                return NavigationUI.onNavDestinationSelected(item, navController);
            }
        });*/

        navView.setOnNavigationItemReselectedListener(null);

        /*navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_book:
                    case R.id.nav_favourite:
                    case R.id.nav_notification:
                    case R.id.nav_profile:
                        Log.e("Tag enable", ""+getOnBackPressedDispatcher().hasEnabledCallbacks());
                        if (getOnBackPressedDispatcher().hasEnabledCallbacks()) {
                            getOnBackPressedDispatcher().onBackPressed();
                        } else {
                            navController.navigateUp();
                        }
                        break;
                    default:
                        navController.navigateUp();
                }
                return true;
            }
        });*/

        navController.getBackStack();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        /*Log.e("Tag back", ""+navController.getBackStack().size());
        Log.e("Tag back 1", ""+navController.getBackStack().iterator());
        Log.e("Tag back 2", ""+navController.getBackStack().iterator().next());*/
        Log.e("Tag back 3", ""+navController.getBackStack());

        super.onBackPressed();
    }
}