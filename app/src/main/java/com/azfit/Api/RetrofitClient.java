package com.azfit.Api;


import android.text.TextUtils;
import android.util.Log;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static final String API_BASE_URL = "https://a-z-fit.com/wooAPI/";
    public static final String API_BASE_URL_WOO = "https://a-z-fit.com/wp-json/tutor/v1/";
    public static final String API_BASE_URL_WOO_V2 = "https://a-z-fit.com/wp-json/wp/v2/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder wooBuilder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL_WOO)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder wooV2Builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL_WOO_V2)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static Retrofit wooRetrofit = wooBuilder.build();

    private static Retrofit wooV2Retrofit = wooV2Builder.build();

//    public static <S> S createService(Class<S> serviceClass) {
//        return createService(serviceClass);
//    }

    public static <S> S createService(
            Class<S> serviceClass) {

        String username = "admin";

//        if (!TextUtils.isEmpty("admin")
//                && !TextUtils.isEmpty("123")) {
            String authToken = Credentials.basic("admin", "123");
            Log.e("authToken",authToken);
            return retrofit.create(serviceClass);
//            return createService(serviceClass, authToken);
//        }

    }

    public static <S> S createServiceWoo(
            Class<S> serviceClass) {

        String username = "admin";

//        if (!TextUtils.isEmpty("admin")
//                && !TextUtils.isEmpty("123")) {
        String authToken = Credentials.basic("admin", "123");
        Log.e("authToken",authToken);
        return wooRetrofit.create(serviceClass);
//            return createService(serviceClass, authToken);
//        }

    }

    public static <S> S createServiceWooV2(
            Class<S> serviceClass) {

        String username = "admin";

//        if (!TextUtils.isEmpty("admin")
//                && !TextUtils.isEmpty("123")) {
        String authToken = Credentials.basic("admin", "123");
        Log.e("authToken",authToken);
        return wooV2Retrofit.create(serviceClass);
//            return createService(serviceClass, authToken);
//        }

    }

    public static <S> S createService(
            Class<S> serviceClass, final String authToken) {
        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(authToken);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit.create(serviceClass);
    }
}
