package com.azfit.Api;

import com.azfit.Model.AnnounceMentModel.AnnouncementModel;
import com.azfit.Model.Category.CategoryModel;
import com.azfit.Model.CouponCode.CouponModel;
import com.azfit.Model.Course.Course;
import com.azfit.Model.Cutomers.CustomersDataModel;
import com.azfit.Model.Lesson.Lesson;
import com.azfit.Model.MediaVideoModel.MediaVideoModel;
import com.azfit.Model.Orders.OrdersModel;
import com.azfit.Model.Product.ProductModel;
import com.azfit.Model.Product.SearchProductModel;
import com.azfit.Model.ResponseModel;
import com.azfit.Model.ShippingMethod.ShippingMethod;
import com.azfit.Model.Topic.TopicModel;
import com.azfit.Model.TopicLesson.TopicLessonModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {


    @GET("customer.php")
    Call<CustomersDataModel> customerLogin(@Query("service")String service,
                                           @Query("username")String email,
                                           @Query("password")String password);

    @GET("customer.php")
    Call<CustomersDataModel> customerRegister(@Query("service")String service,
                                              @Query("email")String email,
                                              @Query("password")String password,
                                              @Query("first_name")String first_name,
                                              @Query("last_name")String last_name,
                                              @Query("username")String username);

    @GET("customer.php")
    Call<CustomersDataModel> getCustomer(@Query("service")String service,
                                         @Query("id")String id);

    @GET("customer.php")
    Call<ResponseModel> changePassword(@Query("service") String service,
                                       @Query("old_password")String current_password,
                                       @Query("new_password")String new_password);

    @GET("customer.php")
    Call<CouponModel> checkCoupon(@Query("service") String service,
                                      @Query("coupon_code")String coupone_code);

    @GET("reset_password.php")
    Call<ResponseModel> forgetPassword(@Query("service")String service,
                                       @Query("email")String email);

    @GET("get_all_products.php")
    Call<ProductModel> getProduct();

    @GET("product_search.php")
    Call<SearchProductModel> getFilteredProduct(@Query("product_id")String product_id);

    @GET("customer.php")
    Call<ResponseBody> create_order(@Query("service")String service, @Query("data")String data);

    @GET("customer.php")
    Call<CustomersDataModel> update_billing(@Query("service")String service, @Query("customer_id")String customer_id, @Query("data")String data);

    @GET("customer.php")
    Call<OrdersModel> getOrders(@Query("service")String service, @Query("customer_id")String customer_id);

    @GET("customer.php")
    Call<Course> getCourse(@Query("service")String service, @Query("category_id")String category_id);

    @GET("customer.php")
    Call<Lesson> getLesson(@Query("service")String service);

    @GET("get_shipping_method.php")
    Call<ShippingMethod> getShippingMethod();

    @GET("customer.php")
    Call<CategoryModel> getCategory(@Query("service")String service);

    @GET("course-topic/{course_id}")
    Call<TopicModel>  getTopic(@Path("course_id") String course_id);

    @GET("lesson/{topic_id}")
    Call<TopicLessonModel>  getTopicLesson(@Path("topic_id") String topic_id);

    @GET("course-annoucement/{course_id}")
    Call<AnnouncementModel> getAnnouncementLesson(@Path("course_id") String course_id);

    @GET("media/{media_id}")
    Call<MediaVideoModel>  getVideoUrl(@Path("media_id") String topic_id);

}
