package com.azfit.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Locale;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Admin on 29/12/2017.
 */

public class MyApplicationClass extends Application {

    private static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocalUtils.updateConfig(this, newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
       /* Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);*/

        TypefaceUtils.overrideFont(getApplicationContext(), "SERIF", "font/poppins_medium.ttf");

        context = getApplicationContext();
        LocalUtils.setLocale(new Locale(LocalUtils.LAN_GERMAN));
        LocalUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());

        /*FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, 1);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "SafeRide");

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        mFirebaseAnalytics.setSessionTimeoutDuration(500);
        mFirebaseAnalytics.setUserId(String.valueOf(1));
        mFirebaseAnalytics.setUserProperty("SafeRide", "SafeRide");*/
    }

    public static Context getAppContext() {
        return context;
    }

    @NotNull
    public static RequestBody get_RequestBody(String str) {
        RequestBody r_strUserName = MyApplicationClass.createPartFromString(str);
        return r_strUserName;
    }

    @NotNull
    public static RequestBody createPartFromString(String partString) {
        return RequestBody.create(MultipartBody.FORM, partString);
    }

    public static boolean hasData(Double text) {
        if (text == null || text.byteValue() == 0)
            return false;
        return true;
    }

    public static MultipartBody.Part get_RequestBody(File file, String parameter_image) {
        return file != null ? createPartFromFile(file, parameter_image) : null;
    }

    @NotNull
    public static MultipartBody.Part createPartFromFile(File file, String p_image) {
        RequestBody requestBody = RequestBody.create(MultipartBody.FORM, file);
        return MultipartBody.Part.createFormData(p_image, file.getName(), requestBody);
    }

}
