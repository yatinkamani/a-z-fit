package com.azfit.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.azfit.R;
import com.victor.loading.rotate.RotateLoading;


/**
 * Created by techintegrity on 08/10/16.
 */

/**
 * Created by techintegrity on 08/10/16.
 */

public class LoaderView extends Dialog {

    private RelativeLayout rl;
    private Context context;
    private RotateLoading rotateloading;

    public LoaderView(Context context) {

        super(context, R.style.Theme_AppCompat_Translucent);
        this.context = context;
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.loader_dialog);
        rl = (RelativeLayout)findViewById(R.id.rlLoaderMainView);
        rotateloading = (RotateLoading)findViewById(R.id.rotateloading);
        rotateloading.start();
        setCancelable(true);

    }


    public RotateLoading loaderObject(){
        return rotateloading;
    }

    public void loaderDismiss(){
        cancel();
        dismiss();
    }

}
