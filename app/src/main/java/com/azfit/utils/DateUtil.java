package com.azfit.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String getCurrentDate(){

        SimpleDateFormat format = new SimpleDateFormat("dd");
        return format.format(new Date());
    }
}
