package com.azfit.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.azfit.Model.CartItem;
import com.azfit.Model.Category.CategoryData;
import com.azfit.Model.CouponCode.CouponData;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.Product.CategoriesData;
import com.azfit.Model.Product.ProductData;
import com.azfit.Model.TopicLesson.TopicLessonData;
import com.azfit.Model.TopicLesson.TopicLessonModel;
import com.azfit.R;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AppUtils {

    public static final String API_BASE_URL = "https://a-z-fit.com/wooAPI/";
    public static final String CATEGORY_IMAGE_BASE_URL = "https://a-z-fit.com/wp-content/uploads/";

    public static final String LOGIN = "login";
    public static final String REGISTER = "registration";
    public static final String GET_PROFILE = "get_profile";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String FORGET_PASSWORD = "reset";
    public static final String CREATE_ORDER = "create_order";
    public static final String UPDATE_CUSTOMER = "update_customer";
    public static final String MY_ORDERS = "my_orders";
    public static final String LESSON = "get_lesson";
    public static final String COURSE = "get_course";
    public static final String CATEGORY = "get_category";
    public static final String CHECK_COUPON = "check_coupon";

    public static final int TAX = 19;

    public static Activity instance = null;
    public static Activity validation = null;

    public static String lastTopic = "";
    public static List<TopicLessonData> lastTopicLessonModel = new ArrayList<>();

    public static List<CourseData> courseData = null;
    public static List<ProductData> productData = null;
    public static List<CategoryData> categoryData = null;

    public static void hideStatusBar(Activity context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            context.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    );
        }
    }

    public static void refreshCartDB(TextView txtCounter, List<CartItem> cartItem) {
        if (cartItem != null && cartItem.size() > 0) {
            if (txtCounter != null) {
                txtCounter.setText(cartItem.size() + "");
            }
        } else {
            if (txtCounter != null) {
                txtCounter.setText("0");
            }
        }
    }

    public static boolean validationText(@NotNull EditText text) {

        if (text.getText().toString().equals("")) {
            text.setError(validation.getString(R.string.please_fill_field));
            text.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return Double.valueOf(twoDForm.format(d));
    }

    public static boolean validationPassword(@NotNull EditText text) {

        if (text.getText().toString().equals("")) {
            text.setError(validation.getString(R.string.please_fill_field));
            text.requestFocus();
            return false;
        } else if (text.getText().toString().length() < 6) {
            text.setError(validation.getString(R.string.please_enter_6caharactore));
            text.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static boolean validationPasswordMatch(@NotNull EditText text, @NotNull EditText text2) {

        if (!text.getText().toString().equals(text2.getText().toString())) {
            text2.setError(validation.getString(R.string.password_does_not_match));
            text2.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static boolean validationEmail(@NotNull EditText text) {

        if (TextUtils.isEmpty(text.getText().toString())) {
            text.setError(validation.getString(R.string.please_fill_field));
            text.requestFocus();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(text.getText().toString()).matches()) {
            text.setError(validation.getString(R.string.enter_valid_email));
            text.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    public static void show_toast_short(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    public static void showLongSnackBar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static boolean isNetworkAvailable(@NotNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED);
    }

    public static String getStringImage(@NotNull Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public static void rateAction(@NotNull Activity activity) {
        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
        }
    }

    public static String getFormattedDate(String simple_date) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM,yyyy");
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getFormattedDateWithTime(String simple_date) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd k:mm:ss");
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM dd, yyyy");
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getFormattedTime(String simple_date) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("k:mm:ss");
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm a");
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getFormattedDateTZ(String simple_date) {

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.ENGLISH);
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getCheckDateExpire(String simple_date) {

        String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.ENGLISH);
            Date date = fmt.parse(simple_date);
            Date todayDate = new Date();

            SimpleDateFormat fmtOut = new SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            Log.e("Tag date", fmtOut.format(calendar.getTime()));

            return ""+date.compareTo(fmtOut.parse(fmtOut.format(calendar.getTime())));

        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getFormattedDateWithTimeDisplay(String simple_date) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd k:mm:ss");
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM,yyyy hh:mm a");
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    @NotNull
    public static List<Date> getDates(String dateString1, String dateString2) {

        ArrayList<Date> dates = new ArrayList<Date>();
        @SuppressLint("SimpleDateFormat")
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    @NotNull
    public static List<Date> getDatesFromDate(Date date1, Date date2) {
        ArrayList<Date> dates = new ArrayList<Date>();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public static long DayBetween(String date1, String date2, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        Date Date1 = null, Date2 = null;
        try {
            Date1 = sdf.parse(date1);
            Date2 = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (Date2.getTime() - Date1.getTime()) / (24 * 60 * 60 * 1000);
    }

    public static long milliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    public static String getAcademicYear(String start_date, String end_date) {
        String academic_year = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat fmtOut = new SimpleDateFormat("MMM yyyy");
        try {
            Date StartDate_in = format.parse(start_date);
            Date EndDate_in = format.parse(end_date);
            academic_year = fmtOut.format(StartDate_in) + " To " + fmtOut.format(EndDate_in);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return academic_year;
    }

    public static String getFormattedDate1(String simple_date) {
        String stringDate = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(simple_date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM dd, yyyy");
            stringDate = fmtOut.format(date);
            return stringDate;

        } catch (Exception e) {
            e.printStackTrace();
            return stringDate;
        }
    }

    @NotNull
    public static String getCurrentMonth() {
        DateFormat dateFormat = new SimpleDateFormat("MM");
        Date date = new Date();
        SimpleDateFormat fmtOut = new SimpleDateFormat("MM");
        return fmtOut.format(date);
    }

    public static String getHourAndMinutes(String simple_date) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date date = fmt.parse(simple_date);

            SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa", Locale.US);
            return fmtOut.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return simple_date;
        }
    }

    public static String getFormattedDate11(String simple_date) {
        String stringDate = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(simple_date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM,yyyy");
            stringDate = fmtOut.format(date);
            return stringDate;

        } catch (Exception e) {
            e.printStackTrace();
            /*SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(simple_date);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }*/
            return stringDate;
        }
    }

}
