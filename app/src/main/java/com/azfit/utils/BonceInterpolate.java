package com.azfit.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.azfit.R;

public class BonceInterpolate implements android.view.animation.Interpolator {
    private double mAmplitude = 1;
    private double mFrequency = 10;

    public BonceInterpolate(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }

    public static void clickView(Context context, View view){

        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        myAnim.setInterpolator(new BonceInterpolate(0.2,20));

        view.startAnimation(myAnim);

    }

    public static void clickViewButton(Context context, View view){

        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
        view.startAnimation(myAnim);

    }
}
