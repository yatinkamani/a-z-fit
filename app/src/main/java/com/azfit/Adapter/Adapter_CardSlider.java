package com.azfit.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.R;
import com.github.islamkhsh.CardSliderAdapter;
import com.github.siyamed.shapeimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

public class Adapter_CardSlider extends CardSliderAdapter<Adapter_CardSlider.CardViewHolder> {

    Context context;
    int image[];

    public Adapter_CardSlider(Context context, int[] image) {
        this.context = context;
        this.image = image;
    }

    @Override
    public void bindVH(@NotNull CardViewHolder cardViewHolder, int i) {
        cardViewHolder.imageView.setImageResource(image[i]);
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_slider_view, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return image.length;
    }

    static class CardViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView imageView;
        public CardViewHolder(View view){
            super(view);
            imageView = view.findViewById(R.id.img_slide);
        }
    }

}
