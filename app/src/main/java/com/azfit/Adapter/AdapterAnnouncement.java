package com.azfit.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.AnnounceMentModel.AnnouncementData;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterAnnouncement extends RecyclerView.Adapter<AdapterAnnouncement.ViewHolder> {

    Context context;
    List<AnnouncementData> announcementData = new ArrayList<>();
    SessionManager manager;

    private ItemClickListener listener;

    public AdapterAnnouncement(Context context, List<AnnouncementData> trendsDataModels) {
        this.context = context;
        this.announcementData = trendsDataModels;
        manager = new SessionManager(context);
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterAnnouncement.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_announcement, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterAnnouncement.ViewHolder holder, int position) {
        holder.bind(announcementData.get(position));
    }

    @Override
    public int getItemCount() {
        return announcementData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title, txt_content;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_title = itemView.findViewById(R.id.txt_title);
            txt_content = itemView.findViewById(R.id.txt_content);
        }

        @SuppressLint("SetTextI18n")
        void bind(@NotNull final AnnouncementData model) {

            txt_title.setText(model.getPost_title());
            txt_content.setText(model.getPost_content());

        }
    }
}
