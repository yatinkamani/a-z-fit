package com.azfit.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.CourseModel;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterCourse extends RecyclerView.Adapter<AdapterCourse.ViewHolder> {

    Context context;
    List<CourseModel> servicesModels = new ArrayList<>();
    public int checkedPosition = 0;
    SessionManager manager;

    private ItemClickListener listener;

    public AdapterCourse(Context context, List<CourseModel> servicesModels) {
        this.context = context;
        this.servicesModels = servicesModels;
        manager = new SessionManager(context);
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterCourse.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_courses_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCourse.ViewHolder holder, int position) {
        holder.bind(servicesModels.get(position));
    }

    @Override
    public int getItemCount() {
        return servicesModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardMain;
        ImageView img_courses;
        RatingBar rating;
        TextView txt_level, txt_courses_name, txt_count_user,
                txt_count_time, txt_name_character, txt_course_owner,
                txt_price, txt_cart_register;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            img_courses = itemView.findViewById(R.id.img_courses);
            rating = itemView.findViewById(R.id.rating);
            txt_level = itemView.findViewById(R.id.txt_level);
            txt_courses_name = itemView.findViewById(R.id.txt_courses_name);
            txt_count_user = itemView.findViewById(R.id.txt_count_user);
            txt_count_time = itemView.findViewById(R.id.txt_count_time);
            txt_name_character = itemView.findViewById(R.id.txt_name_character);
            txt_course_owner = itemView.findViewById(R.id.txt_course_owner);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_cart_register = itemView.findViewById(R.id.txt_cart_register);
        }

        @SuppressLint("SetTextI18n")
        void bind(@NotNull final CourseModel model){

            txt_courses_name.setText(model.getName());
            if (manager.isLoggedIn()){
                txt_cart_register.setText(context.getString(R.string.add_to_cart));
                txt_cart_register.setCompoundDrawablesWithIntrinsicBounds(context.getDrawable(R.drawable.ic_action_cart),null,null,null);
            } else {
                txt_cart_register.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
                txt_cart_register.setText(context.getString(R.string.register_now));
            }

            if (!model.getRate().equals("")  && model.getRate().matches("\\d")){
                rating.setRating(Float.parseFloat(model.getRate()));
            } else {
                rating.setRating(0);
            }

            txt_price.setText("€ " + model.getPrice() +" / " +context.getString(R.string.month));

            txt_name_character.setText(model.getCourse_owner().substring(0,1));

            txt_course_owner.setText(model.getCourse_owner());
            if (!model.getTime().equals("")){
                txt_count_time.setVisibility(View.VISIBLE);
                txt_count_time.setText(model.getTime());
            } else {
                txt_count_time.setVisibility(View.GONE);
            }

            if (!model.getUser_count().equals("")) {
                txt_count_user.setText(model.getUser_count());
            } else {
                txt_count_user.setText("0");
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "comming soon", Toast.LENGTH_LONG).show();
                    /*Intent intent = new Intent("cart");
                    intent.putExtra("item",1);
                    context.sendBroadcast(intent);*/
                }
            });
        }
    }
}
