package com.azfit.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.CartData.DBManagerData;
import com.azfit.Model.WishlistData;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

public class AdapterWishlist extends RecyclerView.Adapter<AdapterWishlist.ViewHolder> {

    Context context;
    List<WishlistData> productData = new ArrayList<>();
    DBManagerData managerData;

    public AdapterWishlist(Context context, List<WishlistData> productData) {
        this.context = context;
        this.productData = productData;
        managerData = new DBManagerData(context);
    }

    @NonNull
    @Override
    public AdapterWishlist.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_whishlist, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterWishlist.ViewHolder holder, int position) {

        WishlistData model = productData.get(position);

        holder.txt_name.setText(model.getName());
        holder.txt_categories.setText(model.getCategories().equals("") ? "-" : model.getCategories());
        holder.txt_level.setText(model.getLevel().equals("") ? "-" : model.getLevel().replaceAll("_", " "));
        holder.txt_rate.setText(model.getRate());

        if (model.getImage() != null && !model.getImage().equals("") ) {

            Glide.with(context)
                    .asBitmap()
                    .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                    .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL+model.getImage())
                    .into(new BitmapImageViewTarget(holder.img_image){
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    holder.img_image.setImageBitmap(resource);
                }
            });
        }

        holder.img_fav.setClickable(true);
        holder.img_share.setClickable(true);
        holder.itemView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardMain;
        ImageView img_image, img_fav, img_share;
        TextView txt_name, txt_rate, txt_categories, txt_level;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            img_image = itemView.findViewById(R.id.img_image);
            img_fav = itemView.findViewById(R.id.img_fav);
            img_share = itemView.findViewById(R.id.img_share);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_rate = itemView.findViewById(R.id.txt_rate);
            txt_categories = itemView.findViewById(R.id.txt_categories);
            txt_level = itemView.findViewById(R.id.txt_level);

        }
    }

}
