package com.azfit.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.CourseDatabase.CourseDBManager;
import com.azfit.Custome.WrappingLinearLayoutManager;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.MediaVideoModel.MediaVideoModel;
import com.azfit.Model.TopicLesson.Videos;
import com.azfit.activity.LoginActivity;
import com.azfit.Model.Topic.TopicData;
import com.azfit.Model.TopicLesson.TopicLessonData;
import com.azfit.Model.TopicLesson.TopicLessonModel;
import com.azfit.R;
import com.azfit.activity.VideoActivity;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterTopic extends RecyclerView.Adapter<AdapterTopic.ViewHolder> {

    Context context;
    List<TopicData> topicModels = new ArrayList<>();
    SessionManager manager;
    AdapterSubTopic adapterSubTopic;
    LoaderView loaderView;
    CourseData courseData;
    CourseDBManager courseDBManager;

    private ItemClickListener listener;

    public AdapterTopic(Context context, List<TopicData> topicModels, CourseData courseData) {
        AppUtils.lastTopic = "";
        AppUtils.lastTopicLessonModel = new ArrayList<>();
        this.context = context;
        this.topicModels = topicModels;
        manager = new SessionManager(context);
        loaderView = new LoaderView(context);
        courseDBManager = new CourseDBManager(context);
        this.courseData = courseData;
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterTopic.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_topic_detial, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTopic.ViewHolder holder, int position) {

        TopicData topicModel = topicModels.get(position);

        holder.txt_topicName.setText(topicModel.getPost_title());

        holder.img_dropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.rcv_topics.setLayoutManager(new WrappingLinearLayoutManager(context));
                holder.rcv_topics.setHasFixedSize(false);
                holder.rcv_topics.setNestedScrollingEnabled(false);

                if (holder.rcv_topics.getVisibility() == View.GONE) {
                    if (holder.rcv_topics != null && holder.rcv_topics.getAdapter() != null  && holder.rcv_topics.getAdapter().getItemCount() > 0){
                        holder.img_dropDown.animate().rotation(180).setDuration(500).start();
                        holder.rcv_topics.setVisibility(View.VISIBLE);
                    }else {
                        if (AppUtils.lastTopic.equals(topicModel.getiD())) {
                            holder.img_dropDown.animate().rotation(180).setDuration(500).start();
                            holder.rcv_topics.setVisibility(View.VISIBLE);
                            adapterSubTopic = new AdapterSubTopic(context, AppUtils.lastTopicLessonModel);
                            holder.rcv_topics.setAdapter(adapterSubTopic);
                        }else {
                            productTopicLesson(topicModel.getiD(), holder.rcv_topics, holder.img_dropDown, holder.progress_circular);
                        }
                    }
                } else {
                    holder.img_dropDown.animate().rotation(0).setDuration(500).start();
                    holder.rcv_topics.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return topicModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardMain;
        ImageView img_dropDown;
        TextView txt_topicName;
        RecyclerView rcv_topics;
        ProgressBar progress_circular;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            img_dropDown = itemView.findViewById(R.id.img_dropDown);
            txt_topicName = itemView.findViewById(R.id.txt_topicName);
            rcv_topics = itemView.findViewById(R.id.rcv_topics);
            progress_circular = itemView.findViewById(R.id.progress_circular);

        }
    }

    public class AdapterSubTopic extends RecyclerView.Adapter<AdapterSubTopic.ViewHolder> {

        Context context;
        List<TopicLessonData> subTopicModels = new ArrayList<>();
        SessionManager manager;
        CourseDBManager dbManager;

        public AdapterSubTopic(Context context, List<TopicLessonData> topicModels) {
            this.context = context;
            this.subTopicModels = topicModels;
            manager = new SessionManager(context);
            dbManager = new CourseDBManager(context);
        }

        public void UpdateData(List<TopicLessonData> lessonData) {
            subTopicModels = new ArrayList<>();
            subTopicModels = lessonData;
            AdapterSubTopic.this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_sub_topic_detail, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            TopicLessonData topicModel = subTopicModels.get(position);

            holder.txt_sub_topic_name.setText(topicModel.getPost_title());

            if (topicModel.getVideo() != null && topicModel.getVideo().size() > 0) {

                String duration = topicModel.getVideo().get(0).getRuntime().getHours() + ":" +
                        topicModel.getVideo().get(0).getRuntime().getMinutes() + ":" +
                        topicModel.getVideo().get(0).getRuntime().getSeconds();
                holder.txt_sub_topic_duration.setText(duration);
                holder.txt_sub_topic_duration.setVisibility(View.VISIBLE);
                holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play_icon));

                /*if (dbManager.getCompleteLessonById(topicModel.getCourse_id()) != null && dbManager.getCompleteLessonById(topicModel.getCourse_id()).size() > 0) {
                    for (TopicLessonData data : dbManager.getCompleteLessonById(topicModel.getCourse_id())) {
                        if (data.getID().equals(topicModel.getID())) {
//                            holder.img_sub_topic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_complet_play));
                            holder.img_complete.setVisibility(View.VISIBLE);
                            break;
                        } else {
//                            holder.img_sub_topic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play));
                        }
                    }
                } else {
//                    holder.img_sub_topic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play));
                }*/

//                Log.e("Tag error", courseData.toString());
                System.out.println("Tag error price " + courseData.get_tutor_course_price_type());
                System.out.println("Tag error p-id " + courseData.get_tutor_course_product_id());

            } else if (topicModel.getPost_content() != null && !topicModel.getPost_content().equals("")) {
                holder.txt_sub_topic_duration.setVisibility(View.GONE);
                holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_document));
            } else {
                holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_play_icon));
            }

            /*if (dbManager.getCompleteLessonById(topicModel.getCourse_id()) != null && dbManager.getCompleteLessonById(topicModel.getCourse_id()).size() > 0) {
                for (TopicLessonData data : dbManager.getCompleteLessonById(topicModel.getCourse_id())) {
                    if (data.getID().equals(topicModel.getID())) {
//                        holder.img_sub_topic.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_complet_play));
                        holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_complet_play));
                        break;
                    }
                }
            }*/

            if (courseData.get_tutor_course_price_type() != null &&
                    !courseData.get_tutor_course_price_type().equals("null") &&
                    courseData.get_tutor_course_price_type().equals("free")) {

                /*if (dbManager != null && dbManager.checkCompleteLesson(topicModel.getCourse_id(), topicModel.getID())){
                    holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_complet_play));
                }*/

                if (dbManager != null && dbManager.checkPlayingLesson(topicModel.getCourse_id(), topicModel.getID()).equals("1")){
                    holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_complet_play));
                }else if (dbManager != null && dbManager.checkPlayingLesson(topicModel.getCourse_id(), topicModel.getID()).equals("0")){
                    holder.img_over.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_audio_waves));
                }

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (courseData != null && courseData.get_tutor_course_price_type() != null &&
                            courseData.get_tutor_course_price_type().equals("free")) {

                        if (courseDBManager != null && courseDBManager.getFreeActiveCourseById(courseData.getID()) != null) {

                            if (topicModel.getVideo() != null && topicModel.getVideo().size() > 0){
                                if (!topicModel.getVideo().get(0).getSource_youtube().equals("") ||
                                        !topicModel.getVideo().get(0).getSource_external_url().equals("") ||
                                        !topicModel.getVideo().get(0).getSource_vimeo().equals("")) {
                                    Intent intent = new Intent(context, VideoActivity.class);
                                    intent.putExtra("lessonData", new Gson().toJson(topicModel));
                                    context.startActivity(intent);
                                } else if (!topicModel.getVideo().get(0).getSource_video_id().equals("")){
                                    getVideoUrl(topicModel);
                                }
                            } else if (topicModel.getAttachments() != null && topicModel.getAttachments().size() > 0 && !topicModel.getAttachments().get(0).equals("")){
                                Intent intent = new Intent(context, VideoActivity.class);
                                intent.putExtra("lessonData", new Gson().toJson(topicModel));
                                context.startActivity(intent);
                            } else if (topicModel.getPost_content() != null && !topicModel.getPost_content().equals("")){
                                Intent intent = new Intent(context, VideoActivity.class);
                                intent.putExtra("lessonData", new Gson().toJson(topicModel));
                                context.startActivity(intent);
                            } else if (topicModel.getThumbnail() != null && !topicModel.getThumbnail().equals("null") && !topicModel.getThumbnail().equals("")){
                                Intent intent = new Intent(context, VideoActivity.class);
                                intent.putExtra("lessonData", new Gson().toJson(topicModel));
                                context.startActivity(intent);
                            } else if (!manager.isLoggedIn()){
                                AppUtils.show_toast_short(context, context.getString(R.string.login));
                            } else {
                                AppUtils.show_toast_short(context, context.getString(R.string.no_detail_view));
                            }
                        } else {
                            AppUtils.show_toast_short(context, context.getString(R.string.course_lesson_not_started));
                        }
                    } else if(courseData != null && courseData.get_tutor_course_product_id() == null ){
                        AppUtils.show_toast_short(context, context.getString(R.string.this_course_not_product_select));
                    } else if (courseData != null && courseData.get_tutor_course_product_id() != null &&
                            courseData.get_tutor_course_price_type().equals("null")) {
                        AppUtils.show_toast_short(context, context.getString(R.string.this_course_not_product_select));
                    } else if (courseData != null && courseData.get_tutor_course_price_type() != null &&
                            courseData.get_tutor_course_price_type().equals("paid")){
                        AppUtils.show_toast_short(context, context.getString(R.string.this_product_not_buy));
                    }else {
                        AppUtils.show_toast_short(context, context.getString(R.string.this_product_not_buy));
                    }
                }
            });

            /*if (topicModel.getAttachments() != null & topicModel.getAttachments().size()>0){

                if (courseData != null && courseData.get_tutor_course_price_type() != null &&
                        courseData.get_tutor_course_price_type().equals("free") &&
                        courseData.get_tutor_course_product_id() == null){

                    holder.img_sub_topic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                Intent intent = new Intent(context, VideoActivity.class);
                                intent.putExtra("attachment", new Gson().toJson(topicModel));
                                context.startActivity(intent);
                        }
                    });
                }

            }*/

            holder.img_sub_topic.setVisibility(View.INVISIBLE);
            if (topicModel.getThumbnail() != null && !topicModel.getThumbnail().equals("false") && topicModel.getThumbnail().length() > 0) {
                holder.img_sub_topic.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .asBitmap()
                        .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                        .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                        .load(topicModel.getThumbnail())
                        .into(new BitmapImageViewTarget(holder.img_sub_topic) {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        holder.img_sub_topic.setImageBitmap(resource);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return subTopicModels.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ImageView img_sub_topic, img_over;
            TextView txt_sub_topic_name, txt_sub_topic_duration;
            LinearLayout lin_over_image;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                img_sub_topic = itemView.findViewById(R.id.img_sub_topic);
                lin_over_image = itemView.findViewById(R.id.lin_over_image);
                img_over = itemView.findViewById(R.id.img_over);
                txt_sub_topic_name = itemView.findViewById(R.id.txt_sub_topic_name);
                txt_sub_topic_duration = itemView.findViewById(R.id.txt_sub_topic_duration);
            }
        }
    }

    public void productTopicLesson(String topic_id, RecyclerView rcv_topics, ImageView img_dropDown, @NotNull ProgressBar progressBar) {

        progressBar.setVisibility(View.VISIBLE);
        ApiService service = RetrofitClient.createServiceWoo(ApiService.class);
        service.getTopicLesson(topic_id).enqueue(new Callback<TopicLessonModel>() {
            @Override
            public void onResponse(@NotNull Call<TopicLessonModel> call, @NotNull Response<TopicLessonModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

//                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());
                    AppUtils.lastTopicLessonModel = new ArrayList<>();
                    if (response.body() != null && response.body().getData().size() > 0) {

                        if (rcv_topics.getVisibility() == View.GONE) {
                            img_dropDown.animate().rotation(180).setDuration(500).start();
                            rcv_topics.setVisibility(View.VISIBLE);
                        } else {
                            img_dropDown.animate().rotation(0).setDuration(500).start();
                            rcv_topics.setVisibility(View.GONE);
                        }

                        AppUtils.lastTopicLessonModel = response.body().getData();
                        AppUtils.lastTopic = topic_id;

                        adapterSubTopic = new AdapterSubTopic(context, AppUtils.lastTopicLessonModel);
                        rcv_topics.setAdapter(adapterSubTopic);
                        adapterSubTopic.UpdateData(AppUtils.lastTopicLessonModel);

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(context, context.getString(R.string.no_lesson_for_this_topic), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NotNull Call<TopicLessonModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void getVideoUrl(@NotNull TopicLessonData lessonData) {

        loaderView = new LoaderView(context);
        if (!((Activity) context).isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createServiceWooV2(ApiService.class);

        service.getVideoUrl(lessonData.getVideo().get(0).getSource_video_id()).enqueue(new Callback<MediaVideoModel>() {
            @Override
            public void onResponse(@NotNull Call<MediaVideoModel> call, @NotNull Response<MediaVideoModel> response) {

                if (response.isSuccessful() && response.body() != null) {

                    if (response.body().getMime_type().equals("video/mp4")) {

                        List<Videos> vid = new ArrayList<>();
                        Videos videos = new Videos();
                        videos.setSource_video_id(lessonData.getVideo().get(0).getSource_video_id());
                        videos.setSource(lessonData.getVideo().get(0).getSource());
                        videos.setPoster(lessonData.getVideo().get(0).getPoster());
                        videos.setRuntime(lessonData.getVideo().get(0).getRuntime());
                        videos.setSource_embedded(lessonData.getVideo().get(0).getSource_embedded());
                        videos.setSource_external_url(lessonData.getVideo().get(0).getSource_external_url());
                        videos.setSource_vimeo(lessonData.getVideo().get(0).getSource_vimeo());
                        videos.setSource_youtube(response.body().getSource_url());
                        vid.add(videos);
                        lessonData.setVideo(vid);
                        Intent intent = new Intent(context, VideoActivity.class);
                        intent.putExtra("lessonData", new Gson().toJson(lessonData));
                        intent.putExtra("orientation", Configuration.ORIENTATION_LANDSCAPE);
                        context.startActivity(intent);

                    }
                }

                loaderView.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<MediaVideoModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                loaderView.dismiss();
            }
        });
    }

}
