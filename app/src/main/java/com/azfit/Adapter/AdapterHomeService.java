package com.azfit.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.CartData.DBManagerData;
import com.azfit.Custome.GlideImageLoader;
import com.azfit.Model.Category.CategoryData;
import com.azfit.activity.ProductListActivity;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;


import java.util.ArrayList;
import java.util.List;

public class AdapterHomeService extends RecyclerView.Adapter<AdapterHomeService.ViewHolder> {

    Context context;
    List<CategoryData> productData = new ArrayList<>();
    DBManagerData managerData;

    public AdapterHomeService(Context context, List<CategoryData> productData) {
        this.context = context;
        this.productData = productData;
        managerData = new DBManagerData(context);
    }

    @NonNull
    @Override
    public AdapterHomeService.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adpter_over_service,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHomeService.ViewHolder holder, int position) {

        CategoryData model = productData.get(position);

        holder.txt_title.setText(model.getSlug().toUpperCase());
        holder.txt_first.setText(context.getString(R.string.total_course)+model.getCount());
        holder.txt_second.setText(context.getString(R.string.more_service));
//        holder.txt_price.setText(Html.fromHtml(model.getPrice_html()));

        if (model.getMeta_value() != null && model.getMeta_value().length() > 0  && !model.getMeta_value().equals("null")){

            RequestOptions options = new RequestOptions()
                    .error(R.drawable.ic_no_photo);

            new GlideImageLoader(holder.img_shape, holder.progress)
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL+model.getMeta_value(), options,null);

            /*Glide.with(context)
                    .asBitmap()
                    .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                    .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL+model.getMeta_value())
                    .into(new BitmapImageViewTarget(holder.img_shape){
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    holder.img_shape.setImageBitmap(resource);
                }
            });*/
        }

        /*if (model.getRating_count() != null){
            holder.txt_rate.setText(model.getAverage_rating());
        }*/

        /*holder.img_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("product", new Gson().toJson(model));
//                Toast.makeText(context, new Gson().toJson(model), Toast.LENGTH_LONG).show();
                context.startActivity(intent);
            }
        });*/

        holder.img_arrow.setVisibility(View.INVISIBLE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!model.getCount().equals("0")){
                    Intent intent = new Intent(context, ProductListActivity.class);
                    intent.putExtra("cat_id", model.getTerm_taxonomy_id());
                    intent.putExtra("cat_name", model.getSlug());
                    context.startActivity(intent);
                }else {
                    AppUtils.show_toast_short(context, context.getString(R.string.no_any_course_added));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardMain;
        TextView txt_title, txt_first, txt_second, txt_price, txt_rate;
        ImageView img_arrow, img_star;
        PorterShapeImageView img_shape;
        ProgressBar progress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_first = itemView.findViewById(R.id.txt_first);
            txt_second = itemView.findViewById(R.id.txt_second);
            txt_price = itemView.findViewById(R.id.txt_price);
            txt_rate = itemView.findViewById(R.id.txt_rate);
            img_arrow = itemView.findViewById(R.id.img_arrow);
            img_star = itemView.findViewById(R.id.img_star);
            img_shape = itemView.findViewById(R.id.img_shape);
            progress = itemView.findViewById(R.id.progress);

        }
    }

}
