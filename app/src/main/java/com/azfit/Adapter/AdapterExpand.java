package com.azfit.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.ServicesModel;
import com.azfit.Model.Topic.TopicData;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterExpand extends RecyclerView.Adapter<AdapterExpand.ViewHolder> {

    Context context;
    List<TopicData> dataModels = new ArrayList<>();
    public int checkedPosition = 0;
    SessionManager manager;

    private ItemClickListener listener;

    public AdapterExpand(Context context, List<TopicData> dataModels) {
        this.context = context;
        this.dataModels = dataModels;
        manager = new SessionManager(context);
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.expandable_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(dataModels.get(position));
    }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_expand;
        LinearLayout lin_expand;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_expand = itemView.findViewById(R.id.txt_expand_name);
            lin_expand = itemView.findViewById(R.id.lin_expand);

        }

        @SuppressLint("SetTextI18n")
        void bind(@NotNull final TopicData model){

            txt_expand.setText(model.getPost_title());

        }
    }
}
