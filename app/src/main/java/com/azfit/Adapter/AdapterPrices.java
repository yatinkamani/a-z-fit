package com.azfit.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.PriceDataModel;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterPrices extends RecyclerView.Adapter<AdapterPrices.ViewHolder> {

    Context context;
    List<PriceDataModel> dataModels = new ArrayList<>();
    public int checkedPosition = 0;

    public AdapterPrices(Context context, List<PriceDataModel> priceDataModels) {
        this.context = context;
        this.dataModels = priceDataModels;
    }

    @NonNull
    @Override
    public AdapterPrices.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_price_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPrices.ViewHolder holder, int position) {

        holder.bind(dataModels.get(position), context);

        holder.img_expand.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_plan_name, txt_plane_mode, txt_first_price, txt_last_price, txt_price_mode;
        ImageView img_expand;
        RecyclerView rcv_expand;
        Button btn_submit;
        LinearLayout lin_service_list;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_plan_name = itemView.findViewById(R.id.txt_plan_name);
            txt_plane_mode = itemView.findViewById(R.id.txt_plane_mode);
            txt_first_price = itemView.findViewById(R.id.txt_first_price);
            txt_last_price = itemView.findViewById(R.id.txt_last_price);
            txt_price_mode = itemView.findViewById(R.id.txt_price_mode);
            img_expand = itemView.findViewById(R.id.img_expand);
            rcv_expand = itemView.findViewById(R.id.rcv_expand);
            btn_submit = itemView.findViewById(R.id.btn_submit);
            lin_service_list = itemView.findViewById(R.id.lin_service_list);
        }

        @SuppressLint("SetTextI18n")
        public void bind(@NotNull final PriceDataModel model, Context context) {

            txt_plan_name.setText(model.getName());

            if (model.getPlane_mode().matches("year")){
                txt_plane_mode.setText(context.getString(R.string.yearly_payment));
                txt_price_mode.setText(context.getString(R.string.yearly));
            }else {
                txt_plane_mode.setText(context.getString(R.string.monthl_payment));
                txt_price_mode.setText(context.getString(R.string.month));
            }

            if (!model.getPrice().equals("") && model.getPrice().length() > 3 && model.getPrice().contains(".")){
                txt_first_price.setText(model.getPrice().substring(0, model.getPrice().indexOf(".")));
                txt_last_price.setText(model.getPrice().substring((model.getPrice().indexOf(".")+1), (model.getPrice().length()-1)));
            }



            /*rcv_expand.setLayoutManager(new LinearLayoutManager(context));
            rcv_expand.setItemAnimator(new DefaultItemAnimator());
            rcv_expand.setHasFixedSize(true);
            rcv_expand.setAdapter(new AdapterExpand(context,model.getList()));*/
            /*img_expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        setSelectedItem(position);
                        notifyDataSetChanged();
                    }

                    */
            /*if (rcv_expand.getVisibility() == View.VISIBLE){
                        lin_service_list.setVisibility(View.GONE);
                    } else {
                        lin_service_list.setVisibility(View.VISIBLE);
                    }*/
            /*
                }
            } );*/
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "comming soon", Toast.LENGTH_LONG).show();
                    */
            /*Intent intent = new Intent("cart");
                    intent.putExtra("item",1);
                    context.sendBroadcast(intent);*/
            /*
                }
            } );*/
        }
    }
}


