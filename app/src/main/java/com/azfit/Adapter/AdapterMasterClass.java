package com.azfit.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.Orders.Order;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.siyamed.shapeimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

public class AdapterMasterClass extends RecyclerView.Adapter<AdapterMasterClass.ViewHolder> {

    Context context;
    List<Order> orders = new ArrayList<>();

    public AdapterMasterClass(Context context, List<Order> productData) {
        this.context = context;
        this.orders = productData;
    }

    @NonNull
    @Override
    public AdapterMasterClass.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_master_class, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMasterClass.ViewHolder holder, int position) {

        Order order = orders.get(position);

        if (order.getStatus() != null && order.getStatus().length() > 0){
            Glide.with(context)
                    .asBitmap()
                    .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                    .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                    .load(order.getStatus())
                    .into(new BitmapImageViewTarget(holder.img_master) {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    holder.img_master.setImageBitmap(resource);
                }
            });
        }

        holder.img_overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.show_toast_short(context, "Video play in only youtube");
            }
        });

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void UpdateData(List<Order> orders) {
        orders = new ArrayList<>();
        orders = orders;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView img_overlay;
        RoundedImageView img_master;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_overlay = itemView.findViewById(R.id.txt_order_number);
            img_master = itemView.findViewById(R.id.txt_name);

        }
    }
}
