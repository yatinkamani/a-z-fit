package com.azfit.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.Orders.Order;
import com.azfit.Model.Orders.LineItems;
import com.azfit.R;
import com.azfit.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

public class AdapterOrder extends RecyclerView.Adapter<AdapterOrder.ViewHolder> {

    Context context;
    List<Order> orders = new ArrayList<>();

    public AdapterOrder(Context context, List<Order> productData) {
        this.context = context;
        this.orders = productData;
    }

    @NonNull
    @Override
    public AdapterOrder.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterOrder.ViewHolder holder, int position) {

        Order order = orders.get(position);

//        List<String> names = new ArrayList<>();

        /*for (LineItems items : order.getLine_items()) {
            names.add(items.getName());
        }*/

        if (order.getStatus().equals("pending") || order.getStatus().equals("processing")) {
            holder.img_status.setImageDrawable(context.getResources().getDrawable(R.drawable.round_pending));
        } else if (order.getStatus().equals("completed")) {
            holder.img_status.setImageDrawable(context.getResources().getDrawable(R.drawable.round_complete));
        } else {
            holder.img_status.setImageDrawable(context.getResources().getDrawable(R.drawable.round_cancle));
        }

        holder.txt_date.setText(AppUtils.getFormattedDateTZ(order.getCreated_at()));

        holder.txt_totalOfItem.setText(order.getTotal() + "€ " + context.getString(R.string.fors) + " " + order.getTotal_line_items_quantity() + " " + context.getString(R.string.Items));
        holder.txt_order_number.setText("# " + order.getOrder_number());

        holder.itemView.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void UpdateData(List<Order> orders) {
        this.orders = new ArrayList<>();
        this.orders = orders;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_status;
        TextView txt_order_number, txt_date, txt_totalOfItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_order_number = itemView.findViewById(R.id.txt_order_number);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_totalOfItem = itemView.findViewById(R.id.txt_totalOfItem);
            img_status = itemView.findViewById(R.id.img_status);

        }
    }
}
