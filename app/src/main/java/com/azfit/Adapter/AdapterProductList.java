package com.azfit.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Custome.GlideImageLoader;
import com.azfit.Model.Course.CourseData;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

public class AdapterProductList extends RecyclerView.Adapter<AdapterProductList.ViewHolder> {

    Context context;
    List<CourseData> serviceData;
    SessionManager manager;

    private ItemClickListener listener;

    public AdapterProductList(Context context, List<CourseData> serviceData) {
        this.context = context;
        this.serviceData = serviceData;
        manager = new SessionManager(context);
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterProductList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_product_list, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterProductList.ViewHolder holder, int position) {

        CourseData data = serviceData.get(position);

        holder.txt_name.setText(data.getPost_title().toUpperCase());

        if (data.getCourse_image() != null && data.getCourse_image().length()>0){

            /*holder.progress.setMax(100);
            RequestOptions o = new RequestOptions().error(R.drawable.ic_no_photo);
            new GlideImageLoader(holder.img_image, holder.progress)
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL + data.getCourse_image(), o, value -> {
                        Log.e("Tag progress Holder", ""+value);
                        holder.progress.setProgress(value);
                        holder.progress.getProgressDrawable().mutate();
                    });*/

            Glide.with(context)
                    .asBitmap()
                    .load(AppUtils.CATEGORY_IMAGE_BASE_URL+data.getCourse_image())
                    .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                    .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                    .into(new BitmapImageViewTarget(holder.img_image){
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            holder.img_image.setImageBitmap(resource);
                        }
                    });
        }

//        holder.img_image.setImageDrawable(data.getService_image());

        holder.btn_read_more.setClickable(true);
        holder.itemView.setClickable(true);

    }

    @Override
    public int getItemCount() {
        return serviceData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardMain;
        ImageView img_image;
        TextView txt_name;
        Button btn_read_more;
        ProgressBar progress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            img_image = itemView.findViewById(R.id.img_image);
            progress = itemView.findViewById(R.id.progress);
            txt_name = itemView.findViewById(R.id.txt_name);
            btn_read_more = itemView.findViewById(R.id.btn_read_more);
        }
    }
}
