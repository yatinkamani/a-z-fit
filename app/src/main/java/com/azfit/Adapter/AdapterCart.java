package com.azfit.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.CartItem;
import com.azfit.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.siyamed.shapeimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

public class AdapterCart extends RecyclerView.Adapter<AdapterCart.ViewHolder> {

    Context context;
    List<CartItem> productData = new ArrayList<>();

    public AdapterCart(Context context, List<CartItem> productData) {
        this.context = context;
        this.productData = productData;
    }

    @NonNull
    @Override
    public AdapterCart.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_cart_item, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCart.ViewHolder holder, int position) {

        CartItem model = productData.get(position);

        holder.txt_product_name.setText(model.getProduct().getName());
        holder.txt_product_price.setText("$ "+(Float.parseFloat(model.getProduct().getPrice()) * model.getQty()));

        if (model.getProduct().getImages() != null && model.getProduct().getImages().size() >0){

            Glide.with(context)
                    .asBitmap()
                    .error(context.getResources().getDrawable(R.drawable.ic_no_photo))
                    .placeholder(context.getResources().getDrawable(R.drawable.loader_progress))
                    .load(model.getProduct().getImages().get(0).getSrc())
                    .into(new BitmapImageViewTarget(holder.img_product_image){
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    holder.img_product_image.setImageBitmap(resource);
                }
            });
        }

        holder.txt_quantity.setText(""+model.getQty());

        holder.btn_minus.setClickable(true);
        holder.btn_plus.setClickable(true);
        holder.itemView.setClickable(true);
        holder.txt_quantity.setClickable(true);
    }

    @Override
    public int getItemCount() {
        return productData.size();
    }

    public void UpdateData(List<CartItem> cartItems) {
        productData = new ArrayList<>();
        productData = cartItems;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        RoundedImageView img_product_image;
        TextView txt_product_name, txt_product_price;
        Button btn_minus, btn_plus;
        TextView txt_quantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_product_image = itemView.findViewById(R.id.img_product_image);
            txt_product_name = itemView.findViewById(R.id.txt_product_name);
            txt_product_price = itemView.findViewById(R.id.txt_product_price);
            btn_minus = itemView.findViewById(R.id.btn_minus);
            txt_quantity = itemView.findViewById(R.id.txt_quantity);
            btn_plus = itemView.findViewById(R.id.btn_plus);

        }
    }
}
