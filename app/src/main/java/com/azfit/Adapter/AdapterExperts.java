package com.azfit.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Model.ExpertsDataModel;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterExperts extends RecyclerView.Adapter<AdapterExperts.ViewHolder> {

    Context context;
    List<ExpertsDataModel> expertsDataModels = new ArrayList<>();
    SessionManager manager;

    private ItemClickListener listener;

    public AdapterExperts(Context context, List<ExpertsDataModel> expertsDataModels) {
        this.context = context;
        this.expertsDataModels = expertsDataModels;
        manager = new SessionManager(context);
    }

    public interface ItemClickListener {
        public void onItemClick(String id);
    }

    public void setListener(ItemClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterExperts.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_experts_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterExperts.ViewHolder holder, int position) {
        holder.bind(expertsDataModels.get(position));
    }

    @Override
    public int getItemCount() {
        return expertsDataModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardMain;
        ImageView img_expert_service;
        TextView txt_expert_name, txt_expert_field;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardMain = itemView.findViewById(R.id.cardMain);
            img_expert_service = itemView.findViewById(R.id.img_expert_service);
            txt_expert_name = itemView.findViewById(R.id.txt_expert_name);
            txt_expert_field = itemView.findViewById(R.id.txt_expert_field);

        }

        @SuppressLint("SetTextI18n")
        void bind(@NotNull final ExpertsDataModel model){

            txt_expert_name.setText(model.getName());
            txt_expert_field.setText(model.getField());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "coming soon", Toast.LENGTH_LONG).show();
                    /*Intent intent = new Intent("cart");
                    intent.putExtra("item",1);
                    context.sendBroadcast(intent);*/
                }
            });
        }
    }
}
