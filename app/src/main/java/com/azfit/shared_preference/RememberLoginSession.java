package com.azfit.shared_preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.azfit.utils.MyApplicationClass;

public class RememberLoginSession {

    private static final String PREF_NAME = "Remember Session";

    private static final String KEY_USERID = "user_id";
    private static final String KEY_USERNAME = "userName";
    private static final String KEY_FIRSTNAME = "firstName";
    private static final String KEY_SURNAME = "suName";
    private static final String KEY_USEREMAIL = "userEmail";
    private static final String KEY_PASSWORD = "password";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static SessionManager instance;

    @SuppressLint("CommitPrefEdits")
    public RememberLoginSession(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public RememberLoginSession() {
        sharedPreferences = MyApplicationClass.getAppContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SessionManager getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new SessionManager();
            return instance;
        }
    }

    public String getUserId() {
        return sharedPreferences.getString(KEY_USERID, "");
    }

    public void setUserId(String user_id) {
        editor.putString(KEY_USERID, user_id);
        editor.apply();
    }

    public String getPassword() {
        return sharedPreferences.getString(KEY_PASSWORD, "");
    }

    public void setPassword(String password) {
        editor.putString(KEY_PASSWORD, password);
        editor.apply();
    }

    public String getUserName() {
        return sharedPreferences.getString(KEY_USERNAME, "");
    }

    public void setUserName(String userName) {
        editor.putString(KEY_USERNAME, userName);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(KEY_USEREMAIL, "");
    }

    public void setUserEmail(String userEmail) {
        editor.putString(KEY_USEREMAIL, userEmail);
        editor.apply();
    }

    public String getFirstName() {
        return sharedPreferences.getString(KEY_FIRSTNAME, "");
    }

    public void setFirstName(String firstName) {
        editor.putString(KEY_USEREMAIL, firstName);
        editor.apply();
    }

    public String getSurName() {
        return sharedPreferences.getString(KEY_SURNAME, "");
    }

    public void setSurName(String surName) {
        editor.putString(KEY_USEREMAIL, surName);
        editor.apply();
    }
}
