package com.azfit.shared_preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.azfit.Model.Cutomers.CustomersDataModel;
import com.azfit.utils.MyApplicationClass;
import com.google.gson.Gson;

/**
 * Created by Admin on 09/10/2017.
 */

public class SessionManager {

    private static final String PREF_NAME = "Login Session";
    private static final String KEY_FCM_TOKEN = "fcm_Token";
    private static final String KEY_USERID = "user_id";
    private static final String KEY_USERNAME = "userName";
    private static final String KEY_USER_DETAIL = "userDetail";
    private static final String KEY_LASTNAME = "lastName";
    private static final String KEY_USEREMAIL = "userEmail";
    private static final String IS_LOGIN = "isLogin";
    private static final String KEY_CART_ITEM = "cart_item";

    private String KEY_POSITION = "position";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static SessionManager instance;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public SessionManager() {
        sharedPreferences = MyApplicationClass.getAppContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SessionManager getInstance() {
        if (instance != null)
            return instance;
        else {
            instance = new SessionManager();
            return instance;
        }
    }

    public void Logout(Context context) {
        try {
            SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFcmToken() {
        return sharedPreferences.getString(KEY_FCM_TOKEN, "");
    }

    public void setFcmToken(String fcmToken) {
        editor.putString(KEY_FCM_TOKEN, fcmToken);
        editor.apply();
    }

    public void setProfileColor(int color) {
        editor.putInt("profile_color", color);
        editor.apply();
    }

    public int getProfileColor() {
        return sharedPreferences.getInt("profile_color", 0);
    }

    public String getUserId() {
        return sharedPreferences.getString(KEY_USERID, "");
    }

    public void setUserId(String user_id) {
        editor.putString(KEY_USERID, user_id);
        editor.apply();
    }

    public String getUserName() {
        return sharedPreferences.getString(KEY_USERNAME, "");
    }

    public void setUserName(String userName) {
        editor.putString(KEY_USERNAME, userName);
        editor.apply();
    }

    public CustomersDataModel getLoginUserDetail() {
        return new Gson().fromJson(sharedPreferences.getString(KEY_USER_DETAIL, ""), CustomersDataModel.class);
    }

    public void setLoginUserDetail(CustomersDataModel user) {
        editor.putString(KEY_USER_DETAIL, new Gson().toJson(user));
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(KEY_USEREMAIL, "");
    }

    public void setUserEmail(String userEmail) {
        editor.putString(KEY_USEREMAIL, userEmail);
        editor.apply();
    }

    public int getCartTotal() {
        return sharedPreferences.getInt(KEY_CART_ITEM, 0);
    }

    public void setCartItem(int item) {
        editor.putInt(KEY_CART_ITEM, item);
        editor.apply();
    }

    public void setIsLogin() {
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
    }

}
