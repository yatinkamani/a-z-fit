package com.azfit.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.azfit.Adapter.AdapterOrder;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.CartData.DBManagerData;
import com.azfit.CourseDatabase.CourseDBManager;
import com.azfit.WishlistData.WishlistDBManager;
import com.azfit.activity.LoginActivity;
import com.azfit.Model.Cutomers.CustomersDataModel;
import com.azfit.Model.Orders.Order;
import com.azfit.Model.Orders.OrdersModel;
import com.azfit.activity.OrderDetailsActivity;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.azfit.utils.RecyclerTouchListener;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.azfit.utils.AppUtils.instance;
import static com.azfit.utils.AppUtils.show_toast_short;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    ImageView img_logout;
    TextView txt_header_title;

    SessionManager sessionManager;
    LoaderView loaderView;

    // no data
    View no_data;
    TextView txt_msg;
    Button btn_retry;

    AutoCompleteTextView AutoTxt_salutation;
    EditText ed_name, ed_surname, ed_companyName, ed_streetName, ed_room, ed_zipCode, ed_cityArea, ed_telephone, ed_email, ed_comment;
    Button btn_update_profile;

    LinearLayout lin_no_order, lin_view;
    TextView txt_no_order, btn_retry_order;
    RecyclerView rcv_order;
    AdapterOrder adapterOrder;

    List<Order> orders = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        AppUtils.validation = getActivity();
        sessionManager = new SessionManager(getActivity());
        loaderView = new LoaderView(getActivity());

        initView(root);

        return root;
    }

    public void initView(@NotNull View view){

        img_logout = view.findViewById(R.id.img_logout);

        AutoTxt_salutation = view.findViewById(R.id.AutoTxt_salutation);

        ed_name = view.findViewById(R.id.ed_name);
        ed_surname = view.findViewById(R.id.ed_surname);
        ed_companyName = view.findViewById(R.id.ed_companyName);
        ed_streetName = view.findViewById(R.id.ed_streetName);
        ed_room = view.findViewById(R.id.ed_room);
        ed_zipCode = view.findViewById(R.id.ed_zipCode);
        ed_cityArea = view.findViewById(R.id.ed_cityArea);
        ed_telephone = view.findViewById(R.id.ed_telephone);
        ed_email = view.findViewById(R.id.ed_email);
        ed_comment = view.findViewById(R.id.ed_comment);
        btn_update_profile = view.findViewById(R.id.btn_update_profile);
        btn_update_profile.setOnClickListener(this);

        no_data = view.findViewById(R.id.no_data);
        txt_msg = view.findViewById(R.id.txt_message);
        btn_retry = view.findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(this);

        lin_view = view.findViewById(R.id.lin_view);

        lin_no_order = view.findViewById(R.id.lin_no_order);
        txt_no_order = view.findViewById(R.id.txt_no_order);
        btn_retry_order = view.findViewById(R.id.btn_retry_order);
        rcv_order = view.findViewById(R.id.rcv_order);

        txt_header_title = view.findViewById(R.id.txt_header_title);

        btn_retry_order.setOnClickListener(this);
        img_logout.setOnClickListener(this);

        initializeView();
        setAutoCompleteText();
    }

    public void initializeView() {

        if (sessionManager.isLoggedIn()){
            lin_view.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.GONE);
            img_logout.setVisibility(View.VISIBLE);
            if (AppUtils.isNetworkAvailable(getActivity())){
                getOrderApi();
            }else {
                show_toast_short(getActivity(), getString(R.string.internet_not_connect));
            }
            fillDetail();
            txt_header_title.setText(getString(R.string.my_profile));
        } else {
            txt_header_title.setText(getString(R.string.app_name));
            lin_view.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
            txt_msg.setText(getString(R.string.click_to_signin));
            btn_retry.setText(getString(R.string.sign_in));
            img_logout.setVisibility(View.GONE);
        }
    }

    public void setAutoCompleteText() {

        List<String> strings = new ArrayList<>();
        strings.add(getString(R.string.no_));
        strings.add(getString(R.string.mr_));
        strings.add(getString(R.string.ms_));
        strings.add(getString(R.string.drivers_));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1, strings);
        AutoTxt_salutation.setThreshold(1);
        AutoTxt_salutation.setAdapter(adapter);

    }

    public void validation() {

        if (!AppUtils.validationText(ed_name)) {
            return;
        }

        if (!AppUtils.validationText(ed_surname)) {
            return;
        }

        if (!AppUtils.validationText(ed_streetName)) {
            return;
        }

        if (!AppUtils.validationText(ed_zipCode)) {
            return;
        }

        if (!AppUtils.validationText(ed_cityArea)) {
            return;
        }

        if (!AppUtils.validationText(ed_email)) {
            return;
        }

        if (!AppUtils.validationEmail(ed_email)) {
            return;
        }

        if (AppUtils.isNetworkAvailable(getActivity())) {
            updateAddressApi();
        } else {
            show_toast_short(getActivity(), getString(R.string.internet_not_connect));
        }
    }

    @Override
    public void onClick(View v) {

        if (v == btn_retry){
            instance = null;
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivityForResult(intent, 111);
        } else if (v == btn_retry_order){
            getOrderApi();
        } else if (v == btn_update_profile) {
            validation();
        } else if (v == img_logout) {

            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.app_name))
                    .setMessage(getString(R.string.do_you_want_to_logout))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    sessionManager.logoutUser();

                                    CourseDBManager courseDBManager = new CourseDBManager(getActivity());
                                    WishlistDBManager wishlistDBManager = new WishlistDBManager(getActivity());
                                    DBManagerData dbManagerData = new DBManagerData(getActivity());

                                    courseDBManager.removeDatabase();
                                    wishlistDBManager.removeDatabase();
                                    dbManagerData.removeDatabase();
                                    /*Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);*/
                                    initializeView();

                                }
                            })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 111){
            initializeView();
        }
    }

    public void fillDetail() {

        if (sessionManager.getLoginUserDetail() != null){

            if (sessionManager.getLoginUserDetail().getCustomer() != null) {

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name().equals("")){
                    ed_name.setText(sessionManager.getLoginUserDetail().getCustomer().getFirst_name());
                }else {
                    ed_name.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getFirst_name());
                }

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name().equals("")){
                    ed_surname.setText(sessionManager.getLoginUserDetail().getCustomer().getLast_name());
                }else {
                    ed_surname.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getLast_name());
                }

                if (sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getEmail().equals("")){
                    ed_email.setText(sessionManager.getLoginUserDetail().getCustomer().getEmail());
                }else {
                    ed_email.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getEmail());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1().equals("")){
                    ed_streetName.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_1());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2().equals("")){
                    ed_room.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getAddress_2());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCompany().equals("")){
                    ed_companyName.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCompany());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCity().equals("")){
                    ed_cityArea.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getCity());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPhone().equals("")){
                    ed_telephone.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPhone());
                }

                if (!sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode().equals("")){
                    ed_zipCode.setText(sessionManager.getLoginUserDetail().getCustomer().getBilling_address().getPostcode());
                }
            }
        }
    }

    public void getOrderApi() {

        if (AppUtils.isNetworkAvailable(getActivity())){

            if (!getActivity().isFinishing()){
                loaderView.show();
            }
            ApiService apiService = RetrofitClient.createService(ApiService.class);
            apiService.getOrders(AppUtils.MY_ORDERS, sessionManager.getUserId()).enqueue(new Callback<OrdersModel>() {
                @Override
                public void onResponse(@NotNull Call<OrdersModel> call, @NotNull Response<OrdersModel> response) {

                    LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                    LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                    if (response.isSuccessful() && response.body() != null){

                        try {
                            LogUtils.error(LoginActivity.class, "Tag Response "+new JSONObject(response.body().toString()).toString(2));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        orders = new ArrayList<>();
                        if (response.body().getOrders() != null) {

                            for (int i=0; i< response.body().getOrders().size(); i++){
                                if (response.body().getOrders().get(i).getLine_items() != null &&
                                        response.body().getOrders().get(i).getLine_items().size()>0)
                                    orders.add(response.body().getOrders().get(i));
                            }
                            setOrderData();
                        } else {
                            rcv_order.setVisibility(View.GONE);
                            lin_no_order.setVisibility(View.VISIBLE);
                            txt_no_order.setText(getActivity().getString(R.string.something_went_wrong));
                            btn_retry.setVisibility(View.VISIBLE);
                        }
                    }else {
                        rcv_order.setVisibility(View.GONE);
                        lin_no_order.setVisibility(View.VISIBLE);
                        txt_no_order.setText(getActivity().getString(R.string.something_went_wrong));
                        btn_retry.setVisibility(View.VISIBLE);
                    }
                    if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                        loaderView.dismiss();
                    }

                }

                @Override
                public void onFailure(@NotNull Call<OrdersModel> call, @NotNull Throwable t) {

                    LogUtils.error(ProfileFragment.class, t.getMessage());

                    rcv_order.setVisibility(View.GONE);
                    lin_no_order.setVisibility(View.VISIBLE);
                    txt_no_order.setText(getActivity().getString(R.string.something_went_wrong));
                    btn_retry.setVisibility(View.VISIBLE);
                    if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                        loaderView.dismiss();
                    }
                }
            });
        }else {
            rcv_order.setVisibility(View.GONE);
            lin_no_order.setVisibility(View.VISIBLE);
            txt_no_order.setText(getString(R.string.internet_not_connect));
            btn_retry.setVisibility(View.VISIBLE);
        }
    }

    public void setOrderData() {

        if (orders != null && orders.size() > 0){

            rcv_order.setVisibility(View.VISIBLE);
            lin_no_order.setVisibility(View.GONE);

            StaggeredGridLayoutManager layoutManager  = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL);
            rcv_order.setLayoutManager(layoutManager);
            rcv_order.setHasFixedSize(true);
            rcv_order.setItemAnimator(new DefaultItemAnimator());
            rcv_order.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    layoutManager.requestLayout();
                }
            });

            rcv_order.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rcv_order, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Intent intent = new Intent(getContext(), OrderDetailsActivity.class);
                    intent.putExtra("orders", new Gson().toJson(orders.get(position)));
                    getActivity().startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
            adapterOrder = new AdapterOrder(getActivity(), orders);
            rcv_order.setAdapter(adapterOrder);
            adapterOrder.UpdateData(orders);

        }else {
            rcv_order.setVisibility(View.GONE);
            lin_no_order.setVisibility(View.VISIBLE);
            txt_no_order.setText(getString(R.string.no_any_order_data_available));
            btn_retry.setVisibility(View.GONE);
        }
    }

    public void updateAddressApi() {

        if (!getActivity().isFinishing()){
            loaderView.show();
        }
        ApiService service = RetrofitClient.createService(ApiService.class);

        JSONObject billing_address = new JSONObject();
        try {
            billing_address.put("first_name",ed_name.getText().toString());
            billing_address.put("last_name", ed_surname.getText().toString());
            billing_address.put("address_1", ed_streetName.getText().toString());
            billing_address.put("address_2",ed_room.getText().toString());
            billing_address.put("city",ed_cityArea.getText().toString());
            billing_address.put("state","");
            billing_address.put("postcode",ed_zipCode.getText().toString());
            billing_address.put("country","Germany");
            billing_address.put("email",ed_email.getText().toString());
            billing_address.put("phone",ed_telephone.getText().toString());
            billing_address.put("company", ed_companyName.getText().toString());
        } catch (JSONException e) {

            e.printStackTrace();
        }
        if (billing_address == null){
            show_toast_short(getActivity(), getString(R.string.order_successfully));
            return;
        }

        JSONObject object = new JSONObject();
        JSONObject object_b = new JSONObject();
        try {
            object_b.put("billing_address", billing_address);
            object.put("customer", object_b);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        service.update_billing(AppUtils.UPDATE_CUSTOMER, sessionManager.getUserId(),
                object_b.toString()).enqueue(new Callback<CustomersDataModel>() {
            @Override
            public void onResponse(@NotNull Call<CustomersDataModel> call, @NotNull Response<CustomersDataModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                if (response.isSuccessful() && response.body() != null){

                    LogUtils.error(LoginActivity.class, "Tag Response "+response.body());

                    if (response.body().getCustomer() != null){
                        sessionManager.setIsLogin();
                        sessionManager.setUserId(response.body().getCustomer().getId());
                        sessionManager.setLoginUserDetail(response.body());
                        sessionManager.setUserEmail(response.body().getCustomer().getEmail());
                        sessionManager.setLoginUserDetail(response.body());

                        Toast.makeText(getActivity(), "Successfully Update Address...", Toast.LENGTH_LONG).show();
                    } else if (response.body().getStatus().equals("false")) {
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.order_successfully), Toast.LENGTH_LONG).show();
                    }
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<CustomersDataModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }
        });

    }

}