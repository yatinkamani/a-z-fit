package com.azfit.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Adapter.AdapterWishlist;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.activity.DetailActivity;
import com.azfit.activity.LoginActivity;
import com.azfit.Model.Product.SearchProductModel;
import com.azfit.R;
import com.azfit.WishlistData.WishlistDBManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.azfit.utils.RecyclerTouchListener;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteFragment extends Fragment implements View.OnClickListener {

    RecyclerView rcv_wishList;
    // cart icon
    RelativeLayout counterValuePanel, counterPanel;
    TextView txt_count;

    View no_data;
    Button btn_retry;
    TextView txt_message;
    LoaderView loaderView;

    WishlistDBManager dbManager;
    AdapterWishlist adapterWishlist;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_favourite, container, false);

        dbManager = new WishlistDBManager(getActivity());
        loaderView = new LoaderView(getActivity());

        initView(root);

        return root;
    }

    public void initView(@NotNull View view) {
        rcv_wishList = view.findViewById(R.id.rcv_wishlist);

        // cart view
        counterValuePanel = view.findViewById(R.id.counterValuePanel);
        counterPanel = view.findViewById(R.id.counterPanel);
        txt_count = view.findViewById(R.id.count);

        // no data
        no_data = view.findViewById(R.id.no_data);
        btn_retry = view.findViewById(R.id.btn_retry);
        txt_message = view.findViewById(R.id.txt_message);

        counterValuePanel.setOnClickListener(this);

        rcv_wishList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rcv_wishList.setItemAnimator(new DefaultItemAnimator());
        rcv_wishList.setHasFixedSize(false);

        setListData();

    }

    public void setListData() {

        if (dbManager != null && dbManager.fetch().size() >0){
            no_data.setVisibility(View.GONE);
            rcv_wishList.setVisibility(View.VISIBLE);

            adapterWishlist = new AdapterWishlist(getActivity(), dbManager.fetch());
            rcv_wishList.setAdapter(adapterWishlist);

            rcv_wishList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rcv_wishList, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    ImageView  img_fav = view.findViewById(R.id.img_fav);
                    ImageView  img_share = view.findViewById(R.id.img_share);

                    img_fav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getString(R.string.app_name))
                                    .setMessage(R.string.do_you_really_want_to_remove_this_product)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setPositiveButton(R.string.ok,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    dbManager.Delete(dbManager.fetch().get(position).getCourse_id());
                                                    setListData();
                                                }
                                            })
                                    .setNegativeButton(R.string.no, null)
                                    .show();
                        }
                    });

                    img_share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                            txtIntent .setType("text/plain");
                            txtIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, dbManager.fetch().get(position).getCourse_name());
                            txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, AppUtils.CATEGORY_IMAGE_BASE_URL+dbManager.fetch().get(position).getImage());
                            startActivity(Intent.createChooser(txtIntent ,"Share"));
                        }
                    });

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), DetailActivity.class);
                            intent.putExtra("course_id", dbManager.fetch().get(position).getCourse_id());
                            intent.putExtra("cat_name", dbManager.fetch().get(position).getCategories());
                            startActivity(intent);
                        }
                    });
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }) );

        }else {
            no_data.setVisibility(View.VISIBLE);
            rcv_wishList.setVisibility(View.GONE);
            btn_retry.setVisibility(View.GONE);
            txt_message.setText(getString(R.string.empty_favourite_list));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        return;
    }

    public void productDetailApi (String string) {

        if (!getActivity().isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getFilteredProduct(string).enqueue(new Callback<SearchProductModel>() {
            @Override
            public void onResponse(@NotNull Call<SearchProductModel> call, @NotNull Response<SearchProductModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

//                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null){

                    LogUtils.error(LoginActivity.class, "Tag Response "+response.body());

                    if (response.body().getAll_products() != null && response.body().getAll_products().size() > 0){

                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        intent.putExtra("product", new Gson().toJson(response.body().getAll_products().get(0)));
                        startActivity(intent);

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), getString(R.string.course_detail_not_found), Toast.LENGTH_LONG).show();
                    }

                } else {
//                    no_data.setVisibility(View.VISIBLE);
                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                }
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<SearchProductModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
//                no_data.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()){
                    loaderView.dismiss();
                }
            }

        });
    }

}