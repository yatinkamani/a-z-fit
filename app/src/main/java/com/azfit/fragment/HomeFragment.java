package com.azfit.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Adapter.AdapterHomeService;
import com.azfit.Adapter.Adapter_CardSlider;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.activity.CartActivity;
import com.azfit.CartData.DBManagerData;
import com.azfit.Custome.WrappingLinearLayoutManager;
import com.azfit.activity.LoginActivity;
import com.azfit.Model.Category.CategoryData;
import com.azfit.Model.Category.CategoryModel;
import com.azfit.Model.Product.ProductData;
import com.azfit.Model.Product.ProductModel;
import com.azfit.R;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.BonceInterpolate;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.github.islamkhsh.CardSliderAdapter;
import com.github.islamkhsh.CardSliderViewPager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {

    RecyclerView rcv_service;
    AdapterHomeService adapterHomeService;
    CardSliderAdapter sliderAdapter;
    CardSliderViewPager sliderPager;
    TextView txt_header_title, txt_user_name;

    SessionManager sessionManager;

    // cart icon
    RelativeLayout counterValuePanel, counterPanel;
    TextView txt_count;

    List<ProductData> productData = new ArrayList<>();
    List<CategoryData> categoryData = new ArrayList<>();

    // no data view
    View no_data;
    Button btn_retry;

    LoaderView loaderView;

    DBManagerData dbManager;

    int images[] = {R.drawable.baby, R.drawable.baby, R.drawable.baby};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        sessionManager = new SessionManager(getActivity());
        loaderView = new LoaderView(getActivity());
        dbManager = new DBManagerData(getActivity());
        initView(root);
        return root;

    }

    public void initView(@NotNull View view) {
        rcv_service = view.findViewById(R.id.rcv_service);
        sliderPager = view.findViewById(R.id.sliderPager);
        txt_user_name = view.findViewById(R.id.txt_user_name);
        txt_header_title = view.findViewById(R.id.txt_header_title);

        // cart view
        counterValuePanel = view.findViewById(R.id.counterValuePanel);
        counterPanel = view.findViewById(R.id.counterPanel);
        txt_count = view.findViewById(R.id.count);

        // no data
        no_data = view.findViewById(R.id.no_data);
        btn_retry = view.findViewById(R.id.btn_retry);

        rcv_service.setLayoutManager(new WrappingLinearLayoutManager(getContext()));
        rcv_service.setHasFixedSize(false);
        rcv_service.setNestedScrollingEnabled(false);
//        rcv_service.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.getLoginUserDetail() != null){
            txt_user_name.setText(sessionManager.getLoginUserDetail().getCustomer().getFirst_name());
        }

        counterValuePanel.setOnClickListener(this);
        counterPanel.setOnClickListener(this);
        btn_retry.setOnClickListener(this);

        String s = "a:3:{s:5:\"hours\";s:2:\"07\";s:7:\"minutes\";s:2:\"01\";s:7:\"seconds\";s:2:\"23\";}";
        String[] sp = s.split("\"");

        for (int i=0; i<sp.length;i++){
            Log.e("Tag string", i+"  "+sp[i]);
        }

        setGreetingName();
        setImage();
//        ProductApi ();

        if (AppUtils.categoryData != null && AppUtils.categoryData.size()>0){
            
            categoryData = AppUtils.categoryData;
            setCategoryList();
        }else {
            courseCategoryApi();
        }
        buildCounterDrawable();

    }

    public void setGreetingName() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        String msg = "";
        if(timeOfDay >= 0 && timeOfDay < 12){
//            Toast.makeText(this, "Good Morning", Toast.LENGTH_SHORT).show();
            msg = getString(R.string.good_moring);
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            msg = getString(R.string.good_afternoon);
//            Toast.makeText(this, "Good Afternoon", Toast.LENGTH_SHORT).show();
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            msg = getString(R.string.good_evening);
//            Toast.makeText(this, "Good Evening", Toast.LENGTH_SHORT).show();
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            msg = getString(R.string.good_night);
//            Toast.makeText(this, "Good Night", Toast.LENGTH_SHORT).show();
        }
        txt_header_title.setText(msg);
    }

    public void setImage() {
        if (getContext() != null) {

            sliderAdapter = new Adapter_CardSlider(getContext(), images);
            sliderPager.setAdapter(sliderAdapter);

            /*int rndInt = new Random().nextInt(images.length);
            banner_image.setImageDrawable(getResources().getDrawable(images[rndInt]));
            banner_image.setScaleX(1.0f);
            banner_image.setScaleY(1.0f);

            banner_image.animate()
                    .scaleX(1.5f)
                    .scaleY(1.5f)
                    .setDuration(10000)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            setImage();
                        }
                    })
                    .start();*/
        }
    }

    public void ProductApi () {

        if (!getActivity().isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getProduct().enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(@NotNull Call<ProductModel> call, @NotNull Response<ProductModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null){

                    try {
                        JSONObject object = new JSONObject(response.body().toString());
                        LogUtils.error(LoginActivity.class, "Tag Response "+ object.toString(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (response.body().getAll_products() != null && response.body().getAll_products().size()>0){

                        productData = response.body().getAll_products();
//                        adapterHomeService = new AdapterHomeService(getContext(), productData);
                        rcv_service.setAdapter(adapterHomeService);

                        if (productData.get(0).getImages() != null && productData.get(0).getImages().size()>0){


                        }

                    } else {
                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.course_detail_not_found), Toast.LENGTH_LONG).show();
                    }

                }else {
                    no_data.setVisibility(View.VISIBLE);
                }
                if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                no_data.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                    loaderView.dismiss();
                }
            }
        });
    }

    public void courseCategoryApi () {

        if (!getActivity().isFinishing()){
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCategory(AppUtils.CATEGORY).enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(@NotNull Call<CategoryModel> call, @NotNull Response<CategoryModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  "+response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  "+response);

                no_data.setVisibility(View.GONE);
                AppUtils.categoryData = new ArrayList<>();
                if (response.isSuccessful() && response.body() != null){

                    try {
                        JSONObject object = new JSONObject(response.body().toString());
                        LogUtils.error(LoginActivity.class, "Tag Response "+ object.toString(1));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (response.body().getResult() != null && response.body().getResult().size()>0){


                        CategoryData category = new CategoryData();
                        for (int i=0; i<response.body().getResult().size(); i++){

                            category = response.body().getResult().get(i);
                            categoryData.add(category);
                        }
                        AppUtils.categoryData = categoryData;
                        setCategoryList();

                    } else {
                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getString(R.string.no_data_found), Toast.LENGTH_LONG).show();
                    }

                }else {
                    no_data.setVisibility(View.VISIBLE);
                }
                if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<CategoryModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  "+ t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }

                if (t instanceof SocketTimeoutException){

                }else if (t instanceof SocketException){

                }

                no_data.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing() && !getActivity().isFinishing()){
                    loaderView.dismiss();
                }
            }
        });
    }

    public void setCategoryList(){

        if (categoryData != null && categoryData.size()>0){
            adapterHomeService = new AdapterHomeService(getContext(), categoryData);
            rcv_service.setAdapter(adapterHomeService);
            no_data.setVisibility(View.GONE);
            rcv_service.setVisibility(View.VISIBLE);
        }else {
            no_data.setVisibility(View.VISIBLE);
            rcv_service.setVisibility(View.GONE);
        }

    }

    private void buildCounterDrawable() {
        AppUtils.refreshCartDB(txt_count, dbManager.fetch());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (txt_count != null){
            buildCounterDrawable();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == counterPanel){
            BonceInterpolate.clickViewButton(getActivity(),v);
            Intent intent = new Intent(getActivity(), CartActivity.class);
            startActivity(intent);
        } else if (v == btn_retry) {
            if (AppUtils.isNetworkAvailable(getActivity())){
                courseCategoryApi();
//                ProductApi();
            } else {
                AppUtils.show_toast_short(getActivity(), getString(R.string.internet_not_connect));
            }
        }
    }

}