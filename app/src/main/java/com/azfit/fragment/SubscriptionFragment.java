package com.azfit.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.Adapter.AdapterExpand;
import com.azfit.Adapter.AdapterExperts;
import com.azfit.Adapter.AdapterPrices;
import com.azfit.Adapter.AdapterWishlist;
import com.azfit.Api.ApiService;
import com.azfit.Api.RetrofitClient;
import com.azfit.Model.Course.Course;
import com.azfit.Model.Course.CourseData;
import com.azfit.Model.Orders.LineItems;
import com.azfit.Model.Orders.OrdersModel;
import com.azfit.Model.PriceDataModel;
import com.azfit.Model.Product.ProductData;
import com.azfit.Model.Product.ProductModel;
import com.azfit.Model.Product.SearchProductModel;
import com.azfit.Model.ServicesModel;
import com.azfit.Model.Topic.TopicModel;
import com.azfit.R;
import com.azfit.WishlistData.WishlistDBManager;
import com.azfit.activity.DetailActivity;
import com.azfit.activity.LoginActivity;
import com.azfit.shared_preference.SessionManager;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;
import com.azfit.utils.LogUtils;
import com.azfit.utils.RecyclerTouchListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionFragment extends Fragment implements View.OnClickListener {

    LinearLayout lin_error;
    TextView txt_msg;
    Button btn_retry;
    LoaderView loaderView;

    WishlistDBManager dbManager;

    ImageView img_logo;
    TextView txt_title, txt_slogan;
    TextView txt_content_title;
    RecyclerView rcv_subscription;
    AdapterPrices adapterPrices;

    List<CourseData> courseData = new ArrayList<>();
    List<ProductData> productData = new ArrayList<>();

    SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_subscription, container, false);

        sessionManager = new SessionManager(getActivity());
        dbManager = new WishlistDBManager(getActivity());
        loaderView = new LoaderView(getActivity());

        initView(root);

        return root;
    }

    public void initView(View view) {

        img_logo = view.findViewById(R.id.img_logo);
        txt_title = view.findViewById(R.id.txt_title);
        txt_slogan = view.findViewById(R.id.txt_slogan);

        lin_error = view.findViewById(R.id.lin_error);
        txt_msg = view.findViewById(R.id.txt_msg);
        btn_retry = view.findViewById(R.id.btn_retry);

        txt_content_title = view.findViewById(R.id.txt_content_title);
        rcv_subscription = view.findViewById(R.id.rcv_subscription);

        if (AppUtils.productData != null && AppUtils.productData.size()>0 && AppUtils.courseData != null && AppUtils.courseData.size() > 0){
            productData = AppUtils.productData;
            courseData = AppUtils.courseData;
            setRecycleView();
        }else {
            productCourseApi();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        return;
    }

    List<PriceDataModel> dataModels = new ArrayList<>();

    public void setRecycleView() {

        dataModels = new ArrayList<>();

        for (CourseData data : courseData) {

            for (ProductData productData : productData) {

                if (data.get_tutor_course_product_id().equals(productData.getId())) {

                    dataModels.add(new PriceDataModel(data.getID(), productData.getId(), data.getPost_title(), productData.getPrice_html(), productData.getPrice(), productData.getPrice_html()));

                }
            }
        }

        if (dataModels != null && dataModels.size() > 0) {

            rcv_subscription.setLayoutManager(new LinearLayoutManager(getActivity()));
            rcv_subscription.setHasFixedSize(true);
            rcv_subscription.setItemAnimator(new DefaultItemAnimator());
            adapterPrices = new AdapterPrices(getActivity(), dataModels);
            rcv_subscription.setAdapter(adapterPrices);

            rcv_subscription.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rcv_subscription, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {

                    holder holder = new holder(view);
                    initSubView(holder, position);

                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));

        }

        /*RecyclerTouchListener touchListener = new RecyclerTouchListener(this, rcv_prices, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


            }

            @Override
            public void onLongClick(View view, int position) {

            }

        });*/

    }

    public class holder extends RecyclerView.ViewHolder{

        ImageView img_expand;
        ProgressBar progressBar;
        View no_data;
        TextView txt_subMsg;
        Button btn_subRetry;
        RecyclerView rcv_expand;
        LinearLayout lin_service_list;
        AdapterExpand adapterExpand;
        Button btn_submit;

        public holder(@NonNull View itemView) {
            super(itemView);
            img_expand = itemView.findViewById(R.id.img_expand);
            progressBar = itemView.findViewById(R.id.progress);
            no_data = itemView.findViewById(R.id.no_data);
            txt_subMsg = itemView.findViewById(R.id.txt_message);
            btn_subRetry = itemView.findViewById(R.id.btn_retry);
            rcv_expand = itemView.findViewById(R.id.rcv_expand);
            lin_service_list = itemView.findViewById(R.id.lin_service_list);
            btn_submit = itemView.findViewById(R.id.btn_submit);

        }
    }

    public void initSubView(holder view, int position) {

        if (view != null){

           /* imageView = view.itemView.findViewById(R.id.img_expand);
            progressBar = viewHolder.itemView.findViewById(R.id.progress);
            no_data = viewHolder.itemView.findViewById(R.id.no_data);
            txt_subMsg = viewHolder.itemView.findViewById(R.id.txt_message);
            btn_subRetry = viewHolder.itemView.findViewById(R.id.btn_retry);
            rcv_expand = viewHolder.itemView.findViewById(R.id.rcv_expand);
            lin_service_list = viewHolder.itemView.findViewById(R.id.lin_service_list);
            btn_submit = viewHolder.itemView.findViewById(R.id.btn_submit);*/

            view.img_expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (view.img_expand.getRotation() == 0){
                        view.img_expand.setRotation(180);
                        if (view.rcv_expand != null && view.rcv_expand.getAdapter() != null && view.rcv_expand.getAdapter().getItemCount() >0){
                            view.lin_service_list.setVisibility(View.VISIBLE);
                        }else {
                            courseTopic(position, view);
                        }

                    }else {
                        view.img_expand.setRotation(0);
                        view.lin_service_list.setVisibility(View.GONE);
                    }
                }
            });

            view.btn_subRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    courseTopic(position, view);
                }
            });

            view.btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (view.btn_submit.getText().equals(getString(R.string.register_now))){
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        intent.putExtra("course_id", dataModels.get(position).getCourse_id());
                        startActivity(intent);
                    }
                }
            });

        }
    }

    public void productCourseApi() {

        if (!getActivity().isFinishing()) {
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getCourse(AppUtils.COURSE, "").enqueue(new Callback<Course>() {

            @Override
            public void onResponse(@NotNull Call<Course> call, @NotNull Response<Course> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
//                no_data.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    courseData = new ArrayList<>();
                    AppUtils.productData = new ArrayList<>();
                    AppUtils.courseData = new ArrayList<>();
                    if (response.body() != null && response.body().getResult().size() > 0) {

                        for (CourseData course : response.body().getResult()) {
//                            if (productData.getName().toLowerCase().equals(course.getPost_title().toLowerCase())){
                                    /*if (sessionManager.isLoggedIn()) {
                                        if (courseData.get_tutor_course_product_id() != null) {
                                            getOrderApi(courseData.get_tutor_course_product_id());
                                        }
                                    }*/
                            if (course.get_tutor_course_product_id() != null && !course.get_tutor_course_product_id().equals("")) {
                                courseData.add(course);
                            }
                        }

                        if (courseData != null && courseData.size() > 0) {
                            AppUtils.courseData = courseData;
                            rcv_subscription.setVisibility(View.VISIBLE);
                            lin_error.setVisibility(View.GONE);
                            btn_retry.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                            productApi();
                        } else {
                            rcv_subscription.setVisibility(View.GONE);
                            lin_error.setVisibility(View.VISIBLE);
                            btn_retry.setVisibility(View.GONE);
                            txt_msg.setText(getString(R.string.no_data_found));
                        }

                    } else {
                        rcv_subscription.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.GONE);
                        txt_msg.setText(getString(R.string.no_data_found));
                    }
                } else {
                    rcv_subscription.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.something_went_wrong));
                }
            }

            @Override
            public void onFailure(@NotNull Call<Course> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_retry.setVisibility(View.VISIBLE);
                lin_error.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }
        });
    }

    public void productApi() {

        if (!getActivity().isFinishing()) {
            loaderView.show();
        }

        ApiService service = RetrofitClient.createService(ApiService.class);

        service.getProduct().enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(@NotNull Call<ProductModel> call, @NotNull Response<ProductModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                productData = new ArrayList<>();
                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body().getAll_products() != null && response.body().getAll_products().size() > 0) {

                        productData = response.body().getAll_products();
                        AppUtils.productData = productData;
                        setRecycleView();
                        lin_error.setVisibility(View.GONE);
                        rcv_subscription.setVisibility(View.VISIBLE);

                    } else {
                        rcv_subscription.setVisibility(View.GONE);
                        lin_error.setVisibility(View.VISIBLE);
                        btn_retry.setVisibility(View.VISIBLE);
                        txt_msg.setText(getString(R.string.something_went_wrong));
                    }

                } else {
                    rcv_subscription.setVisibility(View.GONE);
                    lin_error.setVisibility(View.VISIBLE);
                    btn_retry.setVisibility(View.VISIBLE);
                    txt_msg.setText(getString(R.string.something_went_wrong));
                }
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ProductModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }
                btn_retry.setVisibility(View.VISIBLE);
                lin_error.setVisibility(View.VISIBLE);
                if (loaderView != null && loaderView.isShowing()) {
                    loaderView.dismiss();
                }
            }
        });
    }

    public List<PriceDataModel> getList() {
        List<PriceDataModel> models = new ArrayList<>();

        List<ServicesModel> models1 = new ArrayList<>();
        models1.add(new ServicesModel("1", "Ayurveda", ""));
        models1.add(new ServicesModel("2", "Oats", ""));
        models1.add(new ServicesModel("3", "Bowls", ""));
        models1.add(new ServicesModel("4", "Soul food", ""));
        models1.add(new ServicesModel("5", "Vegan", ""));
        models1.add(new ServicesModel("6", "Sugar free", ""));
        models.add(new PriceDataModel("1", "Nutrition", "monthly payment", "9.95", "Per month", models1));

        List<ServicesModel> models2 = new ArrayList<>();
        models2.add(new ServicesModel("1", "Taping", ""));
        models2.add(new ServicesModel("2", "Acupressure", ""));
        models2.add(new ServicesModel("3", "Reflex zones", ""));
        models2.add(new ServicesModel("4", "Cupping", ""));
        models2.add(new ServicesModel("5", "Flossing", ""));
        models2.add(new ServicesModel("6", "Happy hands", ""));
        models2.add(new ServicesModel("6", "Treat fascia yourself", ""));
        models.add(new PriceDataModel("2", "Halth", "monthly payment", "9.95", "Per month", models2));

        List<ServicesModel> models4 = new ArrayList<>();
        models4.add(new ServicesModel("1", "Booty", ""));
        models4.add(new ServicesModel("2", "Asana yoga", ""));
        models4.add(new ServicesModel("3", "Shoulder", ""));
        models4.add(new ServicesModel("4", "Karuna Yoga", ""));
        models4.add(new ServicesModel("5", "Tabata", ""));
        models4.add(new ServicesModel("6", "Beginner yoga", ""));
        models4.add(new ServicesModel("6", "HIIT", ""));
        models4.add(new ServicesModel("6", "Vinyasa yoga", ""));
        models4.add(new ServicesModel("6", "Mobility", ""));
        models.add(new PriceDataModel("3", "Sports", "monthly payment", "9.95", "Per month", models4));

        List<ServicesModel> models3 = new ArrayList<>();
        models3.add(new ServicesModel("1", "Access to all topics", ""));
        models3.add(new ServicesModel("2", "Access to over 300+ lessons", ""));
        models3.add(new ServicesModel("3", "Constantly new courses and content", ""));
        models3.add(new ServicesModel("4", "Reports & documentation", ""));
        models.add(new PriceDataModel("4", "All-in-one", "monthly payment", "9.95", "Per month", models3));

        return models;
    }

    public void courseTopic(int position, holder holder) {

        holder.progressBar.setVisibility(View.VISIBLE);
        holder.no_data.setVisibility(View.GONE);
        ApiService service = RetrofitClient.createServiceWoo(ApiService.class);

        service.getTopic(dataModels.get(position).getCourse_id()).enqueue(new Callback<TopicModel>() {

            @Override
            public void onResponse(@NotNull Call<TopicModel> call, @NotNull Response<TopicModel> response) {

                LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                LogUtils.error(LoginActivity.class, "Tag Response Default  " + response);

                if (response.isSuccessful() && response.body() != null) {

                    LogUtils.error(LoginActivity.class, "Tag Response " + response.body());

                    if (response.body() != null) {

                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            holder.rcv_expand.setLayoutManager(new LinearLayoutManager(getActivity()));
                            holder.rcv_expand.setHasFixedSize(true);
                            holder.rcv_expand.setItemAnimator(new DefaultItemAnimator());
                            holder.adapterExpand = new AdapterExpand(getActivity(), response.body().getData());
                            holder.rcv_expand.setAdapter(holder.adapterExpand);

                            holder.no_data.setVisibility(View.GONE);
                            holder.lin_service_list.setVisibility(View.VISIBLE);

                            if (sessionManager.isLoggedIn()){
                                getOrderApi(dataModels.get(position).getProduct_id(), holder);
                            }else {
                                holder.btn_submit.setText(getString(R.string.register_now));
                            }

                        } else {

                            holder.no_data.setVisibility(View.VISIBLE);
                            holder.txt_subMsg.setText(getString(R.string.no_data_found));
                            holder.btn_subRetry.setVisibility(View.VISIBLE);
                            holder.lin_service_list.setVisibility(View.GONE);
                        }

                        /*for (TopicData course : response.body().getData()){
                            if (productData.getName().toLowerCase().equals(course.getPost_title().toLowerCase())){
                                courseData = new CourseData();
                                courseData = course;
                            }
                        }*/

                    } else {
//                        no_data.setVisibility(View.VISIBLE);
                        holder.no_data.setVisibility(View.VISIBLE);
                        holder.txt_subMsg.setText(getString(R.string.no_data_found));
                        holder.btn_subRetry.setVisibility(View.VISIBLE);
                        holder.lin_service_list.setVisibility(View.GONE);
                    }

                } else {
                    holder.no_data.setVisibility(View.VISIBLE);
                    holder.txt_subMsg.setText(getString(R.string.no_data_found));
                    holder.btn_subRetry.setVisibility(View.GONE);
                    holder.lin_service_list.setVisibility(View.GONE);
                }

                if (holder.progressBar != null) {
                    holder.progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NotNull Call<TopicModel> call, @NotNull Throwable t) {
                if (t.getMessage() != null) {
                    LogUtils.error(LoginActivity.class, "Tag Error  " + t.getMessage());
                } else {
                    LogUtils.error(LoginActivity.class, "Tag Error  ");
                }

                holder.no_data.setVisibility(View.VISIBLE);
                holder.txt_subMsg.setText(getString(R.string.something_went_wrong));
                holder.btn_subRetry.setVisibility(View.VISIBLE);
                holder.lin_service_list.setVisibility(View.GONE);

                if (holder.progressBar != null) {
                    holder.progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    public void getOrderApi(String product_id, holder holder) {

        if (AppUtils.isNetworkAvailable(getActivity())) {

            ApiService apiService = RetrofitClient.createService(ApiService.class);
            apiService.getOrders(AppUtils.MY_ORDERS, sessionManager.getUserId()).enqueue(new Callback<OrdersModel>() {
                @Override
                public void onResponse(@NotNull Call<OrdersModel> call, @NotNull Response<OrdersModel> response) {

                    LogUtils.error(LoginActivity.class, "Tag Response Status  " + response.isSuccessful());
                    LogUtils.error(LoginActivity.class, "Tag Response Order  " + response);

                    boolean valid = false;
                    if (response.isSuccessful() && response.body() != null) {

                        try {
                            LogUtils.error(LoginActivity.class, "Tag Response Order" + new JSONObject(response.body().toString()).toString(2));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        orders = new ArrayList<>();
                        if (response.body().getOrders() != null && response.body().getOrders().size() > 0) {

                            for (int i = 0; i < response.body().getOrders().size(); i++) {

//                                if (response.body().getOrders().get(i).getStatus().equals("completed")){
                                if (response.body().getOrders().get(i).getLine_items() != null &&
                                        response.body().getOrders().get(i).getLine_items().size() > 0) {

                                    for (LineItems lineItem : response.body().getOrders().get(i).getLine_items()) {

                                        if (lineItem.getProduct_id().equals(product_id)) {

                                            if (response.body().getOrders().get(i).getStatus().equals("completed")) {

                                                Log.e("Tag check Date", AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()));
                                                if (AppUtils.getCheckDateExpire(response.body().getOrders().get(i).getCompleted_at()).equals("1")) {
                                                    valid = true;
                                                } else {
                                                    valid = false;
                                                    break;
                                                }
                                            } else if (response.body().getOrders().get(i).getStatus().equals("cancelled") || response.body().getOrders().get(i).getStatus().equals("failed")) {
                                                valid = true;
                                            } else {
                                                valid = false;
                                                break;
                                            }
                                        }
                                    }

                                }
                                if (!valid) {
                                    break;
                                }
                            }
                        } else {
                            valid = true;
                        }
                    } else {
                        valid = true;
                    }

                    if (!valid){
                        // already Buy
                        holder.btn_submit.setText(getString(R.string.details));
                    }else {
                        // have no buy
                        holder.btn_submit.setText(getString(R.string.details));
                    }

                    Log.e("Tag validate", "" + valid);

                }

                @Override
                public void onFailure(@NotNull Call<OrdersModel> call, @NotNull Throwable t) {

                    LogUtils.error(ProfileFragment.class, t.getMessage());

                    if (loaderView != null && loaderView.isShowing()) {
                        loaderView.dismiss();
                    }
                }
            });
        }
    }

}