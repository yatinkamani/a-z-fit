package com.azfit.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.azfit.activity.CartActivity;
import com.azfit.CartData.DBManagerData;
import com.azfit.R;
import com.azfit.utils.AppUtils;
import com.azfit.utils.LoaderView;

import org.jetbrains.annotations.NotNull;

public class NotificationsFragment extends Fragment implements View.OnClickListener {


    RecyclerView rcv_notification;
    // cart icon
    RelativeLayout counterValuePanel, counterPanel;
    TextView txt_count;

    View no_data;
    Button btn_retry;
    TextView txt_message;
    LoaderView loaderView;

    DBManagerData dbManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        dbManager = new DBManagerData(getActivity());
        loaderView = new LoaderView(getActivity());

        initView(root);

        return root;
    }

    public void initView(@NotNull View view) {
        rcv_notification = view.findViewById(R.id.rcv_notification);

        // cart view
        counterValuePanel = view.findViewById(R.id.counterValuePanel);
        counterPanel = view.findViewById(R.id.counterPanel);
        txt_count = view.findViewById(R.id.count);

        // no data
        no_data = view.findViewById(R.id.no_data);
        btn_retry = view.findViewById(R.id.btn_retry);
        txt_message = view.findViewById(R.id.txt_message);

        no_data.setVisibility(View.VISIBLE);
        txt_message.setText(getString(R.string.no_any_notification));
        btn_retry.setVisibility(View.GONE);

        counterValuePanel.setOnClickListener(this);
        counterPanel.setOnClickListener(this);
        buildCounterDrawable();

    }

    private void buildCounterDrawable() {
        AppUtils.refreshCartDB(txt_count, dbManager.fetch());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (txt_count != null){
            buildCounterDrawable();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == counterPanel){
            Intent intent = new Intent(getActivity(), CartActivity.class);
            startActivity(intent);
        }
    }

}