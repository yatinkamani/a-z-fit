package com.azfit.CourseDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;

import com.azfit.Model.Course.CourseData;
import com.azfit.Model.Orders.LineItems;
import com.azfit.Model.TopicLesson.TopicLessonData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CourseDBManager {

    private DatabaseHelperCourse dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public CourseDBManager(Context c) {
        context = c;
    }

    public CourseDBManager open() throws SQLException, SQLiteCantOpenDatabaseException {
        dbHelper = new DatabaseHelperCourse(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insertCourse(String course_id, String course_name, String total) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        // adding user name in users table
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelperCourse.NAME, course_name);
        contentValue.put(DatabaseHelperCourse.COURSE_ID, course_id);
        contentValue.put(DatabaseHelperCourse.TOTAL_LESSON, total);
        contentValue.put(DatabaseHelperCourse.COMPLETE_LESSON, "0");
        contentValue.put(DatabaseHelperCourse.COURSE_STATUS, "0");
        db.insert(DatabaseHelperCourse.TABLE_NAME, null, contentValue);
    }

    public void insertLesson(String course_id, String lesson_id, String lesson_name) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        // adding user name in users table
//        updateCompleteLesson(course_id);
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelperCourse.LESSON_NAME, lesson_name);
        contentValue.put(DatabaseHelperCourse.LESSON_ID, lesson_id);
        contentValue.put(DatabaseHelperCourse.COURSE_ID, course_id);
        contentValue.put(DatabaseHelperCourse.LESSON_STATUS, "0");
        db.insert(DatabaseHelperCourse.TABLE_NAME_LESSON, null, contentValue);
    }

    public void insertOrder(String product_id, String expire, String status) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        // adding user name in users table
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelperCourse.PRODUCT_ID, product_id);
        contentValue.put(DatabaseHelperCourse.EXPIRE, expire);
        contentValue.put(DatabaseHelperCourse.STATUS, status);
        db.insert(DatabaseHelperCourse.TABLE_NAME_ORDER, null, contentValue);
    }

    public List<CourseData> getFreeActiveCourse() {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<CourseData> databases = new ArrayList<>();
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {

                    String course_name = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_ID));
                    String course_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.NAME));
                    CourseData course = new CourseData();
                    course.setID(course_id);
                    course.setPost_title(course_name);
                    databases.add(course);
                } while (cursor.moveToNext());
            }
        }
        return databases;
    }

    public CourseData getFreeActiveCourseById(String c_id) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME + " WHERE " + DatabaseHelperCourse.COURSE_ID + " ='" + c_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        CourseData databases = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            String course_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_ID));
            String course_name = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.NAME));
            String total_lesson = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.TOTAL_LESSON));
            String complete_lesson = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COMPLETE_LESSON));
            String course_status = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_STATUS));
            CourseData course = new CourseData();
            course.setID(course_id);
            course.setPost_title(course_name);
            course.setNo_of_lesson(total_lesson);
            course.setComplete_lesson(complete_lesson);
            course.setStatus(course_status);
            databases = course;
        }
        return databases;
    }

    public boolean checkStartCourse(String c_id) {
//        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        open();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME + " WHERE " + DatabaseHelperCourse.COURSE_ID + "='" + c_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public List<TopicLessonData> getCompleteLesson() {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_LESSON;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<TopicLessonData> databases = new ArrayList<>();
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {
                    String course_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_ID));
                    String lesson_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_ID));
                    String lesson_name = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_NAME));
                    TopicLessonData course = new TopicLessonData();
                    course.setCourse_id(course_id);
                    course.setPost_title(lesson_name);
                    course.setiD(lesson_id);
                    databases.add(course);
                } while (cursor.moveToNext());
            }
        }
        return databases;
    }

    public List<TopicLessonData> getCompleteLessonById(String c_id) {
//        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        open();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_LESSON + " WHERE " + DatabaseHelperCourse.COURSE_ID + "='" + c_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = database.rawQuery(selectQuery, null);
        List<TopicLessonData> databases = new ArrayList<>();
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {

                    String course_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_ID));
                    String lesson_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_ID));
                    String lesson_name = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_NAME));
                    TopicLessonData course = new TopicLessonData();
                    course.setCourse_id(course_id);
                    course.setPost_title(lesson_name);
                    course.setiD(lesson_id);
                    databases.add(course);

                } while (cursor.moveToNext());
            }
        }
        return databases;
    }

    public boolean checkCompleteLesson(String c_id, String l_id) {
//        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        open();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_LESSON + " WHERE " + DatabaseHelperCourse.COURSE_ID + "='" + c_id + "' AND " + DatabaseHelperCourse.LESSON_ID + "='" + l_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public String checkPlayingLesson(String c_id, String l_id) {
//        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        open();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_LESSON + " WHERE " + DatabaseHelperCourse.COURSE_ID + "='" + c_id + "' AND " + DatabaseHelperCourse.LESSON_ID + "='" + l_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = database.rawQuery(selectQuery, null);
        String status = "-1";

        if (cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            status = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_STATUS));
        }

        return status;
    }

    public List<LineItems> getOrderById(String p_id) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_ORDER + " WHERE " + DatabaseHelperCourse.PRODUCT_ID + "='" + p_id + "'";
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<LineItems> databases = new ArrayList<>();
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {

                    String product_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.COURSE_ID));
                    String expire = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_ID));
                    String status = cursor.getString(cursor.getColumnIndex(DatabaseHelperCourse.LESSON_NAME));
                    LineItems course = new LineItems();
                    course.setProduct_id(product_id);
                    course.setExpire(expire);
                    course.setStatus(status);
                    databases.add(course);

                } while (cursor.moveToNext());
            }
        }
        return databases;
    }

    public boolean checkIsExistOrNoExpire(String p_id, @NotNull String status, String expire) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getReadableDatabase();
        String selectQuery = "";
        Cursor cursor = null;
        if (!status.equals("")) {
            selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_ORDER + " WHERE " + DatabaseHelperCourse.PRODUCT_ID + "=? AND " + DatabaseHelperCourse.STATUS + "=? ";
            cursor = db.rawQuery(selectQuery, new String[]{p_id, status});
        } else if (!status.equals("") && !expire.equals("")) {
            selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_ORDER + " WHERE " + DatabaseHelperCourse.PRODUCT_ID + "=? AND " + DatabaseHelperCourse.STATUS + "=? AND " + DatabaseHelperCourse.EXPIRE + "=? ";
            cursor = db.rawQuery(selectQuery, new String[]{p_id, status, expire});
        } else {
            selectQuery = "SELECT  * FROM " + DatabaseHelperCourse.TABLE_NAME_ORDER + " WHERE " + DatabaseHelperCourse.PRODUCT_ID + "=?";
            cursor = db.rawQuery(selectQuery, new String[]{p_id});
        }
        List<LineItems> databases = new ArrayList<>();
        if (cursor != null && cursor.getCount()>0) {
            return true;
        } else {
            return false;
        }
    }

    public int updateCompleteLesson(String course_id) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        CourseData courseData = getFreeActiveCourseById(course_id);
        String tot_complete = ""+(Integer.parseInt(courseData.getComplete_lesson()) + 1);
        String status = courseData.getNo_of_lesson().equals(tot_complete) ? "1" : "0";

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelperCourse.COMPLETE_LESSON, tot_complete);
        contentValues.put(DatabaseHelperCourse.COURSE_STATUS, status);
        int i = db.update(DatabaseHelperCourse.TABLE_NAME, contentValues, DatabaseHelperCourse.COURSE_ID + "=" + course_id, null);

        return i;
    }

    public int completeLesson(TopicLessonData lessonData) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        updateCompleteLesson(lessonData.getCourse_id());
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelperCourse.LESSON_STATUS, "1");
//        contentValues.put(DatabaseHelperCourse.COURSE_STATUS, status);
        int i = db.update(DatabaseHelperCourse.TABLE_NAME_LESSON, contentValues, DatabaseHelperCourse.LESSON_ID + "=" + lessonData.getID(), null);

        return i;
    }

    public void Delete(String course_id) {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        db.delete(DatabaseHelperCourse.TABLE_NAME, DatabaseHelperCourse.COURSE_ID + "=" + course_id, null);
    }

    public void removeTable() {
        SQLiteDatabase db = new DatabaseHelperCourse(context).getWritableDatabase();
        db.delete(DatabaseHelperCourse.TABLE_NAME, null, null);
    }

    public void removeDatabase() {
        context.deleteDatabase(DatabaseHelperCourse.DB_NAME);
    }
}
