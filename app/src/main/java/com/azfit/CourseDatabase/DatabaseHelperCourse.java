package com.azfit.CourseDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelperCourse extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "Course";

    // Table columns
    public static final String _ID = "_id";
    public static final String NAME = "course_name";
    public static final String COURSE_ID = "course_id";
    public static final String TOTAL_LESSON = "Total_lesson";
    public static final String COMPLETE_LESSON = "Complete_lesson";
    public static final String COURSE_STATUS = "status"; // 1- active // 2 - complete //

    // Table Name
    public static final String TABLE_NAME_ORDER = "CourseOrder";

    // Table columns
    public static final String PRODUCT_ID = "product_id";
    public static final String EXPIRE = "expire"; // not_expire = -1, expire = 1 , last day = 0
    public static final String STATUS = "status";

    public static final String TABLE_NAME_LESSON = "LESSON";

    public static final String LESSON_ID = "lesson_id";
    public static final String LESSON_NAME = "lesson_name";
    public static final String LESSON_STATUS = "lesson_status";

    // Database Information
    static final String DB_NAME = "COURSE.DB";

    // database version
    static final int DB_VERSION = 1;

    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAME + " TEXT NOT NULL, "
            + TOTAL_LESSON + " TEXT NOT NULL, "
            + COMPLETE_LESSON + " TEXT NOT NULL, "
            + COURSE_STATUS + " TEXT NOT NULL, "
            + COURSE_ID + " TEXT NOT NULL);";

    private static final String CREATE_TABLE_ORDER = "create table " + TABLE_NAME_ORDER + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PRODUCT_ID + " TEXT NOT NULL, "
            + EXPIRE + " TEXT NOT NULL, "
            + STATUS + " TEXT NOT NULL);";

    private static final String CREATE_TABLE_LESSON = "create table " + TABLE_NAME_LESSON + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COURSE_ID + " TEXT NOT NULL, "
            + LESSON_NAME + " TEXT NOT NULL, "
            + LESSON_STATUS + " TEXT NOT NULL, "
            + LESSON_ID + " TEXT NOT NULL);";

    // Creating table query

    public DatabaseHelperCourse(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_LESSON);
        db.execSQL(CREATE_TABLE_ORDER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_LESSON);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ORDER);
        onCreate(db);
    }
}
