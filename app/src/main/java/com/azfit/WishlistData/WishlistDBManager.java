package com.azfit.WishlistData;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.azfit.CourseDatabase.DatabaseHelperCourse;
import com.azfit.Model.WishlistData;

import java.util.ArrayList;
import java.util.List;

public class WishlistDBManager {

    private DatabaseHelperWishlist dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public WishlistDBManager(Context c) {
        context = c;
    }

    public WishlistDBManager open() throws SQLException {
        dbHelper = new DatabaseHelperWishlist(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String id, String category, String image, String name, String rate, String course_id, String cat_id, String level, String course_name) {
        SQLiteDatabase db = new DatabaseHelperWishlist(context).getWritableDatabase();
        //adding user name in users table
        ContentValues contentValue = new ContentValues();
        contentValue.put(DatabaseHelperWishlist.PRODUCT_ID, id);
        contentValue.put(DatabaseHelperWishlist.CATEGORY, category);
        contentValue.put(DatabaseHelperWishlist.IMAGE, image);
        contentValue.put(DatabaseHelperWishlist.NAME, name);
        contentValue.put(DatabaseHelperWishlist.RATE, rate);
        contentValue.put(DatabaseHelperWishlist.LEVEL, level.equals("null") ?" ":level);
        contentValue.put(DatabaseHelperWishlist.COURSE_ID, course_id);
        contentValue.put(DatabaseHelperWishlist.CATEGORY_ID, cat_id);
        contentValue.put(DatabaseHelperWishlist.COURSE_NAME, course_name);
        Log.e("Tag data", contentValue.toString());
        db.insert(DatabaseHelperWishlist.TABLE_NAME, null, contentValue);
    }

    public List<WishlistData> fetch() {
        SQLiteDatabase db = new DatabaseHelperWishlist(context).getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DatabaseHelperWishlist.TABLE_NAME;
        @SuppressLint("Recycle")
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<WishlistData> databases = new ArrayList<>();
        if (cursor != null) {

        if (cursor.moveToFirst()){
            do{
                String id = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.PRODUCT_ID));
                String category = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.CATEGORY));
                String image = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.IMAGE));
                String name = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.NAME));
                String level = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.LEVEL));
                String rate = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.RATE));
                String course_name = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.COURSE_NAME));
                String course_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.COURSE_ID));
                String cat_id = cursor.getString(cursor.getColumnIndex(DatabaseHelperWishlist.CATEGORY_ID));
                databases.add(new WishlistData(id, name, image, rate, category, level, course_id, cat_id, course_name));
            }while (cursor.moveToNext());
        }
        }
        return databases;
    }

    public int update( String product_id, String Quantity) {
        SQLiteDatabase db = new DatabaseHelperWishlist(context).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelperWishlist.NAME, Quantity);
        int i = db.update(DatabaseHelperWishlist.TABLE_NAME, contentValues,  DatabaseHelperWishlist.PRODUCT_ID + " = " + product_id, null);

        return i;
    }

    public void Delete(String course_id) {
        SQLiteDatabase db = new DatabaseHelperWishlist(context).getWritableDatabase();
        db.delete(DatabaseHelperWishlist.TABLE_NAME, DatabaseHelperWishlist.COURSE_ID + "=" + course_id, null);
    }

    public void removeTable() {
        SQLiteDatabase db = new DatabaseHelperWishlist(context).getWritableDatabase();
        db.delete(DatabaseHelperWishlist.TABLE_NAME,null,null);
    }

    public void removeDatabase() {
        context.deleteDatabase(DatabaseHelperWishlist.DB_NAME);
    }

}
