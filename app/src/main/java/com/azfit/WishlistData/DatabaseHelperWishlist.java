package com.azfit.WishlistData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelperWishlist extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "Wishlist";

    // Table columns
    public static final String _ID = "_id";
    public static final String NAME = "name";
    public static final String CATEGORY = "category";
    public static final String LEVEL = "level";
    public static final String IMAGE = "image";
    public static final String RATE = "rate";
    public static final String PRODUCT_ID = "product_id";
    public static final String COURSE_ID = "course_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String COURSE_NAME = "course_name";

    // Database Information
    static final String DB_NAME = "MY_WISHLIST.DB";

    // database version
    static final int DB_VERSION = 1;

    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PRODUCT_ID + " TEXT NOT NULL,"
            + NAME + " TEXT NOT NULL, "
            + LEVEL + " TEXT NOT NULL, "
            + IMAGE + " TEXT NOT NULL, "
            + RATE + " TEXT NOT NULL, "
            + COURSE_ID + " TEXT NOT NULL, "
            + COURSE_NAME + " TEXT NOT NULL, "
            + CATEGORY_ID + " TEXT NOT NULL, "
            + CATEGORY + " TEXT);";


    // Creating table query

    public DatabaseHelperWishlist(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
